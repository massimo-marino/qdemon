#include "DLabel.h"

#include <QDebug>


DLabel::DLabel(QWidget* parent) :
  QLabel(parent),
  _parent(parent)
{}

DLabel::~DLabel() {
  qInfo() << "DLabel::~DLabel: Object destroyed";
}

void DLabel::processMouseEvent(const QMouseEvent* event, const bool mousePress) {
  const QPixmap pixmapVal = pixmap(Qt::ReturnByValue);
  const int x {event->pos().x()};
  const int y {event->pos().y()};

  const QColor qc {pixmapVal.toImage().pixelColor(x, y)};
//  qInfo() << "[" << x << "," << y << "]: A:"
//          << qc.alpha() << " R:"
//          << qc.red() << " G:"
//          << qc.green() << " B:"
//          << qc.blue();
  emit colorInfo(x, y, qc, mousePress);
}  // DLabel::processMouseEvent

void DLabel::mouseMoveEvent(QMouseEvent* event) {
  processMouseEvent(event, false);
}

void DLabel::mousePressEvent(QMouseEvent* event) {
  processMouseEvent(event, true);
}

void DLabel::mouseDoubleClickEvent(QMouseEvent* event) {
  processMouseEvent(event, false);
}
