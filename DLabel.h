#pragma once

#include <QLabel>
#include <QMouseEvent>

class DLabel : public QLabel
{
  Q_OBJECT

public:
  explicit DLabel(QWidget* parent = nullptr);
  ~DLabel() override;

  void mouseMoveEvent(QMouseEvent* event) override;
  void mousePressEvent(QMouseEvent* event) override;
  void mouseDoubleClickEvent(QMouseEvent* event) override;

private:
  QWidget* _parent {nullptr};

  void processMouseEvent(const QMouseEvent* event, bool mousePress);

signals:
  void colorInfo(const int x, const int y, const QColor& qc, const bool mousePress);

};  // class DLabel
