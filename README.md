## Two Dimensional Cyclic Cellular Automata

This is a so-called two dimensional cyclic space.  
For a full detailed description see the links and references below.

A matrix of coloured cells is randomly initialized. Every cell can be in a set of `n` possible states represented by a colour numbered from `0` to `n-1`.

A cell in state `k` at time `t` eats all adjacent cells in state `k-1` by changing their state from `k-1` to `k`.
Since the matrix is a modular space (for example cell `(0,0)` is adjacent to cell `(n-1,n-1)`, and so on) and cyclic it happens that a cell in state `0` eats its neighbours that are in state `n-1`.

The code is written in Processing using the Processing Development Environment v.3.5.4.

***

![demon cyclic space](./csca-18-500x500.gif  "Demon Cyclic Space")

Cyclic space with 18 states in a matrix of 500x500 pixels
***

#### Links

- [Fisch, R., Gravner, J., Griffeath, D. (1991). Cyclic Cellular Automata in Two Dimensions (Paper)](https://www.math.ucdavis.edu/~gravner/papers/cca.pdf)  
- [Cyclic Cellular Automata on Wikipedia](https://en.wikipedia.org/wiki/Cyclic_cellular_automaton) 

#### References

- Dewdney, A.K. (1988). Computer Recreations. *Scientific American*, August, 104-107.  
- Dewdney, A. K. (1989). "Computer Recreations: A cellular universe of debris, droplets, defects, and demons". *Scientific American*, August, 102–105.
