//
// qDemon Black Board
//
#include "dbb.h"
#include <QDir>
#include <QUrl>
#include <QDesktopServices>
#include <QDebug>

const QString Dbb::_rngNameDefault {"rn"};


Dbb& Dbb::instance() {
  static bool created {false};
  static Dbb dbb;
  if (created) {
      qInfo() << "Dbb::instance: existing Dbb single instance:" << &dbb;
  } else {
    qInfo() << "Dbb::instance: Dbb single instance created:" << &dbb;
  }
  created = true;
  return dbb;
}

DBBPointer Dbb::operator()() {
  return this;
}

Dbb::Dbb() :
  _width(_widthDefault),
  _height(_heightDefault),
  _states(_statesDefault),
  _range(_rangeDefault),
  _stateDelta(_stateDeltaDefault),
  _maskSize(computeMaskSize()),
  _maskType(_maskTypeDefault),
  _createNewRun(true),
  _useLastRun(false),
  _saveFrames(true),
  _saveGenerations(true),
  _breakLoopOnMatch(true),
  _loadPalette(false),
  _shufflePalette(false),
  _loadGen0(false),
  _lastRun(0),
  _rng(_rngDefault),
  _rngName(_rngNameDefault),
  _mask(static_cast<size_t>(_maskSize), _maskType),
  _demonConfigString(buildDemonConfigString()),
  _demonConfigRootDir(buildDemonConfigRootDirName()),
  _demonConfigRunDir(""),
  _demonDataDir(""),
  _demonConfigDir("")
{
  qInfo() << "Dbb::Dbb: CTOR CALLED:" << this;
}

Dbb::~Dbb() {
  qInfo() << "Dbb::Dbb: DTOR CALLED:" << this;
}

unsigned int Dbb::computeMaskSize(const unsigned int range) noexcept {
  return range * 2 + 1;
}

unsigned int Dbb::computeMaskSize() noexcept {
  return computeMaskSize(_range);
}

QString Dbb::buildDemonConfigString() noexcept {
  return _rngName + "-" +
         QString::number(_states) + "-" +
         QString::number(_range) + "-" +
         QString::number(_stateDelta) + "-" +
         QString::number(_width) + "x" +
         QString::number(_height) + "-" +
         QString::fromStdString(mask_t::maskTypeName(maskType()));
}

QString Dbb::buildDemonConfigRootDirName() noexcept {
  return _rngName + "/" +
         QString::number(_states) + "-" +
         QString::number(_range) + "-" +
         QString::number(_stateDelta) + "/" +
         QString::number(_width) + "x" +
         QString::number(_height) + "/";
}

void Dbb::setRNGName() noexcept {
  switch (_rng)
  {
    // perlin noise 1 - pn
    case PERLIN_NOISE_1:
    {
      _rngName = "pn";
    }
    break;

    // perlin noise 2 - pn2
    case PERLIN_NOISE_2:
    {
      _rngName = "pn2";
    }
    break;

    // perlin noise 3 - pn3
    case PERLIN_NOISE_3:
    {
      _rngName = "pn3";
    }
    break;

    // random noise 1 - rn
    case RANDOM_NUMBER_GENERATOR_1:
    {
      _rngName = "rn";
    }
    break;

    // random noise 2 - rn2
    case RANDOM_NUMBER_GENERATOR_2:
    {
      _rngName = "rn2";
    }
    break;

    // random noise 3 - rn3
    case RANDOM_NUMBER_GENERATOR_3:
    {
      _rngName = "rn3";
    }
    break;

    // random noise 4 - rn4
    case RANDOM_NUMBER_GENERATOR_4:
    {
      _rngName = "rn4";
    }
    break;

    // random noise T - rnT [Test only]
    case RANDOM_NUMBER_GENERATOR_T:
    {
      _rngName = "rnT";
    }
    break;
  }
}

QString Dbb::getRunString() noexcept {
  return "run-" + QString::number(_lastRun);
}

void Dbb::openDemonConfigRootFolder() noexcept {
  QDir d;
  const QString basePath {d.absolutePath() + "/"};

  qInfo() << "Dbb::openDemonConfigRootFolder: _demonConfigRootDir" << basePath + _demonConfigRootDir << "***";
  qInfo() << "Dbb::openDemonConfigRootFolder: _demonDataDir      " << basePath + _demonDataDir;
  qInfo() << "Dbb::openDemonConfigRootFolder: _demonConfigDir    " << basePath + _demonConfigDir;

  QDesktopServices::openUrl(QUrl("file:" + basePath + _demonConfigRootDir, QUrl::TolerantMode));
}

void Dbb::openDemonConfigFolder() noexcept {
  QDir d;
  const QString basePath {d.absolutePath() + "/"};

  qInfo() << "Dbb::openDemonConfigFolder: _demonConfigRootDir" << basePath + _demonConfigRootDir;
  qInfo() << "Dbb::openDemonConfigFolder: _demonDataDir      " << basePath + _demonDataDir;
  qInfo() << "Dbb::openDemonConfigFolder: _demonConfigDir    " << basePath + _demonConfigDir;

  QDesktopServices::openUrl(QUrl("file:" + basePath + _demonConfigDir, QUrl::TolerantMode));

}

void Dbb::openDemonDataFolder() noexcept {
  QDir d;
  const QString basePath {d.absolutePath() + "/"};

  qInfo() << "Dbb::openDemonDataFolder: _demonConfigRootDir" << basePath + _demonConfigRootDir;
  qInfo() << "Dbb::openDemonDataFolder: _demonDataDir      " << basePath + _demonDataDir;
  qInfo() << "Dbb::openDemonDataFolder: _demonConfigDir    " << basePath + _demonConfigDir;

  QDesktopServices::openUrl(QUrl("file:" + basePath + _demonDataDir, QUrl::TolerantMode));

}

unsigned int Dbb::width() const {
  return _width;
}

void Dbb::setWidth(unsigned int width) {
  _width = width;
}

unsigned int Dbb::height() const {
  return _height;
}

void Dbb::setHeight(unsigned int height) {
  _height = height;
}

unsigned int Dbb::states() const {
  return _states;
}

void Dbb::setStates(unsigned int states) {
  _states = states;
}

unsigned int Dbb::range() const {
  return _range;
}

void Dbb::setRange(unsigned int range) {
  _range = range;
}

unsigned int Dbb::stateDelta() const {
  return _stateDelta;
}

void Dbb::setStateDelta(unsigned int stateDelta) {
  _stateDelta = stateDelta;
}

unsigned int Dbb::maskSize() const {
  return _maskSize;
}

void Dbb::setMaskSize(unsigned int maskSize) {
  _maskSize = maskSize;
}

::maskType_t Dbb::maskType() const {
  return _maskType;
}

void Dbb::setMaskType(const ::maskType_t& maskType) {
  _maskType = maskType;
}

bool Dbb::createNewRun() const {
  return _createNewRun;
}

void Dbb::setCreateNewRun(bool createNewRun) {
  _createNewRun = createNewRun;
}

bool Dbb::useLastRun() const {
  return _useLastRun;
}

void Dbb::setUseLastRun(bool useLastRun) {
  _useLastRun = useLastRun;
}

bool Dbb::saveFrames() const {
  return _saveFrames;
}

void Dbb::setSaveFrames(bool saveFrames) {
  _saveFrames = saveFrames;
}

bool Dbb::saveGenerations() const {
  return _saveGenerations;
}

void Dbb::setSaveGenerations(bool saveGenerations) {
  _saveGenerations = saveGenerations;
}

bool Dbb::breakLoopOnMatch() const {
  return _breakLoopOnMatch;
}

void Dbb::setBreakLoopOnMatch(bool breakLoopOnMatch) {
  _breakLoopOnMatch = breakLoopOnMatch;
}

bool Dbb::loadPalette() const {
  return _loadPalette;
}

void Dbb::setLoadPalette(bool loadPalette) {
  _loadPalette = loadPalette;
}

bool Dbb::shufflePalette() const {
  return _shufflePalette;
}

void Dbb::setShufflePalette(bool shufflePalette) {
  _shufflePalette = shufflePalette;
}

bool Dbb::loadGen0() const {
  return _loadGen0;
}

void Dbb::setLoadGen0(bool loadGen0) {
  _loadGen0 = loadGen0;
}

int Dbb::lastRun() const {
  return _lastRun;
}

void Dbb::setLastRun(int lastRun) {
  _lastRun = lastRun;
}

unsigned int Dbb::rng() const {
  return _rng;
}

void Dbb::setRng(unsigned int rng) {
  _rng = rng;
}

QString Dbb::rngName() const {
  return _rngName;
}

void Dbb::setRngName(const QString& rngName) {
  _rngName = rngName;
}

QString Dbb::demonConfigString() const {
  return _demonConfigString;
}

void Dbb::setDemonConfigString(const QString& demonConfigString) {
  _demonConfigString = demonConfigString;
}

QString Dbb::demonConfigRootDir() const {
  return _demonConfigRootDir;
}

void Dbb::setDemonConfigRootDir(const QString& demonConfigRootDir) {
  _demonConfigRootDir = demonConfigRootDir;
}

QString Dbb::demonConfigRunDir() const {
  return _demonConfigRunDir;
}

void Dbb::setDemonConfigRunDir(const QString& demonConfigRunDir) {
  _demonConfigRunDir = demonConfigRunDir;
}

QString Dbb::demonDataDir() const {
  return _demonDataDir;
}

void Dbb::setDemonDataDir(const QString& demonDataDir) {
  _demonDataDir = demonDataDir;
}

QString Dbb::demonConfigDir() const {
  return _demonConfigDir;
}

void Dbb::setDemonConfigDir(const QString& demonConfigDir) {
  _demonConfigDir = demonConfigDir;
}

mask_t& Dbb::mask() const {
  return _mask;
}

void Dbb::setMask(const mask_t& mask) {
  _mask = mask;
}
