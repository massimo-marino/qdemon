#ifndef DBB_H
#define DBB_H
//
// qDemon Black Board
//
#include "mask.h"

#include <QObject>
#include <QPointer>

class Dbb;
using DBBPointer = QPointer<Dbb>;

class Dbb : public QObject {
  Q_OBJECT

public:
  static Dbb& instance();

  DBBPointer operator()();
  Dbb(const Dbb&) = delete;
  Dbb& operator=(const Dbb&) = delete;

  static constexpr unsigned int PERLIN_NOISE_1 {1};
  static constexpr unsigned int PERLIN_NOISE_2 {2};
  static constexpr unsigned int PERLIN_NOISE_3 {3};

  static constexpr unsigned int RANDOM_NUMBER_GENERATOR_1 {20};
  static constexpr unsigned int RANDOM_NUMBER_GENERATOR_2 {21};
  static constexpr unsigned int RANDOM_NUMBER_GENERATOR_3 {22};
  static constexpr unsigned int RANDOM_NUMBER_GENERATOR_4 {23};
  static constexpr unsigned int RANDOM_NUMBER_GENERATOR_T {2000};

  static constexpr unsigned int _widthDefault {500};
  static constexpr unsigned int _heightDefault {500};
  static constexpr unsigned int _statesDefault {16};
  static constexpr unsigned int _rangeDefault {1};
  static constexpr ::maskType_t _maskTypeDefault {::maskType_t::MOORE};
  static constexpr unsigned int _stateDeltaDefault {1};
  static constexpr unsigned int _rngDefault {RANDOM_NUMBER_GENERATOR_1};

  static const QString _rngNameDefault;

  unsigned int computeMaskSize(const unsigned int range) noexcept;
  unsigned int computeMaskSize() noexcept;
  QString buildDemonConfigString() noexcept;
  QString buildDemonConfigRootDirName() noexcept;
  void setRNGName() noexcept;
  QString getRunString() noexcept;
  void openDemonConfigRootFolder() noexcept;
  void openDemonConfigFolder() noexcept;
  void openDemonDataFolder() noexcept;

  unsigned int width() const;
  void setWidth(unsigned int width);

  unsigned int height() const;
  void setHeight(unsigned int height);

  unsigned int states() const;
  void setStates(unsigned int states);

  unsigned int range() const;
  void setRange(unsigned int range);

  unsigned int stateDelta() const;
  void setStateDelta(unsigned int stateDelta);

  unsigned int maskSize() const;
  void setMaskSize(unsigned int maskSize);

  ::maskType_t maskType() const;
  void setMaskType(const ::maskType_t& maskType);

  bool createNewRun() const;
  void setCreateNewRun(bool createNewRun);

  bool useLastRun() const;
  void setUseLastRun(bool useLastRun);

  bool saveFrames() const;
  void setSaveFrames(bool saveFrames);

  bool saveGenerations() const;
  void setSaveGenerations(bool saveGenerations);

  bool breakLoopOnMatch() const;
  void setBreakLoopOnMatch(bool breakLoopOnMatch);

  bool loadPalette() const;
  void setLoadPalette(bool loadPalette);

  bool shufflePalette() const;
  void setShufflePalette(bool shufflePalette);

  bool loadGen0() const;
  void setLoadGen0(bool loadGen0);

  int lastRun() const;
  void setLastRun(int lastRun);

  unsigned int rng() const;
  void setRng(unsigned int rng);

  QString rngName() const;
  void setRngName(const QString& rngName);

  QString demonConfigString() const;
  void setDemonConfigString(const QString& demonConfigString);

  QString demonConfigRootDir() const;
  void setDemonConfigRootDir(const QString& demonConfigRootDir);

  QString demonConfigRunDir() const;
  void setDemonConfigRunDir(const QString& demonConfigRunDir);

  QString demonDataDir() const;
  void setDemonDataDir(const QString& demonDataDir);

  QString demonConfigDir() const;
  void setDemonConfigDir(const QString& demonConfigDir);

  mask_t& mask() const;
  void setMask(const mask_t& mask);

private:
// picture size: width x height
//  500x500  px -> 4.2x4.2 cm
// 5000x2500 px -> 42x20 cm
// 5906x3500 px -> 50x30 cm
// 5906x5906 px -> 50x50 cm
// 7000x3500 px -> 60x30 cm
// 9000x4500 px -> 76x38 cm
// 9500x4750 px -> 80x40 cm
// 9500x9500 px -> 80x80 cm
// A4 is 21.0x29.7 cm or 210x297 mm
  unsigned int _width;
  unsigned int _height;
// _states: number of states/colors
  unsigned int _states;
  unsigned int _range;
  unsigned int _stateDelta;
  unsigned int _maskSize;
  ::maskType_t _maskType;
  bool _createNewRun;
  bool _useLastRun;
  bool _saveFrames;
  bool _saveGenerations;
// _breakLoopOnMatch: if true the demon loop around a pixel might be faster
  bool _breakLoopOnMatch;
  bool _loadPalette;
  bool _shufflePalette;
  bool _loadGen0;
  int _lastRun;
  unsigned int _rng;
  QString _rngName;
  mutable mask_t _mask;
  QString _demonConfigString;
// _demonConfigRootDir = "rngName/s-r-m/WxH/" - e.g.: "rn2/20-1-2/1000x2000/"
  QString _demonConfigRootDir;
  QString _demonConfigRunDir;
  QString _demonDataDir;
  QString _demonConfigDir;

  Dbb();
  ~Dbb();
};

#endif // DBB_H
