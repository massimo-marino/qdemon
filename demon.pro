#-------------------------------------------------
#
# Project created by QtCreator 2018-10-17T11:56:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = demon
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++17 # qdemon compiles with Qt 6.2.4 and the GUI is fine

CONFIG += -system-libpng

LIBS += -lpng -lm -lrt

SOURCES += main.cpp\
    dbb.cpp \
    mainwindow.cpp \
    demonconfig.cpp \
    palettemanager.cpp \
    sg/helpers.cpp \
    sg/palette.cpp \
    sg/rgb.cpp \
    demonrunner.cpp \
    DLabel.cpp \
    util.cpp

HEADERS  += mainwindow.h \
    dbb.h \
    demonconfig.h \
    mask.h \
    palettemanager.h \
    sg/helpers.h \
    sg/image.h \
    sg/palette.h \
    sg/randomNumberGenerators.h \
    sg/rgb.h \
    demonrunner.h \
    matx.h \
    DLabel.h \
    util.h

FORMS    += mainwindow.ui \
    demonconfig.ui \
    demonrunner.ui \
    palettemanager.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    demon.qss
