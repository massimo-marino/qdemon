//
// demonconfig.cpp
//
#include "demonconfig.h"
#include "ui_demonconfig.h"
#include "mainwindow.h"

#include "util.h"

#include <QDesktopServices>
#include <QUrl>
#include <QDir>
#include <QDebug>
////////////////////////////////////////////////////////////////////////////////

demonConfig::demonConfig(QWidget *parent) :
  QDialog(parent),
  _dbb(Dbb::instance()),
  ui(new Ui::demonConfig),
  _mainWindow(qobject_cast<MainWindow*>(parent))
{
  qInfo() << "demonConfig::demonConfig: >>>>>>>>>> CTOR START";

  ui->setupUi(this);

  readSettings();
  demon::util::logSettings();

  ui->widthSpinBox->setValue(_dbb.width());
  ui->heightSpinBox->setValue(_dbb.height());
  ui->statesSpinBox->setValue(_dbb.states());
  ui->rangeSpinBox->setValue(_dbb.range());
  ui->maskTypeComboBox->setCurrentIndex(static_cast<unsigned int>(_dbb.maskType()));
  ui->stateDeltaSpinBox->setValue(_dbb.stateDelta());
  ((_dbb.createNewRun()) ? ui->createNewRunCheckBox->setCheckState(Qt::Checked) : ui->createNewRunCheckBox->setCheckState(Qt::Unchecked));
  ((_dbb.useLastRun()) ? ui->useLastRunCheckBox->setCheckState(Qt::Checked) : ui->useLastRunCheckBox->setCheckState(Qt::Unchecked));
  ((_dbb.saveFrames()) ? ui->saveFramesCheckBox->setCheckState(Qt::Checked) : ui->saveFramesCheckBox->setCheckState(Qt::Unchecked));
  ((_dbb.saveGenerations()) ? ui->saveGenerationsCheckBox->setCheckState(Qt::Checked) : ui->saveGenerationsCheckBox->setCheckState(Qt::Unchecked));
  ((_dbb.breakLoopOnMatch()) ? ui->breakLoopOnMatchCheckBox->setCheckState(Qt::Checked) : ui->breakLoopOnMatchCheckBox->setCheckState(Qt::Unchecked));
  updateMaskSize(_dbb.range());
  ((_dbb.loadPalette()) ? ui->loadPaletteCheckBox->setCheckState(Qt::Checked) : ui->loadPaletteCheckBox->setCheckState(Qt::Unchecked));
  ((_dbb.shufflePalette()) ? ui->shufflePaletteCheckBox->setCheckState(Qt::Checked) : ui->shufflePaletteCheckBox->setCheckState(Qt::Unchecked));
  ((_dbb.loadGen0()) ? ui->loadGen0CheckBox->setCheckState(Qt::Checked) : ui->loadGen0CheckBox->setCheckState(Qt::Unchecked));
  _dbb.setRNGName();
  switch (_dbb.rng())
  {
    case Dbb::RANDOM_NUMBER_GENERATOR_1:
      ui->rng1RadioButton->setChecked(true);
      break;
    case Dbb::RANDOM_NUMBER_GENERATOR_2:
      ui->rng2RadioButton->setChecked(true);
      break;
    case Dbb::RANDOM_NUMBER_GENERATOR_3:
      ui->rng3RadioButton->setChecked(true);
      break;
    default:
      qInfo() << "RNG not implemented:" << _dbb.rng() << _dbb.rngName();
  }

  connect(ui->createNewRunCheckBox,   &QCheckBox::stateChanged,  this, &demonConfig::on_createNewRunCheckBoxChanged);
  connect(ui->useLastRunCheckBox,     &QCheckBox::stateChanged,  this, &demonConfig::on_useLastRunCheckBoxChanged);
  connect(ui->loadPaletteCheckBox,    &QCheckBox::stateChanged,  this, &demonConfig::on_loadPaletteCheckBoxChanged);
  connect(ui->shufflePaletteCheckBox, &QCheckBox::stateChanged,  this, &demonConfig::on_shufflePaletteCheckBoxChanged);
  connect(ui->saveConfigPushButton,   &QPushButton::clicked,      this, &demonConfig::on_ClickSaveConfig);
  connect(ui->buttonBox,              &QDialogButtonBox::clicked, this, &demonConfig::on_ClickClose);
  connect(ui->rangeSpinBox,           SIGNAL(valueChanged(int)),  this, SLOT(updateMaskSize(int)));
  connect(ui->maskTypeComboBox,       SIGNAL(currentIndexChanged(int)), this, SLOT(updateMaskType(int)));

  qInfo() << "demonConfig::demonConfig: >>>>>>>>>> CTOR END";
}  // demonConfig::demonConfig

demonConfig::~demonConfig() {
  writeSettings();
  delete ui;
}

void demonConfig::writeSettings() const {
  // config file in /home/<user-name>/.config/skylark
  demon::util::writeGeometryInSettings(*this, "demonConfig");
}

void demonConfig::readSettings() {
  demon::util::readGeometryFromSettings(*this, "demonConfig");
}

void
demonConfig::on_ClickSaveConfig() noexcept {
//  qInfo() << "demonConfig::onClick: Button Clicked: 'Save Config'";
  updateDemonConfig();
  setWindowTitle("Demon Config | " + _dbb.demonConfigString());
  emit demonConfigSaved();
}  // demonConfig::on_ClickSaveConfig

void
demonConfig::on_ClickClose(QAbstractButton *button) noexcept {
  //qInfo() << "demonConfig::onClick: Button Text:" << button->text();
  if ( "&Close" == button->text() ) {
//    qInfo() << "demonConfig::onClick: Button Clicked: 'Close'";
    emit demonConfigClosed();
  }
//  if ( "&Cancel" == button->text() ) {
//    qInfo() << "demonConfig::onClick: Button Clicked: 'Cancel'";
//  }
}  // demonConfig::on_ClickClose

void
demonConfig::on_loadPaletteCheckBoxChanged(const int state) {
  switch ( state )
  {
    case Qt::Unchecked:
    break;

    case Qt::Checked:
    {
      ui->shufflePaletteCheckBox->setCheckState(Qt::Unchecked);
    }
    break;
  }
}  // demonConfig::on_loadPaletteCheckBoxChanged

void
demonConfig::on_shufflePaletteCheckBoxChanged(const int state) {
  switch ( state )
  {
    case Qt::Unchecked:
    break;

    case Qt::Checked:
    {
      ui->loadPaletteCheckBox->setCheckState(Qt::Unchecked);
    }
    break;
  }
}  // demonConfig::on_shufflePaletteCheckBoxChanged

void
demonConfig::on_createNewRunCheckBoxChanged(const int state) {
  switch ( state )
  {
    case Qt::Unchecked:
    {
      ui->useLastRunCheckBox->setCheckState(Qt::Checked);
    }
    break;

    case Qt::Checked:
    {
      ui->useLastRunCheckBox->setCheckState(Qt::Unchecked);
    }
    break;
  }
}  // demonConfig::on_createNewRunCheckBoxChanged

void
demonConfig::on_useLastRunCheckBoxChanged(const int state) {
  switch ( state )
  {
    case Qt::Unchecked:
    {
      ui->createNewRunCheckBox->setCheckState(Qt::Checked);
    }
    break;

    case Qt::Checked:
    {
      ui->createNewRunCheckBox->setCheckState(Qt::Unchecked);
    }
    break;
  }
}  // demonConfig::on_useLastRunCheckBoxChanged

void
demonConfig::updateMaskSize(int rangeValue) const noexcept {
  ui->maskSizeValueLabel->setNum(static_cast<int>(_dbb.computeMaskSize(rangeValue)));
}

void demonConfig::updateMaskType(int index) const noexcept {
  _dbb.setMaskType(static_cast<::maskType_t>(index));
}

void
demonConfig::updateDemonConfig() const noexcept {
  qInfo() << "Updating demon config...";

  const auto widthPrev {_dbb.width()};
  const auto heightPrev {_dbb.height()};
  const auto statesPrev {_dbb.states()};
  const auto rangePrev {_dbb.range()};
  const auto stateDeltaPrev {_dbb.stateDelta()};
  const auto maskSizePrev {_dbb.maskSize()};
  const auto maskTypePrev {_dbb.maskType()};
  const bool createNewRunPrev {_dbb.createNewRun()};
  const bool useLastRunPrev {_dbb.useLastRun()};
  const bool saveFramesPrev {_dbb.saveFrames()};
  const bool saveGenerationsPrev {_dbb.saveGenerations()};
  const bool breakLoopOnMatchPrev {_dbb.breakLoopOnMatch()};
  const bool loadPalettePrev {_dbb.loadPalette()};
  const bool shufflePalettePrev {_dbb.shufflePalette()};
  const bool loadGen0Prev {_dbb.loadGen0()};

  _dbb.setWidth(ui->widthSpinBox->value());
  _dbb.setHeight(ui->heightSpinBox->value());
  _dbb.setStates(ui->statesSpinBox->value());
  _dbb.setRange(ui->rangeSpinBox->value());
  _dbb.setStateDelta(ui->stateDeltaSpinBox->value());
  _dbb.setMaskSize(_dbb.computeMaskSize());
  _dbb.setMaskType(static_cast<::maskType_t>(ui->maskTypeComboBox->currentIndex()));
  _dbb.setCreateNewRun(ui->createNewRunCheckBox->isChecked());
  _dbb.setUseLastRun(ui->useLastRunCheckBox->isChecked());
  _dbb.setSaveFrames(ui->saveFramesCheckBox->isChecked());
  _dbb.setSaveGenerations(ui->saveGenerationsCheckBox->isChecked());
  _dbb.setBreakLoopOnMatch(ui->breakLoopOnMatchCheckBox->isChecked());
  _dbb.setLoadPalette(ui->loadPaletteCheckBox->isChecked());
  _dbb.setShufflePalette(ui->shufflePaletteCheckBox->isChecked());
  _dbb.setLoadGen0(ui->loadGen0CheckBox->isChecked());
  if ( ui->rng1RadioButton->isChecked() ) {
    _dbb.setRng(Dbb::RANDOM_NUMBER_GENERATOR_1);
  }
  else if ( ui->rng2RadioButton->isChecked() ) {
    _dbb.setRng(Dbb::RANDOM_NUMBER_GENERATOR_2);
  }
  else if ( ui->rng3RadioButton->isChecked() ) {
    _dbb.setRng(Dbb::RANDOM_NUMBER_GENERATOR_3);
  }
  _dbb.setRNGName();
  _dbb.mask().resize(_dbb.maskSize(), _dbb.maskType());
  _dbb.setDemonConfigString(_dbb.buildDemonConfigString());
  _dbb.setDemonConfigRootDir(_dbb.buildDemonConfigRootDirName());
  _dbb.setDemonDataDir("");
  _dbb.setDemonConfigDir("");

  saveDemonConfig();

  qInfo() << "create new run:" << createNewRunPrev << "->" << _dbb.createNewRun();
  qInfo() << "use last run:  " << useLastRunPrev   << "->" << _dbb.useLastRun();
  qInfo() << "width: " << widthPrev  << "->" << _dbb.width();
  qInfo() << "height:" << heightPrev << "->" << _dbb.height();
  qInfo() << "states:" << statesPrev << "->" << _dbb.states();
  qInfo() << "range: " << rangePrev  << "->" << _dbb.range();
  qInfo() << "mask size:  " << maskSizePrev   << "->" << _dbb.maskSize();
  qInfo() << "mask type:  " << QString::fromStdString(mask_t::maskTypeName(maskTypePrev)) << "->" << QString::fromStdString(mask_t::maskTypeName(_dbb.maskType()));
  qInfo() << "state delta:" << stateDeltaPrev << "->" << _dbb.stateDelta();
  qInfo() << "save frames:" << saveFramesPrev << "->" << _dbb.saveFrames();
  qInfo() << "save generations:   " << saveGenerationsPrev  << "->" << _dbb.saveGenerations();
  qInfo() << "break loop on match:" << breakLoopOnMatchPrev << "->" << _dbb.breakLoopOnMatch();
  qInfo() << "load palette:       " << loadPalettePrev      << "->" << _dbb.loadPalette();
  qInfo() << "shuffle palette:    " << shufflePalettePrev   << "->" << _dbb.shufflePalette();
  qInfo() << "load gen 0:         " << loadGen0Prev         << "->" << _dbb.loadGen0();
  qInfo() << "rng:     " << _dbb.rng();
  qInfo() << "rng name:" << _dbb.rngName();
  qInfo() << "demon config string:  " << _dbb.demonConfigString();
  qInfo() << "demon config root dir:" << _dbb.demonConfigRootDir();
  qInfo() << "demon config dir:     " << _dbb.demonConfigDir();
  qInfo() << "demon data dir:       " << _dbb.demonDataDir();
}  // demonConfig::updateDemonConfig

void
demonConfig::saveDemonConfig() const {
  const QString demonConfigRootDir {_dbb.demonConfigRootDir()};
  const QString demonConfigLastRunFile {demonConfigRootDir + "lastRun"};
  QDir d {};

  if ( !d.exists(demonConfigRootDir) ) {
    _dbb.setLastRun(0);
    d.mkpath(demonConfigRootDir);

    // the booleans _createNewRun and _useLastRun do not affect this branch
    // because an initial lastRun file must be created if not existing
    qInfo() << "Creating initial demon config lastRun file:" << demonConfigLastRunFile;

    std::ofstream ofs(demonConfigLastRunFile.toStdString(), std::ios::binary);
    ofs << _dbb.lastRun();
    ofs.flush();
    ofs.close();
    qInfo() << "New run:" << _dbb.lastRun();
  }
  else {
    std::ifstream ifs(demonConfigLastRunFile.toStdString(), std::ios::binary);
    int lastRunStoredOnFile {};

    ifs >> lastRunStoredOnFile;
    ifs.close();

    if ( _dbb.createNewRun() ) {
      qInfo() << "A new run is created: Updating demon config lastRun file:" << demonConfigLastRunFile;
      std::ofstream ofs(demonConfigLastRunFile.toStdString(), std::ios::binary);
      _dbb.setLastRun(lastRunStoredOnFile + 1);
      ofs << _dbb.lastRun();
      ofs.flush();
      ofs.close();
      qInfo() << "New run:" << _dbb.lastRun();
    }
    else {
      _dbb.setLastRun(lastRunStoredOnFile);
      qInfo() << "Last run is used:" << _dbb.lastRun();
    }
  }

  _dbb.setDemonConfigRunDir(demonConfigRootDir + _dbb.getRunString());

  qInfo() << "Creating demon config run dir:" << _dbb.demonConfigRunDir();
  d.mkpath(_dbb.demonConfigRunDir());

  _dbb.setDemonConfigDir(_dbb.demonConfigRunDir() + "/demon.conf.d/");
  d.mkpath(_dbb.demonConfigDir());

  _dbb.setDemonDataDir(_dbb.demonConfigRunDir() + QString("/data-").append(_dbb.demonConfigString()).append(QString("/")));
  d.mkpath(_dbb.demonDataDir());

  const QString demonConfigFile {_dbb.demonConfigDir() + "demon.conf"};
  const QString maskFile {_dbb.demonConfigDir() + "mask"};

  qInfo() << "Saving demon config file:" << demonConfigFile;

  std::ofstream ofs(demonConfigFile.toStdString(), std::ios::binary);
  ofs << _dbb.rng() << "-"
      << _dbb.states() << "-"
      << _dbb.range() << "-"
      << _dbb.stateDelta() << "-"
      << _dbb.width() << "x" << _dbb.height()
      << "-" << _dbb.mask().maskTypeName();
  ofs.flush();
  ofs.close();

  qInfo() << "Saving mask file:" << maskFile;
  bool result {_dbb.mask().saveMask(maskFile.toStdString())};
  if ( false == result ) {
    qInfo() << "WARNING: Failed to save mask file" << maskFile;
  }
  else {
    std::cout << _dbb.mask();
  }

  if ( _dbb.loadPalette() ) {
    qInfo() << "The color palette will be loaded from an existing file in dir:" << _dbb.demonConfigDir();
  }
  else {
    qInfo() << "Creating palette and saving palette file (rgbhex) and palette image in" << _dbb.demonConfigDir();
    sg::palette::Palette p{static_cast<uint32_t>(_dbb.states())};
    if ( _dbb.shufflePalette() ) {
      p.shufflePalette();
    }
    result = p.saveRGBHexPalette(_dbb.demonConfigDir().toStdString());
    if ( false == result ) {
      qInfo() << "WARNING: Failed to save palette file (rgbhex)";
    }
    result = p.makePaletteImage(_dbb.demonConfigDir().toStdString());
    if ( false == result ) {
      qInfo() << "WARNING: Failed to save palette image file";
    }
  }
  qInfo() << " ";
}  // demonConfig::saveDemonConfig

void
demonConfig::openDemonsRootFolder() noexcept {
  QDir d;
  QDesktopServices::openUrl(QUrl("file:" + d.absolutePath(), QUrl::TolerantMode));
}  // demonConfig::openDemonsRootFolder
