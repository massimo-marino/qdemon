//
// demonconfig.h
//
#pragma once

#include "dbb.h"
#include "mask.h"
#include "sg/palette.h"

#include <QDialog>
#include <QPointer>

class QAbstractButton;

class MainWindow;
////////////////////////////////////////////////////////////////////////////////
namespace Ui {
class demonConfig;
}

class demonConfig : public QDialog
{
  Q_OBJECT

public:
  Dbb& _dbb;

  explicit demonConfig(QWidget *parent = nullptr);
  ~demonConfig();

  void openDemonsRootFolder() noexcept;
  void saveDemonConfig() const;

private:
  Ui::demonConfig *ui;

  QPointer<MainWindow> _mainWindow;

  void writeSettings() const;
  void readSettings();

signals:
  void demonConfigClosed();
  void demonConfigSaved();

private slots:
  void on_createNewRunCheckBoxChanged(int state);
  void on_useLastRunCheckBoxChanged(int state);
  void on_loadPaletteCheckBoxChanged(int state);
  void on_shufflePaletteCheckBoxChanged(int state);
  void updateMaskSize(int rangeValue) const noexcept;
  void updateMaskType(int index) const noexcept;
  void updateDemonConfig() const noexcept;
  void on_ClickSaveConfig() noexcept;
  void on_ClickClose(QAbstractButton *button) noexcept;
};  // class demonConfig
