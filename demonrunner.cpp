//
// demonrunner.cpp
//
#include "demonrunner.h"
#include "ui_demonrunner.h"
#include "mainwindow.h"

#include "util.h"

#include <QGraphicsScene>
#include <QImage>
#include <QPixmap>
#include <QMessageBox>
#include <QDateTime>
#include <QGraphicsView>
#include <QColor>
#include <QDir>
#include <QDebug>

#include <iostream>
#include <chrono>

#define ____STATIC____ static
////////////////////////////////////////////////////////////////////////////////
demonRunner::demonRunner(demonConfig& demonConfigWindow, QWidget *parent) :
  QDialog (parent),
  ui (new Ui::demonRunner),
  _mainWindow(qobject_cast<MainWindow*>(parent)),
  _demonConfigWindow(demonConfigWindow),
  _dbb(_demonConfigWindow._dbb),
  _drImage (QImage(_dbb.width(), _dbb.height(), QImage::Format_RGB32))
{
  qInfo() << "demonRunner::demonRunner: >>>>>>>>>> CTOR START";

  ui->setupUi(this);

  readSettings();
  demon::util::logSettings();

  setWindowTitle("demon runner " + _dbb.getRunString());

  _logFile.open(QFile::WriteOnly | QFile::Truncate);
  _logOut.setString(&_log, QFile::WriteOnly);

  _logOut << _dbb.demonConfigString() << "\n";

  _drLabelPtr = new DLabel(ui->demonRunnerScrollArea);
  _drLabelPtr->setMouseTracking(true);

  ui->runPushButton->setEnabled(true);
  ui->pauseContinuePushButton->setEnabled(false);
  ui->endPushButton->setEnabled(false);

  connect(ui->runPushButton,           &QPushButton::clicked, this, &demonRunner::runButton, Qt::DirectConnection);
  connect(ui->pauseContinuePushButton, &QPushButton::clicked, this, &demonRunner::pauseContinueButton, Qt::DirectConnection);
  connect(ui->endPushButton,           &QPushButton::clicked, this, &demonRunner::endButton, Qt::DirectConnection);
  connect(ui->shotPushButton,          &QPushButton::clicked, this, &demonRunner::shotButton, Qt::DirectConnection);
  connect(ui->viewFramesPushButton,    &QPushButton::clicked, this, &demonRunner::openFrames);
  connect(ui->viewConfigPushButton,    &QPushButton::clicked, this, &demonRunner::openConfig);
  connect(ui->viewShotsPushButton,     &QPushButton::clicked, this, &demonRunner::openShots);
  connect(this,                        &demonRunner::runDemonSig, this, &demonRunner::demonRun, Qt::DirectConnection);
  connect(this,                        &demonRunner::updateGraphicViewSig, this, &demonRunner::updateGraphicsView, Qt::DirectConnection);

  demonInit();

  qInfo() << "demonRunner::demonRunner: >>>>>>>>>> CTOR END";
}  // demonRunner::demonRunner

demonRunner::~demonRunner() {
  writeSettings();
  delete ui;

  qInfo() << "demonRunner::~demonRunner: Object destroyed";
}  // demonRunner::~demonRunner

void demonRunner::writeSettings() const noexcept {
  // config file in /home/<user-name>/.config/skylark
  demon::util::writeGeometryInSettings(*this, "demonRunner");
}

void demonRunner::readSettings() noexcept {
  demon::util::readGeometryFromSettings(*this, "demonRunner");
}

void
demonRunner::demonInit() noexcept {
  QDir d {};

  if ( false == d.exists(_demonFramesDir) ) {
    qInfo() << "Demon Frames directory:" << _demonFramesDir << "does NOT exist. It must be created";
    d.mkpath(_demonFramesDir);
    qInfo() << "Demon Frames directory:" << _demonFramesDir << "created";
  }

  if ( false == d.exists(_demonGenerationsDir) ) {
    qInfo() << "Demon Generations directory:" << _demonGenerationsDir << "does NOT exist. It must be created";
    d.mkpath(_demonGenerationsDir);
    qInfo() << "Demon Generations directory:" << _demonGenerationsDir << "created";
  }

  if ( false == d.exists(_shotsDir) ) {
    qInfo() << "Shots directory:" << _shotsDir << "does NOT exist. It must be created";
    d.mkpath(_shotsDir);
    qInfo() << "Shots directory:" << _shotsDir << "created";
  }

  qInfo() << "Demon Frames Directory:" << _demonFramesDir;
  qInfo() << "Demon Generations Directory:" << _demonGenerationsDir;
  qInfo() << "Shots Directory:" << _shotsDir;

  _generation = 0;
  _totalTimeSpan_sec = std::chrono::duration<double>{0};

  ui->descriptionLabel->setText(_dbb.demonConfigString());

  setWindowSize();
  initGraphicsView();

  if ( false == loadMask() ) {
    // error loading the mask file
    qInfo() << "ERROR: Mask file could not be loaded";
    ui->runPushButton->setEnabled(false);
    return;
  }

  if ( false == loadPalette() ) {
    // error loading the palette file
    qInfo() << "ERROR: Color palette file could not be loaded";
    ui->runPushButton->setEnabled(false);
    return;
  }

  const auto gen0FileName {_dbb.demonConfigDir().toStdString() + "gen-0.txt"};

  if ( false == _dbb.loadGen0() ) {
    // generate a random image using the palette _colorPalette and the chosen algorithm
    generateRandomImage();

    // save the gen-0 matrix
    _stateArrayOld.saveMatx(gen0FileName);

    qInfo() << "Saved Gen 0 file:" << QString::fromStdString(gen0FileName);
  }
  else {
    // load gen 0 file
    _stateArrayOld.loadMatx(gen0FileName);

    qInfo() << "Loaded Gen 0 file:" << QString::fromStdString(gen0FileName) << "->>> No Random Generation made <<<-";
  }

  // set the pixels in the image with colors from palette colorPalette_
  setRandomImage();
  _stateArraySwappable = _stateArrayNew = _stateArrayOld;

  // save gen 0 frame in the demon config dir
  _drImage.save(_dbb.demonConfigDir().append(getGenerationString()).append(".png"));

  // save gen 0 frame
  if ( _saveFrames ) {
    saveFrame();
  }

  // update/draw image
  updateGraphicsView();
}  // demonRunner::demonInit

void
demonRunner::start() noexcept {
  _logOut << "\n---------------------------\n"
          << QDateTime::currentDateTime().toString()
          << "\ndemon started "
          << _dbb.demonConfigString()
          << " - Neighbourhood: "
          << QString::fromStdString(mask_t::maskTypeName(_dbb.maskType()))
          << " - Break Loop On Match: "
          << ((_breakLoopOnMatch) ? "YES" : "NO")
          << "\n";
  qInfo() << "demon started";

  loadMask();
  std::cout << _mask;

  ui->runPushButton->setDisabled(true);
  ui->pauseContinuePushButton->setEnabled(true);
  ui->endPushButton->setEnabled(true);
  ui->shotPushButton->setEnabled(false);
  _status = demonStatus::RUNNING;

  emit runDemonSig();
}  // demonRunner::start

void
demonRunner::pause() const noexcept {
  _status = demonStatus::PAUSED;
  ui->pauseContinuePushButton->setText("CONTINUE");
  ui->shotPushButton->setEnabled(true);
  qInfo() << "demon paused";
}  // demonRunner::pause

void
demonRunner::restart() noexcept {
  _status = demonStatus::RUNNING;
  ui->pauseContinuePushButton->setText("PAUSE");
  ui->shotPushButton->setEnabled(false);
  emit runDemonSig();
  qInfo() << "demon restarted";
}  // demonRunner::restart

void
demonRunner::end() const noexcept {
  _status = demonStatus::ENDED;
  qInfo() << "demon ended";
  _logOut << "\n"
          << QDateTime::currentDateTime().toString()
          << "\ndemon ended - Total time: "
          << _totalTimeSpan_sec.count()
          << " sec\n---------------------------\n";
  // write the log when ended
  _logFile.write(_log.toStdString().c_str(), _log.size());
  _logFile.close();

  ui->runPushButton->setEnabled(false);
  ui->pauseContinuePushButton->setEnabled(false);
  ui->endPushButton->setEnabled(false);
  ui->shotPushButton->setEnabled(true);
  ui->statusLabel->setText(getDemonStatusString());
}  // demonRunner::end

void
demonRunner::saveFrame() const noexcept {
    auto fileName {_demonFramesDir};
    _drImage.save(fileName.append(getGenerationString()).append(".jpg"));
}

void
demonRunner::saveGeneration() const noexcept {
    auto generationFileName {_demonGenerationsDir};
    _stateArrayNew.saveMatx(generationFileName.append(getGenerationString()).append(".txt").toStdString());
}

constexpr
unsigned int
demonRunner::getGeneration() const noexcept {
    return _generation;
}

QString
demonRunner::getGenerationString(const int fieldWidth, const QChar filler) const noexcept {
    QString gen {"%1"};
    return gen.arg(getGeneration(), fieldWidth, 10, filler);
}

QString&
demonRunner::getDemonStatusString() const noexcept {
    return _demonStatusString[static_cast<unsigned int>(_status)];
}

constexpr
int
demonRunner::modmap(const int v, const int max) noexcept {
    // if v is in [0, max] then return v
    if ( (v >= 0) && (v <= max) )
    {
        return v;
    }
    // range = max - min + 1; here min is 0
    const int range {max + 1};

    // if v > max then return a map of v in [0,max]
    if ( v > max )
    {
        return (v % range);
    }

    // if v < 0 then return a map of v in [0,max]
    return ((range - (std::abs(v) % range)) % range);
}  // modmap

constexpr
float
demonRunner::map(const float x,
                 const float in_min,
                 const float in_max,
                 const float out_min,
                 const float out_max) noexcept {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}  // map

void
demonRunner::endButton() const {
    switch (_status)
    {
    case demonStatus::RUNNING:
    case demonStatus::PAUSED:
    {
        end();
    }
    break;

    default:
        break;
    }
}  // demonRunner::endButton

void
demonRunner::pauseContinueButton() {
    switch (_status)
    {
    case demonStatus::RUNNING:
    {
        pause();
    }
    break;

    case demonStatus::PAUSED:
    {
        restart();
    }
    break;

    default:
        break;
  }
}  // demonRunner::pauseContinueButton

void
demonRunner::runButton() {
  switch (_status)
  {
    case demonStatus::NOT_RUNNING:
    {
      start();
    }
    break;

    default:
    break;
  }
}  // demonRunner::runButton

void
demonRunner::demonRun() {
  do {
    generateNewState_yx();
    //generateNewState_xy();  // original code equivalent to that in processing that gives the same results

    if ( _saveFrames ) {
      saveFrame();
    }
    if ( _saveGenerations ) {
      saveGeneration();
    }
  } while ( demonStatus::RUNNING == _status );
}  // demonRunner::demonRun

// slower than generateNewState_yx()
[[maybe_unused]]
void
demonRunner::generateNewState_xy() noexcept {
  ____STATIC____ std::chrono::high_resolution_clock::time_point t1 {};

  t1 = std::chrono::high_resolution_clock::now();

  ____STATIC____ std::chrono::high_resolution_clock::time_point t2 {};
  ____STATIC____ std::chrono::duration<double> timeSpan_sec {};
  ____STATIC____ int k;
  //____STATIC____ int l;
  ____STATIC____ int x;
  ____STATIC____ int y;
  ____STATIC____ int kx;
  ____STATIC____ int ly;
  ____STATIC____ int kxMin;
  ____STATIC____ int kxMax;
  ____STATIC____ int lyMin;
  ____STATIC____ int lyMax;
  ____STATIC____ int maskX;
  const int maxX {_cols - 1};
  const int maxY {_rows - 1};
  ____STATIC____ unsigned int stateArrayOld_l_k;
  ____STATIC____ bool brokenLoop {false};
  ____STATIC____ sg::rgb::RGB c {};
  ____STATIC____ QColor qc {};

  qc.setAlpha(255);
  _stateChanges = 0;
//  _loops = 0;
  ++_generation;
  brokenLoop = false;

  // x is the column index
  // y is the row index
  for (x = 0; x < _cols; ++x) {
    kxMin = x - _range;
    kxMax = x + _range;

    for (y = 0; y < _rows; ++y) {
      lyMin = y - _range;
      lyMax = y + _range;

      for (kx = kxMin; kx <= kxMax; ++kx) {
        maskX = modmap(kx - kxMin, _maskSize);
        // k is a column index
        k = modmap(kx, maxX);

        for (ly = lyMin; ly <= lyMax; ++ly) {
//          ++_loops;
          if ( 0 == _mask[modmap(ly - lyMin, _maskSize)][maskX]
                //|| ((ly == y) && (kx == x))
             ) {
            continue;
          }
          // l is a row index
          //l = modmap(ly, maxY);
          stateArrayOld_l_k = _stateArrayOld[modmap(ly, maxY)][k];
          if ( stateArrayOld_l_k == ((_stateArrayOld[y][x] + _stateDelta) % _paletteSize) )
          //if ( stateArrayOld_l_k <= ((stateArrayOld_[y][x] + stateDelta_) % paletteSize_) )
          //if ( stateArrayOld_l_k >= ((stateArrayOld_[y][x] + stateDelta_) % paletteSize_) )
          {
            _stateArrayNew[y][x] = stateArrayOld_l_k;

            c = _colorPalette()[stateArrayOld_l_k];
            //qc.setAlpha(c.Alpha());
            //qc.setAlpha(255);
            qc.setRed(c.Red());
            qc.setGreen(c.Green());
            qc.setBlue(c.Blue());
            //qc = qc.toHsl();

            _drImage.setPixelColor(x, y, qc);

            ++_stateChanges;
            // break ly loop
            if ( _breakLoopOnMatch ) {
              brokenLoop = true;
              break;
            }
          }
        }  // end for ly
        // break kx loop
        if ( brokenLoop ) {
          brokenLoop = false;
          break;
        }
      }  // end for kx
      _stateArraySwappable[y][x] = _stateArrayNew[y][x];
    }  // end for y
    QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers, 1);
  }  // end for x

////////////////////////////////////////////////////////////////////////////////
  // draw new image, if configured
  if ( _updateImageOnScreen ) {
    updateGraphicsView();
  }
////////////////////////////////////////////////////////////////////////////////
  // copy the state array
//  t3 = std::chrono::high_resolution_clock::now();
  _stateArrayOld.swap(_stateArraySwappable);
  //_stateArrayOld = _stateArrayNew;  // copy takes a long time for big matx's

  t2 = std::chrono::high_resolution_clock::now();
  timeSpan_sec = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
  _totalTimeSpan_sec += timeSpan_sec;
  _timeSpan_msec = timeSpan_sec.count() * 1000;

////////////////////////////////////////////////////////////////////////////////
//  timeSpan_sec = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t3);
//  std::cout << "Copy of state array took (ms): " << timeSpan_sec.count() * 1000 << std::endl;
////////////////////////////////////////////////////////////////////////////////

  if ( 0 == _stateChanges ) {
    // no state changes: ended
    _status = demonStatus::ENDED;
    ui->pauseContinuePushButton->setEnabled(false);
    ui->endPushButton->setEnabled(false);

    qInfo() << "demon ended";
    updateInfoLabels();
    _logOut << "\n"
            << QDateTime::currentDateTime().toString()
            << "\ndemon ended - Total time: "
            << _totalTimeSpan_sec.count()
            << " sec\n---------------------------\n";
  }
  else if ( _stateChanges == _previousStateChanges ) {
    if ( true == _breakLoopOnMatch ) {
      _breakLoopOnMatch = false;
      qInfo() << _generation << ": _breakLoopOnMatch set to false";
    }
    // paused
    pause();
    updateInfoLabels();
  }
  else {
    updateInfoLabels();
  }

  _previousStateChanges = _stateChanges;
}  // demonRunner::generateNewState_xy

// faster than generateNewState_xy()
void
demonRunner::generateNewState_yx() const noexcept {
  ____STATIC____ std::chrono::high_resolution_clock::time_point t1 {};

  t1 = std::chrono::high_resolution_clock::now();

  ____STATIC____ std::chrono::high_resolution_clock::time_point t2 {};
//  ____STATIC____ std::chrono::high_resolution_clock::time_point t3 {};
  ____STATIC____ std::chrono::duration<double> timeSpan_sec {};
  //____STATIC____ int k;
  ____STATIC____ int l;
  ____STATIC____ int x;
  ____STATIC____ int y;
  ____STATIC____ int kx;
  ____STATIC____ int ly;
  ____STATIC____ int kxMin;
  ____STATIC____ int kxMax;
  ____STATIC____ int lyMin;
  ____STATIC____ int lyMax;
  //____STATIC____ int maskX;
  ____STATIC____ int maskY;
  const int maxX {_cols - 1};
  const int maxY {_rows - 1};
  ____STATIC____ unsigned int stateArrayOld_l_k;
  ____STATIC____ bool brokenLoop {false};
  ____STATIC____ sg::rgb::RGB c {};
  ____STATIC____ QColor qc {};

  qc.setAlpha(255);
  _stateChanges = 0;
//  _loops = 0;
  ++_generation;
  brokenLoop = false;

  // x is the column index
  // y is the row index
  for (y = 0; y < _rows; ++y) {
    lyMin = y - _range;
    lyMax = y + _range;

    for (x = 0; x < _cols; ++x) {
      kxMin = x - _range;
      kxMax = x + _range;

      // kx is a column index
      // ly is a row index
      for (ly = lyMin; ly <= lyMax; ++ly) {
        maskY = modmap(ly - lyMin, _maskSize);
        //l is a row index
        l = modmap(ly, maxY);

        for (kx = kxMin; kx <= kxMax; ++kx) {
          //maskX = modmap(kx - kxMin, maskSize_);
          // k is a column index
          //k = modmap(kx, maxX);

          // ++_loops;
          if ( 0 == _mask[maskY][modmap(kx - kxMin, _maskSize)]
                // || ((ly == y) && (kx == x))
             ) {
            continue;
          }
          stateArrayOld_l_k = _stateArrayOld[l][modmap(kx, maxX)];
          if ( stateArrayOld_l_k == ((_stateArrayOld[y][x] + _stateDelta) % _paletteSize) )
//          if ( stateArrayOld_l_k <= ((stateArrayOld_[y][x] + stateDelta_) % paletteSize_) )
//          if ( stateArrayOld_l_k >= ((stateArrayOld_[y][x] + stateDelta_) % paletteSize_) )
          {
            _stateArrayNew[y][x] = stateArrayOld_l_k;

            c = _colorPalette()[stateArrayOld_l_k];
            //qc.setAlpha(c.Alpha());
            //qc.setAlpha(255);
            qc.setRed(c.Red());
            qc.setGreen(c.Green());
            qc.setBlue(c.Blue());
            //qc = qc.toHsl();

            _drImage.setPixelColor(x, y, qc);

            ++_stateChanges;
            // break kx loop
            if ( _breakLoopOnMatch ) {
              brokenLoop = true;
              break;
            }
          }
        }  // end for kx
        // break ly loop
        if ( brokenLoop ) {
          brokenLoop = false;
          break;
        }
      }  // end for ly
      _stateArraySwappable[y][x] = _stateArrayNew[y][x];
    }  // end for x
    QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers, 1);
  }  // end for y

////////////////////////////////////////////////////////////////////////////////
  // draw new image, if configured
  if ( _updateImageOnScreen ) {
    updateGraphicsView();
  }
////////////////////////////////////////////////////////////////////////////////
  // copy the state array
//  t3 = std::chrono::high_resolution_clock::now();
  _stateArrayOld.swap(_stateArraySwappable);
//  _stateArrayOld = _stateArrayNew;  // copy takes a long time for big matx's

  t2 = std::chrono::high_resolution_clock::now();
  timeSpan_sec = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
  _totalTimeSpan_sec += timeSpan_sec;
  _timeSpan_msec = timeSpan_sec.count() * 1000;

////////////////////////////////////////////////////////////////////////////////
//  timeSpan_sec = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t3);
//  std::cout << "Copy of state array took (ms): " << timeSpan_sec.count() * 1000 << std::endl;
////////////////////////////////////////////////////////////////////////////////

  if ( 0 == _stateChanges ) {
    // no state changes: ended
    _status = demonStatus::ENDED;
    ui->pauseContinuePushButton->setEnabled(false);
    ui->endPushButton->setEnabled(false);

    qInfo() << "demon ended";
    _logOut << "\n"
            << QDateTime::currentDateTime().toString()
            << "\ndemon ended - Total time: "
            << _totalTimeSpan_sec.count()
            << " sec\n---------------------------\n";
  }
//  else if ( _stateChanges == _previousStateChanges )
//  {
//    if ( true == _breakLoopOnMatch )
//    {
//      _breakLoopOnMatch = false;
//      std::cout << _generation << ": _breakLoopOnMatch set to false" << std::endl;
//    }
//    // paused
//    pause();
//  }
  updateInfoLabels();
  _previousStateChanges = _stateChanges;
}  // demonRunner::generateNewState_yx

void
demonRunner::initGraphicsView() const noexcept {
  qInfo() << "demonRunner: initializing graphics view";

  _drImage.fill(QColor(128, 64, 255, 0));
  _drPixmap = QPixmap::fromImage(_drImage);
  _drLabelPtr->setPixmap(_drPixmap);
  _drLabelPtr->resize(_drPixmap.width(), _drPixmap.height());
  ui->demonRunnerScrollArea->setBackgroundRole(QPalette::Dark);
  ui->demonRunnerScrollArea->setWidget(_drLabelPtr);
  ui->demonRunnerScrollArea->resize(_drImage.width() + 5, _drImage.height() + 5);
}  // initGraphicsView

void
demonRunner::updateGraphicsView() const {
  // update image, and GraphicsView
  _drPixmap = QPixmap::fromImage(_drImage);
  _drLabelPtr->setPixmap(_drPixmap);

//  QApplication::processEvents(QEventLoop::AllEvents, 1);
}  // demonRunner::updateGraphicsView

void
demonRunner::setWindowSize() noexcept {
  const auto w {std::min(static_cast<unsigned int>(width()), _dbb.width() + 50u)};
  const auto h {std::min(600u, _dbb.height() + 100)};
  resize(w, h);
  qInfo() << "Window dimension:" << width() << "x" << height();
}  // demonRunner::setWindowSize

bool
demonRunner::loadMask() const noexcept {
  const bool result {_mask.loadMask(_dbb.demonConfigDir().append("mask").toStdString())};
  if ( false == result ) {
    qInfo() << "ERROR: Failed to load mask file";
  }
  else {
    qInfo() << "Mask file loaded";
  }
  qInfo() << " ";
  return result;
}  // demonRunner::loadMask

bool
demonRunner::loadPalette(const std::string& fileNameExtension) const noexcept {
  const std::string states {std::to_string(_dbb.states())};
  const std::string mode {((_dbb.shufflePalette()) ? "shuffled-" : "")};
  const std::string paletteFileName {_dbb.demonConfigDir().toStdString() + "rgbhex-" + mode + "palette-" + states + fileNameExtension};
  qInfo() << "Loading palette file:" << QString::fromStdString(paletteFileName);

  const bool result {_colorPalette.loadHexPaletteFromFile(paletteFileName)};
  if ( false == result ) {
    qInfo() << "ERROR: Failed to load color palette";
  }
  else {
    qInfo() << "Color palette file loaded";
  }
  qInfo() << " ";
  return result;
}  // demonRunner::loadPalette

void
demonRunner::generateRandomImage() const noexcept {
  unsigned int paletteMaxIndex {};

  switch ( _dbb.rng() )
  {
    // random noise 1 - rn
    case Dbb::RANDOM_NUMBER_GENERATOR_1:
    {
      paletteMaxIndex = _paletteSize - 1;

      //std::cout << "stateArrayOld before randomize with RN1:\n" << _stateArrayOld << std::endl;

      std::function<size_t()> rgenfun = [paletteMaxIndex] ()
      {
        // set the pixel in the image with color from palette p
        // TODO

        return utilities::getRandomINT<size_t>(0, paletteMaxIndex);
      };  // rgenfun

      _stateArrayOld.randomize(rgenfun);

      //std::cout << "stateArrayOld after randomize with RN1:\n" << _stateArrayOld << std::endl;
    }
    break;

    case Dbb::RANDOM_NUMBER_GENERATOR_2:
    {
      paletteMaxIndex = _paletteSize;

      //std::cout << "stateArrayOld before randomize with RN2:\n" << _stateArrayOld << std::endl;

      std::function<size_t(size_t, size_t)> rgenfun = [paletteMaxIndex, this] (size_t x, size_t y)
      {
        // random noise

        // half/full-framing: due to a bug in the original code in Processing:
        // without this framing the algorithm behaves a lot different with
        // different graphical results/features
        //

        // original half-framing
        //if ( (0 == x) || (0 == y) )

        // full-framing
        //if ( (0 == x) || (0 == y) || (x == (_cols - 1)) || (y == (_rows - 1)) )

        // full-framing plus diagonal
//        if ( (0 == x) || (0 == y) || (x == (_rows - y)) || (x == y) || (x == (_cols - 1)) || (y == (_rows - 1))
//             || (y == static_cast<size_t>(x * (2 + sin(1.0/(x + 2.0)))))
//             || (x == static_cast<size_t>(y * (2 + sin(1.0/(x + 2.0)))))
//             || ((x - _cols/2)*(x - _cols/2) + (y - _rows/2)*(y - _rows/2) < 40000)  // circle
//             || ((x - y/2)*(y - x/2) + (y - y/3)*(y - y/2) < 40000)  // arrow 1
//             || ((x - y/2)*(y - x/2) + (x - y/3)*(y - y/2) < 40000)  // arrow 2
//             || ((y*y + x*x < 5000) && (y*y + x*x > 4500)) )
        //{
        //  return static_cast<size_t>(paletteMaxIndex / 2);
        //}
        // end of half/full-framing
        //return static_cast<size_t>(0);
        ////////////////////////////////////////////////////////////////////////

        const float M {2.0f};
        float v = M * (x + 1) * (y + 1);  // <<--- THIS IS REALLY INTERESTING
        auto rn = utilities::getRandomFP<float>(0, v);
        float r = map(rn, 0.0f, v, 0, paletteMaxIndex);

        // set the pixel in the image with color from palette p
        // TODO

        return static_cast<size_t>(r);
      };  // rgenfun

      _stateArrayOld.randomize(rgenfun);

      //std::cout << "stateArrayOld after randomize with RN2:\n" << _stateArrayOld << std::endl;
    }
    break;

    case Dbb::RANDOM_NUMBER_GENERATOR_3:
    {
       paletteMaxIndex = _paletteSize;

      //std::cout << "stateArrayOld before randomize with RN3:\n" << _stateArrayOld << std::endl;

      std::function<size_t(size_t, size_t)> rgenfun = [paletteMaxIndex] (size_t x, size_t y)
      {
        // random noise
        const float M {2.0f};
        auto Kx = x + 1;
        auto Ky = y + 2;
        float v = M * (Kx / Ky);  // <<--- THIS IS FUNNY [red\chaos]
        auto rn = utilities::getRandomFP<float>(0, v);
        float r = map(rn, 0.0, v, 0, paletteMaxIndex);

        // set the pixel in the image with color from palette p
        // TODO

        return static_cast<size_t>(r);
      };  // rgenfun

      _stateArrayOld.randomize(rgenfun);

      //std::cout << "stateArrayOld after randomize with RN3:\n" << _stateArrayOld << std::endl;
    }
    break;

    case Dbb::RANDOM_NUMBER_GENERATOR_4:
    {

    }
    break;

    case Dbb::RANDOM_NUMBER_GENERATOR_T:
    {

    }
    break;

    case Dbb::PERLIN_NOISE_1:
    {

    }
    break;

    case Dbb::PERLIN_NOISE_2:
    {

    }
    break;

    case Dbb::PERLIN_NOISE_3:
    {

    }
    break;
  }
}  // demonRunner::generateRandomImage

void
demonRunner::setRandomImage() const noexcept{
  // set the pixels in the image with colors from palette colorPalette_
  sg::rgb::RGB c {};
  QColor qc {};
  size_t y {0};
  size_t x {0};
  for (y = 0; y < _stateArrayOld.height(); ++y) {
    for (x = 0; x < _stateArrayOld.width(); ++x) {
      c = _colorPalette()[_stateArrayOld[y][x]];
      qc.setAlpha(c.Alpha());
      qc.setRed(c.Red());
      qc.setGreen(c.Green());
      qc.setBlue(c.Blue());
      _drImage.setPixelColor(x, y, qc);
    }
  }
}  // demonRunner::setRandomImage

void
demonRunner::updateInfoLabels() const noexcept {
  const auto g {getGenerationString(5, ' ')};
  const auto sc {nf(_stateChanges, 9, ' ')};
  const auto t {nf(_timeSpan_msec, 6, ' ')};
//  const auto l {nf(_loops, 9, ' ')};
  const auto& status {getDemonStatusString()};

  _logOut << qSetFieldWidth(6)  << g
          << qSetFieldWidth(10) << sc
          << qSetFieldWidth(10) << t
//          << qSetFieldWidth(10) << l
          << qSetFieldWidth(15) << status
          << "\n";

  ui->generationLabel->setText("Gen: " + g);
  ui->stateChangesLabel->setText("St. changes: " + sc);
  ui->durationLabel->setText("T(ms): " + t);
  ui->statusLabel->setText(status);

  QApplication::processEvents(QEventLoop::ExcludeSocketNotifiers, 1);
}  // demonRunner::updateInfoLabels

void
demonRunner::shotButton() {
  auto shotFileName {_shotsDir};
  auto generationFileName {_shotsDir};
  shotFileName = shotFileName.append(getGenerationString()).append(".png");
  _drImage.save(shotFileName);
  _stateArrayOld.saveMatx(generationFileName.append(getGenerationString()).append(".txt").toStdString());
  QMessageBox::information(this,
                           "shot saved",
                           "Shot\n" + shotFileName + "\nwas saved.",
                           QMessageBox::Ok);
}  // demonRunner::shotButton

void
demonRunner::openFrames() const {
  demon::util::openFolder(_mainWindow->getDemonRootDir() + _demonFramesDir);
}  // demonRunner::openFrames

void
demonRunner::openConfig() const {
  _dbb.openDemonConfigFolder();
}  // demonRunner::openConfig

void
demonRunner::openShots() const {
  demon::util::openFolder(_mainWindow->getDemonRootDir() + _shotsDir);
}  // demonRunner::openShots
