//
// demonrunner.h
//
#pragma once

#include <QDialog>
#include <QTextStream>
#include <QPointer>
#include <QFile>

#include "DLabel.h"
#include "matx.h"
#include "demonconfig.h"

class QGraphicsScene;
class QImage;
class QPixmap;

class MainWindow;
////////////////////////////////////////////////////////////////////////////////
namespace Ui {
class demonRunner;
}

class demonRunner : public QDialog
{
  Q_OBJECT

public:
  enum class demonStatus : unsigned { NOT_RUNNING = 0,
                                      RUNNING,
                                      PAUSED,
                                      ENDED };
  Q_ENUM(demonStatus)

  explicit demonRunner(demonConfig& demonConfigWindow, QWidget *parent = nullptr);
  ~demonRunner();

private:
  Ui::demonRunner *ui;

  mutable QPointer<MainWindow> _mainWindow;
  demonConfig& _demonConfigWindow;
  Dbb& _dbb;

  mutable QString _log {};
  mutable QTextStream _logOut {};
  mutable QFile _logFile {_dbb.demonConfigDir().append("demonRunner.log")};

  mutable DLabel* _drLabelPtr {nullptr};
  mutable QImage _drImage {nullptr};
  mutable QPixmap _drPixmap {};

  mutable std::vector<QString> _demonStatusString {"NOT RUNNING", "RUNNING", "PAUSED", "ENDED"};

  const int _cols {static_cast<int>(_dbb.width())};
  const int _rows {static_cast<int>(_dbb.height())};
  const int _range {static_cast<int>(_dbb.range())};
  const unsigned int _stateDelta {_dbb.stateDelta()};
  const unsigned int _paletteSize {_dbb.states()};
  mutable bool _breakLoopOnMatch {_dbb.breakLoopOnMatch()};
  const bool _saveFrames {_dbb.saveFrames()};
  const bool _saveGenerations {_dbb.saveGenerations()};
  const bool _updateImageOnScreen {true};
  mutable unsigned int _generation {0};
  mutable int _stateChanges {0};
  mutable int _previousStateChanges {-1};
  mutable unsigned int _timeSpan_msec {0};
  mutable std::chrono::duration<double> _totalTimeSpan_sec {0};
//  unsigned int _loops {0};
  mutable demonStatus _status {demonStatus::NOT_RUNNING};

  mutable sg::palette::Palette _colorPalette {};
  mutable unsigned int _maskSize {_dbb.computeMaskSize()};
  mutable mask_t _mask {_maskSize};

  mutable demon::matx<size_t> _stateArraySwappable {_dbb.height(), _dbb.width()};
  mutable demon::matx<size_t> _stateArrayOld {_dbb.height(), _dbb.width()};
  mutable demon::matx<size_t> _stateArrayNew {_dbb.height(), _dbb.width()};

  const QString _demonFramesDir {_dbb.demonDataDir().append("frames.d/")};
//  const QString _demonGenerationsDir {demonConfig::demonConfigRunDir().append("/generations.d/")};
  const QString _demonGenerationsDir {_dbb.demonDataDir().append("generations.d/")};
//  const QString _shotsDir {demonConfig::demonConfigRunDir().append("/shots.d/")};
  const QString _shotsDir {_dbb.demonDataDir().append("shots.d/")};

  void writeSettings() const noexcept;
  void readSettings() noexcept;

  void demonInit() noexcept;
  void initGraphicsView() const noexcept;
  //inline void updateGraphicsView() const;
  void setWindowSize() noexcept;
  bool loadMask() const noexcept;
  bool loadPalette(const std::string& fileNameExtension = ".txt") const noexcept;
  void generateRandomImage() const noexcept;
  void setRandomImage() const noexcept;
  [[maybe_unused]] void generateNewState_xy() noexcept;
  void generateNewState_yx() const noexcept;
  inline void updateInfoLabels() const noexcept;
  void start() noexcept;
  void pause() const noexcept;
  void restart() noexcept;
  void end() const noexcept;

  void saveFrame() const noexcept;
  void saveGeneration() const noexcept;

  constexpr unsigned int getGeneration() const noexcept;
  QString getGenerationString (const int fieldWidth = 5, const QChar filler = '0') const noexcept;

  QString& getDemonStatusString() const noexcept;

  // if v is in [0, max] then return v
  // if v < 0 or v > max then return a map of v in [0,max]
  constexpr static int modmap (const int v, const int max) noexcept;

  // similar to Processing's map(): https://processing.org/reference/map_.html
  constexpr static float map(const float x,const float in_min, const float in_max, const float out_min, const float out_max) noexcept;

  // Similar to Processing's nf(): https://processing.org/reference/nf_.html
  template <typename T>
  QString
  nf (T v, const int fieldWidth, QChar filler = '0') const noexcept
  {
      QString gen {"%1"};
      return gen.arg(v, fieldWidth, 10, filler);
  }

signals:
  void runDemonSig();
  void updateGraphicViewSig();

private slots:
  void demonRun();
  void runButton();
  void pauseContinueButton();
  void endButton() const;
  void shotButton();
  void openFrames() const;
  void openConfig() const;
  void openShots() const;
  void updateGraphicsView() const;
};  // class demonRunner
