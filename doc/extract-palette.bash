extract-palette()
{
  convert "$1" -resize 300x -dither None -unique-colors -colors "$2" txt: | \
  tail -n +2 | \
  tr -s ' ' | \
  cut -d ' ' -f 3 | \
  sort | \
  uniq -c | \
  sort -rn | \
  tr -s ' ' | \
  cut -d ' ' -f 3;
}

palette-scheme()
{
  convert "$1" -geometry 16x16 -colors "$2" -unique-colors -scale 4000% "$1"-palette-"$2".jpg;
}

list-hex-colors()
{
convert "$1" -unique-colors -depth 8 txt:- | tail -n +2 | tr -s ' ' | cut -d ' ' -f 3;
}

list-rgb-colors()
{
convert "$1" -unique-colors -depth 8 txt:- | tail -n +2 | tr -s ' ' | cut -d ' ' -f 2 | sed -r 's/[\)\(]//g' | sed -r 's/,/ /g';
}

list-all-colors()
{
convert "$1" -unique-colors -define histogram:unique-colors=true -format "%c" histogram:info:
}

