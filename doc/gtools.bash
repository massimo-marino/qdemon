#!/bin/bash
#
## Useful bash functions
#
################################################################################
extract-palette()
{
  convert "$1" -resize 300x -dither None -unique-colors -colors "$2" txt: | \
  tail -n +2 | \
  tr -s ' ' | \
  cut -d ' ' -f 3 | \
  sort | \
  uniq -c | \
  sort -rn | \
  tr -s ' ' | \
  cut -d ' ' -f 3;
}

palette-scheme()
{
  convert "$1" -geometry 16x16 -colors "$2" -unique-colors -scale 4000% "$1"-palette.jpg;
}

list-hex-colors()
{
convert "$1" -unique-colors -depth 8 txt:- | tail -n +2 | tr -s ' ' | cut -d ' ' -f 3;
}

list-rgb-colors()
{
convert "$1" -unique-colors -depth 8 txt:- | tail -n +2 | tr -s ' ' | cut -d ' ' -f 2 | sed -r 's/[\)\(]//g' | sed -r 's/,/ /g';
}

list-all-colors()
{
convert "$1" +dither -define histogram:unique-colors=true -format "%c" histogram:info:
}
#############################################################################
# Convert hex string to rgb
# @param 1 (String) 3 or 6 character hex string
#   Case insensitive, leading # optional (01a, fff1b1, #ABB)
# @param 2 (Float) optional float from 0 to 1
#   If provided, outputs an rgba() string
#
# $ hex2rgb FA0133
# rgb(250,1,51)
# $ hex2rgb FA0133 .5
# rgba(250,1,51,.5)
function hex2rgb()
{
    local css=false
    local printstring
    local hex="$(tr '[:lower:]' '[:upper:]' <<< ${1#\#})"

    # Convert ABC to AABBCC
    if [[ $hex =~ ^[A-F0-9]{3}$ ]]; then
        hex=$(sed -e 's/\(.\)/\1\1/g' <<< $hex)
    fi

    # If the first param is a valid hex string, convert to rgb
    if [[ $hex =~ ^[A-F0-9]{6}$ ]]; then
        # If second param exists and is a float between 0 and 1, output rgba
        if [[ -n $2 && $2 =~ ^(0?\.[0-9]+|1(\.0)?)$ ]]; then
            if ( $css ); then
              printstring="rgba(%d,%d,%d,%s)\n";
            else
              printstring="%d %d %d %s\n";
            fi
            printf "$printstring" 0x${hex:0:2} 0x${hex:2:2} 0x${hex:4:2} $2;
        else
            if ( $css ) then
              printstring="rgb(%d,%d,%d)\n":
            else
              printstring="%d %d %d\n";
            fi
            printf "$printstring" 0x${hex:0:2} 0x${hex:2:2} 0x${hex:4:2};
        fi
    # If it's not valid hex, return the original string
    else
        echo -n $@
    fi
}

function convert-file-hex2rgb()
{
  function usage()
  {
    echo "usage: convert-file-hex2rgb hex-file rgb-file";
  }
  if [ "" == "$1" ] ; then
    usage;
    return 1;
  fi
  if [ "" == "$2" ] ; then
    usage;
    return 1;
  fi

  local in_file=$1;
  local out_file=$2;
  local line;

  rm -fiv $out_file;
  
  while read line; do
    h=$(hex2rgb "$line");
    echo "$h" >> $out_file;
  done < $in_file

return $?
}
