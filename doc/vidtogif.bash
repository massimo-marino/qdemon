#!/bin/bash
## Author: Montana Flynn
## Updated: 2014-10-08
    
# Define constants
NAME="vidtogif"
VERSION="$NAME version 1.0"
GREEN="\033[0;32m"
YELLOW="\033[0;33m"
RED="\033[0;31m"
NORMAL="\033[0m"

# Colors
end="\033[0m"
black="\033[0;30m"
blackb="\033[1;30m"
white="\033[0;37m"
whiteb="\033[1;37m"
red="\033[0;31m"
redb="\033[1;31m"
green="\033[0;32m"
greenb="\033[1;32m"
yellow="\033[0;33m"
yellowb="\033[1;33m"
blue="\033[0;34m"
blueb="\033[1;34m"
purple="\033[0;35m"
purpleb="\033[1;35m"
lightblue="\033[0;36m"
lightblueb="\033[1;36m"

function black {
  echo -e "${black}${1}${end}"
}

function blackb {
  echo -e "${blackb}${1}${end}"
}

function white {
  echo -e "${white}${1}${end}"
}

function whiteb {
  echo -e "${whiteb}${1}${end}"
}

function red {
  echo -e "${red}${1}${end}"
}

function redb {
  echo -e "${redb}${1}${end}"
}

function green {
  echo -e "${green}${1}${end}"
}

function greenb {
  echo -e "${greenb}${1}${end}"
}

function yellow {
  echo -e "${yellow}${1}${end}"
}

function yellowb {
  echo -e "${yellowb}${1}${end}"
}

function blue {
  echo -e "${blue}${1}${end}"
}

function blueb {
  echo -e "${blueb}${1}${end}"
}

function purple {
  echo -e "${purple}${1}${end}"
}

function purpleb {
  echo -e "${purpleb}${1}${end}"
}

function lightblue {
  echo -e "${lightblue}${1}${end}"
}

function lightblueb {
  echo -e "${lightblueb}${1}${end}"
}

function colors {
  black "black"
  blackb "blackb"
  white "white"
  whiteb "whiteb"
  red "red"
  redb "redb"
  green "green"
  greenb "greenb"
  yellow "yellow"
  yellowb "yellowb"
  blue "blue"
  blueb "blueb"
  purple "purple"
  purpleb "purpleb"
  lightblue "lightblue"
  lightblueb "lightblueb"
}

# Check Dependencies
declare -a arry=('convert' 'ffmpeg')
for i in "${arry[@]}"
do
    command -v $i >/dev/null 2>&1 || { 
        if [ "$i" = "convert" ]; then 
            echo >&2 "${RED}$NAME requires imagemagick"; 
        else
            echo >&2 "${RED}$NAME requires $i"; 
        fi;
        exit 1; 
    }
done

# Function to print usage 
print_usage() {
    USAGE="
Usage: 
    $NAME [-r -w <integer> -h <integer> -u -v] input.mp4 output.gif

Options: 
    -r: Framerate per second <integer>
    -w: Set max width <integer>
    -h: Set max height <integer>
    -u: Print usage information
    -v: Version

Example:
    Example: $NAME -r 10 -w 600 input.mp4 output.gif

License: 

    This is free software. You may redistribute copies of it under the terms
    of the GNU General Public License <http://www.gnu.org/licenses/gpl.html>.
    There is NO WARRANTY, to the extent permitted by law.
    "
    echo -e "\\n ${yellowb}$USAGE${end}"
}

# Set default framerate per second
FPS=10

# Function to change FPS from option
set_fps() {
    if [ "$1" -eq "$1" ] 2>/dev/null; then
      FPS=$1
    else
      echo -e "\\n${RED}Rate per second must be a number${NORMAL}"
      print_usage
      exit 1
    fi
}

# Check for optional width or heights and apply to scale
SCALE=""
WIDTH=""
HEIGHT=""

set_width() {
    if [ "$1" -eq "$1" ] 2>/dev/null; then
      WIDTH=$1
      set_scale
    else
      echo -e "\\n${RED}Width must be a number${NORMAL}"
      print_usage
      exit 1
    fi
}

set_height() {
    if [ "$1" -eq "$1" ] 2>/dev/null; then
      HEIGHT=$1
      set_scale
    else
      echo -e "\\n${RED}Height must be a number${NORMAL}"
      print_usage
      exit 1
    fi
}

set_scale() {
    if [ -z != $WIDTH ] && [ -z != $HEIGHT ]; then
        SCALE="-vf scale=$WIDTH:$HEIGHT"
    elif [ -z != $WIDTH ]; then
        SCALE="-vf scale=$WIDTH:-1"
    elif [ -z != $HEIGHT ]; then
        SCALE="-vf scale=-1:$HEIGHT"
    fi
}

# Parse optional arguments
while getopts r:w:h:uv OPT; do
    case "$OPT" in
        u)
            print_usage
            exit 0
            ;;
        v)
            echo $VERSION
            exit 0
            ;;
        r)
            set_fps $OPTARG
            ;;
        w)
            set_width $OPTARG
            ;;
        h)
            set_height $OPTARG
            ;;
        ?)
            print_usage
            exit 1
            ;;
    esac
done

# Remove the option flags from args
shift `expr $OPTIND - 1`

# We need two args for input and ouput
if [ $# != 2 ]; then
    echo -e "\n\n${red}Missing input and output files${end}"
    print_usage
    exit 1
fi

echo -e "Converting ${YELLOW}$1${NORMAL} to ${YELLOW}$2${NORMAL}"
VIDPATH=$(cd $(dirname $1); pwd)/$(basename $1)
GIFPATH=$(cd $(dirname $2); pwd)/$(basename $2)

echo -e "Created temporary image directory at /tmp/frames"
mkdir /tmp/frames && cd /tmp/frames

echo -e "Converting video to images at ${YELLOW}$FPS${NORMAL} FPS"
ffmpeg -loglevel error -i $VIDPATH -r $FPS $SCALE ./ffout%03d.png

echo -e "Turning individual images into animated gif"
convert -loop 0 ./ffout*.png tmp.gif
convert -layers Optimize tmp.gif $GIFPATH

echo -e "Cleaning up temporary image file directory"
rm -rf /tmp/frames

SIZE=`ls -lah $GIFPATH | awk '{ print $5}'`
echo -e "${GREEN}Gif ($SIZE) ready at $GIFPATH"
