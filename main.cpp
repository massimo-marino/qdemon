//
// main.cpp
//
#include "mainwindow.h"
#include <QApplication>
#include <QDir>
#include <QDebug>
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char *argv[])
{
//  QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
//  QApplication::setAttribute(Qt::AA_DisableHighDpiScaling);
  QApplication::setAttribute(Qt::AA_Use96Dpi);
//  QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
  QApplication::setAttribute(Qt::AA_NativeWindows);

  QGuiApplication::setHighDpiScaleFactorRoundingPolicy(Qt::HighDpiScaleFactorRoundingPolicy::Floor);

  // config file in /home/<user-name>/.config/skylark
  QCoreApplication::setOrganizationName("skylark");
  QCoreApplication::setOrganizationDomain("skylark.com");
  QCoreApplication::setApplicationName("qdemon");
  QCoreApplication::setApplicationVersion("1.0.0");

  QApplication a(argc, argv);

  qInfo() << __func__ << ": at startup the currentPath is: '" << QDir::currentPath();

  // open and set style sheet
  {
    QFile f( "../demon.qss" );
    if ( f.open(QFile::ReadOnly) )
    {
      QString ssheet = QLatin1String(f.readAll());
      a.setStyleSheet(ssheet);
    }
    else
    {
      qInfo() << __func__ << ": demon.qss not loaded";
    }
  }

  MainWindow w(nullptr, QDir::currentPath());
  qInfo() << __func__ << ": currentPath/working dir set to: '" << QDir::currentPath();
  w.show();

  return QApplication::exec();
}
