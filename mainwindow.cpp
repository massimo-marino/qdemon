//
// mainwindow.cpp
//
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "demonconfig.h"
#include "demonrunner.h"
#include "palettemanager.h"

#include "util.h"

#include <QFileDialog>
#include <QDesktopServices>
#include <QDebug>
////////////////////////////////////////////////////////////////////////////////

MainWindow::MainWindow(QWidget *parent, const QString currentPath) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  _demonConfigWindow(new demonConfig(this)),
  _dbb(_demonConfigWindow->_dbb),
  _workingDirAbsolutePath(currentPath),
  _demonRootDir(QDir::homePath() + "/demons/"),
  _demonPalettesDir(QDir::homePath() + "/demons/palettes/")
{
  qInfo() << "MainWindow::MainWindow: >>>>>>>>>> CTOR START";

  ui->setupUi(this);

  readSettings();
  demon::util::logSettings();
  createDemonRootDir();
  createDemonPalettesDir();
  makeConnections();
  //updateStatusString();

  qInfo() << "MainWindow::MainWindow: >>>>>>>>>> CTOR END";
}  // MainWindow::MainWindow

MainWindow::~MainWindow() {
  qInfo() << "MainWindow::~MainWindow: Object destroyed";
  delete ui;
}

//void MainWindow::resizeEvent(QResizeEvent* ev) {
//  qInfo() << width() << "x" << height();
//}

void
MainWindow::exitApp(const int status) const {
  writeSettings();
  qInfo() << "quitting...";
  QApplication::exit(status);
  exit(status);
}  // MainWindow::exitApp

void
MainWindow::openDemonConfigWindow() const noexcept {
  connect(_demonConfigWindow.get(), &demonConfig::demonConfigClosed, this, &MainWindow::updateStatusString);
  connect(_demonConfigWindow.get(), &demonConfig::demonConfigSaved,  this, &MainWindow::enableDemonRunner);

  _demonConfigWindow->exec();
}  // MainWindow::openDemonConfigWindow

void
MainWindow::openDemonRunnerWindow() noexcept {
  demonRunner dr(*_demonConfigWindow, this);

  dr.exec();
}  // MainWindow::openDemonRunnerWindow

void MainWindow::openPaletteManager() noexcept {
  PaletteManager pmw(this);

  pmw.exec();
}  // MainWindow::openPaletteManager

void
MainWindow::updateStatusString() const noexcept {
  ui->statusBar->showMessage(_dbb.demonConfigString() +
                             " | run-" +
                             QString::number(_dbb.lastRun()));
}  // MainWindow::updateStatusString

void
MainWindow::enableDemonRunner() const noexcept {
  ui->demonRunnerPushButton->setEnabled(true);
}

QString MainWindow::getDemonPalettesDir() const {
  return _demonPalettesDir;
}

void MainWindow::openDemonsPalettesFolder() noexcept {
  QDesktopServices::openUrl(QUrl("file:" + getDemonPalettesDir(), QUrl::TolerantMode));
}

QString MainWindow::getDemonRootDir() const {
  return _demonRootDir;
}

QString MainWindow::workingDirAbsolutePath() const noexcept {
  return _workingDirAbsolutePath;
}

void MainWindow::setWorkingDirAbsolutePath(const QString& workingDirAbsolutePath) const noexcept {
  _workingDirAbsolutePath = workingDirAbsolutePath;
}  // MainWindow::colorInfo

void
MainWindow::makeConnections() const noexcept {
  connect(ui->viewWorkingDirPushButton, &QPushButton::clicked, this, &MainWindow::openWorkingDir);
  connect(ui->quitButton,   &QPushButton::clicked, this, &MainWindow::exitApp);
  connect(ui->demonConfigPushButton, &QPushButton::clicked, this, &MainWindow::openDemonConfigWindow);
  connect(ui->demonRunnerPushButton, &QPushButton::clicked, this, &MainWindow::openDemonRunnerWindow);
  connect(ui->openPaletteManagerPushButton, &QPushButton::clicked, this, &MainWindow::openPaletteManager);
}  // MainWindow::makeConnections

void MainWindow::writeSettings() const noexcept {
  // config file in /home/<user-name>/.config/skylark
  demon::util::writeGeometryInSettings(*this, "MainWindow");
}

void MainWindow::readSettings() noexcept {
  demon::util::readGeometryFromSettings(*this, "MainWindow");
}

void MainWindow::createDemonRootDir() {
  const QString demonRootDir {getDemonRootDir()};
  QDir d {demonRootDir};
  if ( false == d.exists() ) {
    const bool res = d.mkpath(demonRootDir);
    if ( true == res ) {
     qInfo() << "new demons root dir" << demonRootDir << "created";
    } else {
      qInfo() << "demons root dir" << demonRootDir << "not created (already existing)";
    }
  } else {
    qInfo() << "demons root dir" << demonRootDir << "exists";
  }

  const bool result {QDir::setCurrent(demonRootDir)};
  if ( true == result ) {
    qInfo() << "Working dir set to:" << QDir::currentPath();
  } else {
    qInfo() << "Working dir NOT SET";
  }
}

void MainWindow::createDemonPalettesDir() {
  const QString demonPalettesDir {getDemonPalettesDir()};
  QDir d {demonPalettesDir};
  if ( false == d.exists() ) {
    const bool res = d.mkpath(demonPalettesDir);
    if ( true == res ) {
     qInfo() << "new palettes dir" << demonPalettesDir << "created";
    } else {
      qInfo() << "palettes dir" << demonPalettesDir << "not created (already existing)";
    }
  } else {
    qInfo() << "palettes dir" << demonPalettesDir << "exists";
  }
}

void MainWindow::closeEvent(QCloseEvent* event) {
  writeSettings();
  event->accept();
}

void
MainWindow::openWorkingDir() const noexcept {
  demon::util::openFolder(getDemonRootDir());
}
