//
// mainwindow.h
//
#pragma once

#include <QMainWindow>
#include <QScopedPointer>

#include <iostream>
#include "DLabel.h"
#include "dbb.h"

class demonConfig;
////////////////////////////////////////////////////////////////////////////////
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr, const QString currentPath = "");
  ~MainWindow();

  QString workingDirAbsolutePath() const noexcept;
  void setWorkingDirAbsolutePath(const QString& workingDirAbsolutePath) const noexcept;

  QString getDemonRootDir() const;
  QString getDemonPalettesDir() const;
  void openDemonsPalettesFolder() noexcept;

private slots:
  void exitApp(const int status = 0) const;
  void openWorkingDir() const noexcept;

public slots:
  void openDemonConfigWindow() const noexcept;
  void openDemonRunnerWindow() noexcept;
  void openPaletteManager() noexcept;

  void updateStatusString() const noexcept;
  void enableDemonRunner() const noexcept;

private:
  Ui::MainWindow *ui;

  QScopedPointer<demonConfig> _demonConfigWindow;
  Dbb& _dbb;

  mutable QString _workingDirAbsolutePath {};
  const QString _demonRootDir;
  const QString _demonPalettesDir;

  void makeConnections() const noexcept;

  void writeSettings() const noexcept;
  void readSettings() noexcept;

  void createDemonRootDir();
  void createDemonPalettesDir();

protected:
  void closeEvent(QCloseEvent *event) override;
  //void resizeEvent(QResizeEvent* ev);
};  // class MainWindow
