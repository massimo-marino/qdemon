//
// mask.h
//
// Created by massimo on 10/22/18.
//
#pragma once

#include <iostream>
#include <fstream>
#include <vector>
//#include <deque>

#include <QDebug>
////////////////////////////////////////////////////////////////////////////////
// squared matrix NxN, N>=3 and odd (3, 5, 7, 9, ...), initialized as needed for
// the demon app
//
// std::deque and std::vector are both possible for typename R
//template <typename T, typename R = std::deque<T>>
template <typename T, typename R = std::vector<T>>
class mask
{
  using maskRow_t = R;
  using mask_t    = std::vector<maskRow_t>;

protected:
  static constexpr size_t _defaultD {3};
  mutable size_t _D {_defaultD};
  mutable mask_t _mask {_D, maskRow_t(_D, T{1})};

  void
  init() const noexcept
  {
    _mask.reserve(_D);
    _mask.resize(_D);

    for (size_t i {0}; i < _D; ++i)
    {
      _mask[i] = maskRow_t(_D, T{1});
    }

    // the central element must be always 0
    _mask[(_D-1)/2][(_D-1)/2] = T{0};
  }

public:
  // default ctor
  explicit
  mask()
  {
    // the central element must be always 0
    _mask[(_D-1)/2][(_D-1)/2] = T{0};
  }

  explicit
  mask(const size_t D) noexcept(false) : _D(D)
  {
    if ( 0 == (_D & 1) )
    {
      qInfo() << "ERROR: a mask must have odd dimensions greater than or equal to 3, but" << _D << "was given";
      throw 99;
    }
    init();
  }

  explicit
  mask(const int D) noexcept(false) : mask(static_cast<size_t>(D)) {}

  explicit
  mask(const unsigned int D) noexcept(false) : mask(static_cast<size_t>(D)) {}

  constexpr
  size_t
  width() const noexcept
  {
    return _D;
  }

  constexpr
  size_t
  height() const noexcept
  {
    return _D;
  }

  mask&
  resize(const size_t D) noexcept
  {
    _mask.clear();
    _mask.shrink_to_fit();
    _D = D;
    init();

    return *this;
  }

  // allow writing m[r][c]
  constexpr
  maskRow_t
  operator[](const size_t r) const noexcept
  {
    return _mask[r];
  }

  constexpr
  maskRow_t
  operator[](const int r) const noexcept
  {
    return _mask[static_cast<size_t>(r)];
  }

  void swap(mask& rhs) noexcept
  {
    _mask.swap(rhs._mask);
  }

  bool
  saveMask(const std::string& fname) const
  {
    std::ofstream ofs(fname, std::ios::binary);
    std::size_t i{0};
    std::size_t j{0};
    if ( ofs.is_open() )
    {
      for (i = 0; i < _D; ++i)
      {
        for (j = 0; j < (_D - 1); ++j)
        {
          ofs << _mask[i][j] << " ";
        }
        ofs << _mask[i][j] << "\n";
      }
      return true;
    }
    return false;
  }  // saveMask

  bool
  loadMask(const std::string& fname) const
  {
    std::ifstream ifs(fname, std::ios::binary);
    std::size_t i{0};
    std::size_t j{0};
    if ( ifs.is_open() )
    {
      for (i = 0; i < _D; ++i)
      {
        for (j = 0; j < (_D - 1); ++j)
        {
          ifs >> _mask[i][j];
        }
        ifs >> _mask[i][j];
      }
      return true;
    }
    return false;
  }  // loadMask

  std::ostream&
  operator<<(std::ostream &os) const
  {
    os << "mask " << _D << "x" << _D << ":\n";
    for (std::size_t i{0}; i < _D; ++i)
    {
//      os << "R " << i << ": ";
      for (std::size_t j{0}; j < _D; ++j)
      {
//        os << "[" << i << "," << j << "]:" << mask_[i][j] << " ";
        os << _mask[i][j] << " ";
      }
      os << "\n";
    }
    os << "\n" << std::flush;
    return os;
  }
};  // class mask

// mask types
enum class maskType_t: int { MOORE = 0,
                           VON_NEUMANN = 1,
                           DIAGONALS = 2,
                           PLUS = 3,
                           PERIMETER = 4,
                           CUSTOM = 5
                         };

template <typename T, typename R = std::vector<T>>
class typedMask final : public mask<T, R>
{
  using maskRow_t = R;
  using mask_t    = std::vector<maskRow_t>;

public:
  explicit
  typedMask(const size_t D) noexcept(false) : mask<T>(D) {}

  typedMask(const size_t D, const ::maskType_t maskType) noexcept(false) : mask<T>(D),
  _maskType(maskType)
  {
    init(maskType);
  }

  typedMask(const unsigned int D, const ::maskType_t maskType) noexcept(false) : mask<T>(D),
  _maskType(maskType)
  {
    init(maskType);
  }

  ::maskType_t
  getMaskType() const noexcept
  {
    return _maskType;
  }

  std::string
  getMaskTypeName() const noexcept
  {
    return maskTypeName(_maskType);
  }

  static
  std::string
  maskTypeName(const ::maskType_t maskType) noexcept
  {
    switch (maskType)
    {
      case ::maskType_t::VON_NEUMANN:
      return "Von Neumann";

      case ::maskType_t::DIAGONALS:
      return "Diagonals";

      case ::maskType_t::PLUS:
      return "Plus";

      case ::maskType_t::PERIMETER:
      return "Perimeter";

      case ::maskType_t::CUSTOM:
      return "Custom";

      case ::maskType_t::MOORE:
      default:
      return "Moore";
    }
  }

  std::string
  maskTypeName() const noexcept
  {
    return maskTypeName(_maskType);
  }

  typedMask<T>&
  resize(const size_t D, const ::maskType_t maskType) noexcept
  {
    typedMask<T>::_mask.clear();
    typedMask<T>::_mask.shrink_to_fit();
    typedMask<T>::_D = D;
    _maskType = maskType;
    init(maskType);

    return *this;
  }

private:
  ::maskType_t _maskType {maskType_t::MOORE};

  void
  init(const ::maskType_t maskType) const noexcept
  {
    qInfo() << __func__ << ": mask type:" << QString::fromStdString(maskTypeName(maskType));

    mask<T>::init();

    switch (maskType)
    {
      case ::maskType_t::VON_NEUMANN:
      {
        size_t r {0};  // row index
        size_t c {0};  // col index

        for (c = 0;
             (c < typedMask<T>::_D) && (c != ((typedMask<T>::_D - 1) / 2));
             ++c)
        {
          typedMask<T>::_mask[0][c] = 0;
          typedMask<T>::_mask[0][typedMask<T>::_D - 1 - c] = 0;
          typedMask<T>::_mask[typedMask<T>::_D - 1][c] = 0;
          typedMask<T>::_mask[typedMask<T>::_D - 1][typedMask<T>::_D - 1 - c] = 0;
        }
        for (r = 1;
             (r < (typedMask<T>::_D - 1)) && (r != ((typedMask<T>::_D - 1) / 2));
             ++r)
        {
          typedMask<T>::_mask[r][0] = 0;
          typedMask<T>::_mask[r][typedMask<T>::_D - 1] = 0;
          typedMask<T>::_mask[typedMask<T>::_D - 1 - r][0] = 0;
          typedMask<T>::_mask[typedMask<T>::_D - 1 - r][typedMask<T>::_D - 1] = 0;
        }
      }
      break;

      case ::maskType_t::DIAGONALS:
      {
        size_t r {0};  // row index
        size_t c {0};  // col index

        for (r = 0; r < typedMask<T>::_D; ++r)
        {
          for (c = 0; c < typedMask<T>::_D; ++c)
          {
            if ((r != c) && ((r + c) != (typedMask<T>::_D - 1)))
            {
              typedMask<T>::_mask[r][c] = 0;
            }
          }
        }
      }
      break;

      case ::maskType_t::PLUS:
      {
        size_t r {0};  // row index
        size_t c {0};  // col index

        for (r = 0; r < typedMask<T>::_D; ++r)
        {
          for (c = 0; c < typedMask<T>::_D; ++c)
          {
            if ((r != ((typedMask<T>::_D - 1) / 2)) && (c != ((typedMask<T>::_D - 1) / 2)))
            {
              typedMask<T>::_mask[r][c] = 0;
            }
          }
        }
      }
      break;

      case ::maskType_t::PERIMETER:
      {
        size_t r {0};  // row index
        size_t c {0};  // col index

        for (r = 0; r < typedMask<T>::_D; ++r)
        {
          for (c = 0; c < typedMask<T>::_D; ++c)
          {
            if ((r != 0) && (r != (typedMask<T>::_D - 1)) &&
                (c != 0) && (c != (typedMask<T>::_D - 1)))
            {
              typedMask<T>::_mask[r][c] = 0;
            }
          }
        }
      }
      break;

      // Moore is the default: nothing to do here
      case ::maskType_t::MOORE:
      default:
      break;
    }
  }
};  // class typedMask

template <typename T>
std::ostream&
operator<<(std::ostream &os, const mask<T>& m)
{
  m.operator<<(os);
  return os;
}

using maskItemType = size_t;
using mask_t = typedMask<maskItemType>;
