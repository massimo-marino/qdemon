//
// matx.h
//
// Created by massimo on 10/25/18.
//
#pragma once

#include "sg/randomNumberGenerators.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <functional>
#include <algorithm>
#include <future>
////////////////////////////////////////////////////////////////////////////////
namespace demon {
// matrix RxC
//
// The constructor can create a matrix with random generated items by passing
// a std::function<T()> to it that does the job
//
#pragma pack (push, 1)

template <typename T>
static
size_t
randomizeThread (const size_t startColIdx,
                 const size_t endColIdx,
                 std::vector<T>& rowY,
                 std::function<T()> _rng)
{
  // column index (x) in the row y
  size_t colIdx {};
  for (colIdx = startColIdx; colIdx <= endColIdx; ++colIdx)
  {
    rowY[colIdx] = _rng();
  }
  return colIdx;
}  // randomizeThread

template <typename T>
static
size_t
randomizeXYThread (const size_t rowIdx,  // row index (y)
                   const size_t startColIdx,
                   const size_t endColIdx,
                   std::vector<T>& rowY,
                   std::function<T(size_t x, size_t y)> _rng)
{
  // column index (x) in the row y
  size_t colIdx {};
  for (colIdx = startColIdx; colIdx <= endColIdx; ++colIdx)
  {
    rowY[colIdx] = _rng(colIdx, rowIdx);
  }
  return colIdx;
}  // randomizeXYThread

template <typename T = int>
class matx final
{
  using matxRow_t = std::vector<T>;
  using matx_t    = std::vector<matxRow_t>;

private:
  static constexpr size_t _defaultR {2};
  static constexpr size_t _defaultC {2};
  mutable size_t _R {_defaultR};
  mutable size_t _C {_defaultC};
  std::function<T()> _initfn {[] () -> T {return T{};}};
  // _matx is a vector of vectors of type matxRow_t, where every vector is a row
  // of the matrix made of elements of type T
  // { {<row-0>} {<row-1>} ... }
  mutable matx_t _matx {}; //{R_, matxRow_t(C_, initfn_())};

  void
  init() const noexcept
  {
    _matx.reserve(_R);

    for (auto& r : _matx)
    {
      r = matxRow_t(_C, _initfn());
    }
  }  // init

  void
  init(std::function<T()> rng) const noexcept
  {
    _matx.reserve(_R);

    for (auto& r : _matx)
    {
      r.reserve(_C);
      for (size_t c {0}; c < _C ; ++c)
      {
        r.emplace_back(rng());
      }
    }
  }  // init

public:
  // default ctor - it does not call any init() method
  explicit
  matx() : _matx({_R, matxRow_t(_C, _initfn())}) {}

  explicit
  matx(const std::string& fileName)
  {
    loadMatx(fileName);
  }

  explicit
  matx(std::function<T()> rng) : _matx({_R, matxRow_t{}})
  {
    init(rng);
  }

  explicit
  matx(const size_t R, const size_t C) noexcept
  : _R(R), _C(C), _matx({_R, matxRow_t(_C, _initfn())}) {}

  explicit
  matx(const size_t R, const size_t C, std::function<T()> rng) noexcept
  : _R(R), _C(C), _matx({_R, matxRow_t{}})
  {
    init(rng);
  }

  explicit
  matx(const int R, const int C) noexcept
  : matx(static_cast<size_t>(R), static_cast<size_t>(C))
  {}

  explicit
  matx(const int R, const int C, std::function<T()> rng) noexcept
  : matx(static_cast<size_t>(R), static_cast<size_t>(C), rng)
  {}

  explicit
  matx(const unsigned int R, const unsigned int C) noexcept
  : matx(static_cast<size_t>(R), static_cast<size_t>(C))
  {}

  explicit
  matx(const unsigned int R, const unsigned int C, std::function<T()> rng) noexcept
  : matx(static_cast<size_t>(R), static_cast<size_t>(C), rng)
  {}

//  matx& operator=(matx& rhs)
//  {
//    _R = rhs._R;
//    _C = rhs._C;
//    _matx = rhs._matx;
//    return *this;
//  }

  constexpr
  size_t
  size() const noexcept
  {
    return _R * _C;
  }

  constexpr
  size_t
  cols() const noexcept
  {
    return _C;
  }
  constexpr
  size_t
  width() const noexcept
  {
    return _C;
  }

  constexpr
  size_t
  rows() const noexcept
  {
    return _R;
  }
  constexpr
  size_t
  height() const noexcept
  {
    return _R;
  }

  // allow writing m[r][c]
  matxRow_t&
  operator[](const size_t r) noexcept
  {
    return _matx[r];
  }

  matxRow_t&
  operator[](const int r) noexcept
  {
    return _matx[static_cast<size_t>(r)];
  }

  void
  swap(matx& rhs) noexcept
  {
    std::swap(_R, rhs._R);
    std::swap(_C, rhs._C);
    _matx.swap(rhs._matx);
  }

  void
  randomize(std::function<T()> rng, const size_t numTasks = std::thread::hardware_concurrency()) const
  {
    threadedRandomize(rng, numTasks);
//    for (auto& r : _matx)
//    {
//      for (auto& c : r)
//      {
//        c = rng();
//      }
//    }
  }  // randomize

  void
  threadedRandomize(std::function<T()> rng, const size_t numTasks = std::thread::hardware_concurrency()) const
  {
//    std::cout << "[" << __func__ << "] "
//              << "There are "
//              << numTasks
//              << " cores\n";

    const size_t fullBatches  {rows() / numTasks};
    const size_t smallBatches {rows() - numTasks * fullBatches};

//    std::cout << "numTasks: " << numTasks << " - "
//              << "rows(): " << rows() << " - "
//              << "fullBatches: " << fullBatches << " - "
//              << "smallBatches: " << smallBatches << "\n";

    std::vector<size_t> jobs(fullBatches, numTasks);
    if ( smallBatches > 0 )
    {
      jobs.push_back(smallBatches);
    }
//    std::cout << "jobs: ";
//    for (const auto& j : jobs)
//    {
//      std::cout << j << " ";
//    }
//    std::cout << "\n";

//    size_t cnt {0};
    size_t batch {0};
    size_t idx {0};
    size_t rowIdx {0};
    std::vector<std::future<size_t>> fa {};
    std::future<size_t> f;
    for (const auto& n : jobs)
    {
      fa.reserve(n);
      for (idx = 0; idx < n; ++idx)
      {
        //std::vector<T>& row {_matx[idx + numTasks * batch]};
        rowIdx = idx + numTasks * batch;
        f = std::async(std::launch::async, randomizeThread<T>,
                                           0,
                                           _C - 1,
                                           // pass the row by reference
                                           std::ref(_matx[rowIdx]),  //std::ref(row),
                                           rng);
        fa.emplace_back(std::move(f));
      }
      for (auto& ff : fa)
      {
        [[maybe_unused]] size_t pr = ff.get();
        // pr must be equal to _C
//        std::cout << ++cnt << ": future returned: " << pr << " batch " << batch
//                  << " - " << std::boolalpha << (pr == _C) << std::endl;
      }
      fa.clear();
      fa.shrink_to_fit();
      ++batch;
    }
  }  // threadedRandomize

  void
  randomize(std::function<T(size_t x, size_t y)> rng,
            const size_t numTasks = std::thread::hardware_concurrency()) const
  {
    threadedXYRandomize(rng, numTasks);
//    for (auto& r : _matx)
//    {
//      for (auto& c : r)
//      {
//        c = rng();
//      }
//    }
  }  // randomize

  void
  threadedXYRandomize(std::function<T(size_t x, size_t y)> rng,
                      const size_t numTasks = std::thread::hardware_concurrency()) const
  {
//    std::cout << "[" << __func__ << "] "
//              << "There are "
//              << numTasks
//              << " cores\n";

    const size_t fullBatches  {rows() / numTasks};
    const size_t smallBatches {rows() - numTasks * fullBatches};

//    std::cout << "numTasks: " << numTasks << " - "
//              << "rows(): " << rows() << " - "
//              << "fullBatches: " << fullBatches << " - "
//              << "smallBatches: " << smallBatches << "\n";

    std::vector<size_t> jobs(fullBatches, numTasks);
    if ( smallBatches > 0 )
    {
      jobs.push_back(smallBatches);
    }
//    std::cout << "jobs: ";
//    for (const auto& j : jobs)
//    {
//      std::cout << j << " ";
//    }
//    std::cout << "\n";

//    size_t cnt {0};
    size_t batch {0};
    size_t idx {0};
    size_t rowIdx {0};
    std::vector<std::future<size_t>> fa {};
    std::future<size_t> f;
    for (const auto& n : jobs)
    {
      fa.reserve(n);
      for (idx = 0; idx < n; ++idx)
      {
        //std::vector<T>& row {_matx[idx + numTasks * batch]};
        rowIdx = idx + numTasks * batch;
        f = std::async(std::launch::async, randomizeXYThread<T>,
                                           rowIdx,
                                           0,
                                           _C - 1,
                                           // pass the row by reference
                                           std::ref(_matx[rowIdx]),  //std::ref(row),
                                           rng);
        fa.emplace_back(std::move(f));
      }
      for (auto& ff : fa)
      {
        [[maybe_unused]] size_t pr = ff.get();
        // pr must be equal to _C
//        std::cout << ++cnt << ": future returned: " << pr << " batch " << batch
//                  << " - " << std::boolalpha << (pr == _C) << std::endl;
      }
      fa.clear();
      fa.shrink_to_fit();
      ++batch;
    }
  }  // threadedXYRandomize

  bool
  loadMatx(const std::string& fileName) const
  {
    std::ifstream ifs(fileName, std::ios::binary);
    size_t r {0};
    size_t c {0};

    if ( ifs.is_open() )
    {
      ifs >> _R >> _C;

      _matx.reserve(_R);

      T item {};
      for (r = 0; r < _R; ++r)
      {
        //_matx.push_back( {matxRow_t{}} );
        _matx[r].reserve(_C);

        for (c = 0; c < _C; ++c)
        {
          ifs >> item;
          _matx[r][c] = item;
        }
      }
      return true;
    }
    return false;
  }  // loadMatx

  bool
  saveMatx(const std::string& fileName) const
  {
    std::ofstream ofs(fileName, std::ios::binary);
    if ( ofs.is_open() )
    {
      bool isFirst {true};
      ofs << _R << " " << _C << '\n';
      for (const auto& r : _matx)
      {
        isFirst = true;
        for (const auto& celem : r)
        {
          if (false == isFirst) {
            ofs << " ";
          }
          ofs << celem;
          isFirst = false;
        }
        ofs << '\n';
      }
      return true;
    }
    return false;
  }  // saveMatx

  std::ostream&
  operator<<(std::ostream &os) const
  {
    os << "matx " << _R << "x" << _C << ":\n";
    for (const auto& r : _matx)
    {
      for (const auto& celem : r)
      {
        os << celem << " ";
      }
      os << "\n";
    }
    os << "\n" << std::flush;
    return os;
  }

};  // class matx

#pragma pack (pop)

}  // namespace demon

template <typename T>
std::ostream&
operator<<(std::ostream &os, const demon::matx<T>& m)
{
  m.operator<<(os);
  return os;
}
