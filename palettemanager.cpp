#include "palettemanager.h"
#include "ui_palettemanager.h"

#include "DLabel.h"
#include "mainwindow.h"
#include "demonconfig.h"

#include "util.h"

#include <QTextStream>
#include <QFileDialog>
#include <QHash>
#include <QDebug>

PaletteManager::PaletteManager(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::PaletteManager),
  _mainWindow(qobject_cast<MainWindow *>(parent)),
  _palettesDir(_mainWindow->getDemonPalettesDir())
{
  ui->setupUi(this);

  readSettings();
  demon::util::logSettings();

  _paletteLabelPtr = new DLabel(ui->scrollArea);
  _paletteLabelPtr->setMouseTracking(true);

  connect(ui->loadPaletteImagePushButton, &QPushButton::clicked, this, &PaletteManager::loadPaletteImage);
  connect(ui->extractPalettePushButton, SIGNAL(clicked(bool)), this, SLOT(extractPalette(bool)));
  connect(_paletteLabelPtr, &DLabel::colorInfo, this, &PaletteManager::colorInfo);
  connect(ui->clearPaletteSelection, &QPushButton::clicked, this, &PaletteManager::clearPaletteSelection);
  connect(ui->viewPalettesDirPushButton, &QPushButton::clicked, this, &PaletteManager::openPalettesDir);
  connect(ui->generatePalettePushButton, &QPushButton::clicked, this, &PaletteManager::generatePalette);
  connect(ui->makePaletteImagePushButton, SIGNAL(clicked(bool)), this, SLOT(makePaletteImage(bool)));
  connect(ui->sortPaletteCheckBox, &QCheckBox::stateChanged, this, &PaletteManager::on_sortPaletteCheckBoxChanged);
  connect(ui->shufflePaletteCheckBox, &QCheckBox::stateChanged, this, &PaletteManager::on_shufflePaletteCheckBoxChanged);
  connect(ui->redSlider, &QSlider::valueChanged, this, &PaletteManager::redSliderValueChanged);
  connect(ui->greenSlider, &QSlider::valueChanged, this, &PaletteManager::greenSliderValueChanged);
  connect(ui->blueSlider, &QSlider::valueChanged, this, &PaletteManager::blueSliderValueChanged);

  setRGBColor();
}

PaletteManager::~PaletteManager()
{
  writeSettings();
  delete ui;
}

void PaletteManager::writeSettings() const
{
  // config file in /home/<user-name>/.config/skylark
  demon::util::writeGeometryInSettings(*this, "PaletteManager");
}

void PaletteManager::readSettings()
{
  demon::util::readGeometryFromSettings(*this, "PaletteManager");
}

QString PaletteManager::selectImage(const QString& caption) const noexcept
{
  QString fname {};
  static QString lastDir {_palettesDir};
  QFileDialog qfd(nullptr, caption, lastDir, tr("Image Files (*.JPG *.jpg *.png *.bmp *.ppm)"));

  if ( qfd.exec() )
  {
    lastDir = qfd.directory().absolutePath();
    const auto selectedFiles { qfd.selectedFiles() };
    fname = selectedFiles.first();
  }
  return fname;
}

QString PaletteManager::selectRGBHexFile(const QString& caption) const noexcept
{
  QString fname {};
  static QString lastDir {_palettesDir};
  QFileDialog qfd(nullptr, caption, lastDir, tr("RGB Hex Files (*rgbhex*.txt *rgbhex*.rgbhex *.rgbhex)"));

  if ( qfd.exec() )
  {
    lastDir = qfd.directory().absolutePath();
    const auto selectedFiles { qfd.selectedFiles() };
    fname = selectedFiles.first();
  }
  return fname;
}

bool PaletteManager::loadPaletteImage(bool checked) const noexcept
{
  Q_UNUSED(checked);

  QString fname {selectImage(tr("Select Palette Image"))};

  if ("" == fname)
  {
    return false;
  }

  bool result {_paletteImage.load(fname)};
  if ( result == false )
  {
    qInfo() << "Palette image" << fname << "not loaded";
  }
  else
  {
    ui->paletteNameLabel->setText(QFileInfo(fname).fileName());
    qInfo() << "Palette image"
            << fname
            << "loaded - size"
            << _paletteImage.width() << "x" << _paletteImage.height();

    _palettePixmap = QPixmap::fromImage(_paletteImage);
    qInfo() << "Palette Pixmap size"
            << _palettePixmap.width() << "x" << _palettePixmap.height();
//    printQImageInfo(paletteImage_);

    _paletteLabelPtr->setPixmap(_palettePixmap);
    _paletteLabelPtr->resize(_palettePixmap.width(), _palettePixmap.height());
    ui->scrollArea->setBackgroundRole(QPalette::Dark);
    ui->scrollArea->setWidget(_paletteLabelPtr);
    //ui->scrollArea->ensureWidgetVisible(&paletteLabel_, 0, 0);
    //ui->scrollArea->setWidgetResizable(true);
    ui->scrollArea->resize(std::min(_palettePixmap.width() + 6, 1000), 6 + _palettePixmap.height());
    qInfo() << "scroll area size:"
            << ui->scrollArea->width()
            << "x"
            << ui->scrollArea->height();
    qInfo() << "label size:"
            << _paletteLabelPtr->width()
            << "x"
            << _paletteLabelPtr->height();

   //ui->colorInfoLabel->setScaledContents(true);

   QColor c {QColor(0, 0, 0)};
   QVariant variant {c};
   QString colcode {variant.toString()};
   ui->colorLabel->setStyleSheet("QLabel { background-color :"+colcode+" ; color : white; }");
  }
  return result;
} // PaletteManager::loadPaletteImage

void PaletteManager::colorInfo(const int x, const int y, const QColor& qc, const bool mousePress) const noexcept
{
  QString decRGB {};
  QString hexRGB {};
  const int red {qc.red()};
  const int green {qc.green()};
  const int blue {qc.blue()};

  decRGB = QString("%1 %2 %3").arg(red, 3, 10, QChar(' '))
                              .arg(green, 3, 10, QChar(' '))
                              .arg(blue, 3, 10, QChar(' '));
  hexRGB = QString("#%1%2%3").arg(red, 2, 16, QChar('0'))
                             .arg(green, 2, 16, QChar('0'))
                             .arg(blue, 2, 16, QChar('0'));
  ui->colorInfoLabel->setText("[" + QString::number(x) + "," +
                              QString::number(y) + "]: " +
                              decRGB + " | " + hexRGB);

  const QVariant variant {qc};
  const QString colcode {variant.toString()};
  ui->colorLabel->setStyleSheet("QLabel { background-color :"+colcode+" ; color : white; }");

  if ( mousePress )
  {
    bool ok {};

    ui->paletteSelection->textCursor().insertText(hexRGB + "\n");
    ui->colorsSelectedLabel->setNum(ui->colorsSelectedLabel->text().toInt(&ok) + 1);
  }
}  // PaletteManager::colorInfo

void PaletteManager::clearPaletteSelection(bool checked) const noexcept
{
  Q_UNUSED(checked);

  ui->paletteSelection->clear();
  ui->colorsSelectedLabel->setNum(0);
}  // PaletteManager::clearPaletteSelection

bool PaletteManager::extractPalette(const QString& fname,
                                    int32_t colorsToExtract,
                                    const bool sortPalette,
                                    const bool shufflePalette) const noexcept
{
  QFileInfo fileInfo(fname);
  QImage img;
  const bool result {img.load(fname)};
  if ( result == false )
  {
    qInfo() << "Image" << fname << "not loaded";
    return result;
  }

  const QSize imageSize {img.size()};
  const int isz {imageSize.width() * imageSize.height()};
  QPixmap pixmap;

  qInfo() << "Image"
          << fname
          << "loaded - size (WxH)"
          << img.width() << "x" << img.height()
          << "=" << isz;

  pixmap = QPixmap::fromImage(img);
  const QSize pixmapSize {pixmap.size()};
  const int pxmsz {pixmapSize.width() * pixmapSize.height()};

  qInfo() << "Image Pixmap size (WxH)"
          << pixmap.width() << "x" << pixmap.height()
          << "=" << pxmsz;
  printQImageInfo(img);

  int32_t totalNumberOfColors {};
  QMap<uint32_t, QVector<sg::rgb::RGB_t>> occurrencesColorsMap {};  // Key: occurrences -> Value: {color-1, color-2, ...}
  {
    QColor color {};
    sg::rgb::RGB rgbColor {};
    QMap<sg::rgb::RGB_t, uint32_t> colorsOccurrencesMap {};  // Key: color -> Value: occurrences
    for (int w {0}; w < pixmap.width(); ++w)
    {
      for (int h {0}; h < pixmap.height(); ++h)
      {
        color = img.pixelColor(w, h);
        rgbColor.setRGB(color.red(), color.green(), color.blue());
        uint32_t& occurrences = colorsOccurrencesMap[rgbColor()];
        colorsOccurrencesMap.insert(rgbColor(), ++occurrences);
      }
    }
    totalNumberOfColors = colorsOccurrencesMap.size();
    qInfo() << "The image" << fname
            << "contains" << totalNumberOfColors
            << "colors";

    // in the following loop the map occurrencesColorsMap is populated
    QMapIterator<sg::rgb::RGB_t, uint32_t> iter(colorsOccurrencesMap);
    while (iter.hasNext())
    {
      iter.next();
      QVector<sg::rgb::RGB_t>& colorsV = occurrencesColorsMap[iter.value()];
      colorsV.push_back(iter.key());
      // print out the colors with their occurrences
//      std::cout << std::hex << "0x"
//                << std::setw(6) << std::setfill('0') << iter.key() << ": "
//                << std::dec << iter.value() << std::endl;
    }
  }
  // iterate from the most occurring colors
  if (0 == colorsToExtract)
  {
    colorsToExtract = totalNumberOfColors;
  }
  int counter {1};
  sg::palette::Palette extractedPalette {};

  const QString rootDirName {_mainWindow->getDemonPalettesDir() + fileInfo.baseName() + ".d/"};
  QDir rootDir(rootDirName);
  rootDir.mkpath(rootDirName);

  qInfo() << "rootDir:" << rootDirName;

  QMapIterator<uint32_t, QVector<sg::rgb::RGB_t>> revIter(occurrencesColorsMap);
  revIter.toBack();
  QString logs;
  QTextStream out(&logs);

  out << Qt::endl << "List of colors in order of occurrences:" << Qt::endl;
  while (revIter.hasPrevious() && (colorsToExtract > 0))
  {
    revIter.previous();
    out << revIter.key()
        << " -> (" << revIter.value().size()
        << ")[ " << Qt::hex;
    for (const sg::rgb::RGB_t& color : revIter.value())
    {
      extractedPalette.addRGBColor(sg::rgb::RGB(color));
      --colorsToExtract;
      if ((0 == colorsToExtract) || (extractedPalette.size() == 2048))
      {
        orderPalette(extractedPalette, sortPalette, shufflePalette);
        QString fileLoc {rootDirName + fileInfo.baseName() + "-extracted-" + QString::number(counter) + "-"};
        extractedPalette.saveRGBHexPalette(fileLoc.toStdString());
        extractedPalette.makePaletteImage<sg::png>(fileLoc.toStdString());
        extractedPalette.clear();
        ++counter;
      }
      out << "0x"
          << qSetFieldWidth(6) << qSetPadChar('0')
          << color
          << qSetFieldWidth(0) << qSetPadChar(QChar(0x20))
          << " ";
    }
    out << Qt::dec << "]" << Qt::endl;
  }
  out << Qt::endl;
  qInfo() << out.string();
  if ((colorsToExtract >= 0) && (0 != extractedPalette.size()))
  {
    orderPalette(extractedPalette, sortPalette, shufflePalette);
    QString fileLoc {rootDirName + fileInfo.baseName() + "-extracted-" + QString::number(counter) + "-"};
    extractedPalette.saveRGBHexPalette(fileLoc.toStdString());
    extractedPalette.makePaletteImage<sg::png>(fileLoc.toStdString());
  }

  return result;
}

bool PaletteManager::extractPalette(bool checked) const noexcept
{
  Q_UNUSED(checked);

  const QString fname {selectImage(tr("Select an Image"))};

  if ("" == fname)
  {
    return false;
  }

  const int32_t colorsToExtract {ui->numberOfColors->value()};
  const bool sortPalette {ui->sortPaletteCheckBox->isChecked()};
  const bool shufflePalette {ui->shufflePaletteCheckBox->isChecked()};

  return extractPalette(fname, colorsToExtract, sortPalette, shufflePalette);
}  // PaletteManager::extractPalette

void PaletteManager::openPalettesDir(bool checked) const noexcept
{
  Q_UNUSED(checked);

  _mainWindow->openDemonsPalettesFolder();
}

void PaletteManager::generatePalette(bool checked) const noexcept
{
  Q_UNUSED(checked);

  sg::palette::Palette palette {ui->numberOfColors->value()};
  orderPalette(palette);
  palette.saveRGBHexPalette(_palettesDir.toStdString());
  palette.makePaletteImage<sg::png>(_palettesDir.toStdString());
}

void PaletteManager::makePaletteImage(const QString& rgbHexFileName) const noexcept
{
  const QFileInfo fileInfo(rgbHexFileName);
  const QString fileDir {fileInfo.absolutePath() + "/"};
  sg::palette::Palette p(rgbHexFileName.toStdString());

  qInfo() << "RGBHex File dir:" << fileDir;
  p.makePaletteImage<sg::png>(fileDir.toStdString());
}

void PaletteManager::makePaletteImage(bool checked) const noexcept
{
  Q_UNUSED(checked);

  const QString rgbHexFileName {selectRGBHexFile(tr("Select an RGB Hex File"))};

  if ("" == rgbHexFileName)
  {
    return;
  }

  qInfo() << "RGBHex File selected:" << rgbHexFileName;

  makePaletteImage(rgbHexFileName);
}

void PaletteManager::orderPalette(sg::palette::Palette& palette,
                                  const bool sortPalette,
                                  const bool shufflePalette) const noexcept
{
  if (sortPalette)
  {
    palette.sortPalette();
  }
  else if (shufflePalette)
  {
    palette.shufflePalette();
  }
}

void PaletteManager::orderPalette(sg::palette::Palette& palette) const noexcept
{
  orderPalette(palette,
               ui->sortPaletteCheckBox->isChecked(),
               ui->shufflePaletteCheckBox->isChecked());
}

void PaletteManager::on_sortPaletteCheckBoxChanged(const int state)
{
  switch (state)
  {
    case Qt::Unchecked:
    {
    }
    break;

    case Qt::Checked:
    {
      ui->shufflePaletteCheckBox->setCheckState(Qt::Unchecked);
    }
    break;
  }
}

void PaletteManager::on_shufflePaletteCheckBoxChanged(const int state)
{
  switch (state)
  {
    case Qt::Unchecked:
    {
    }
    break;

    case Qt::Checked:
    {
      ui->sortPaletteCheckBox->setCheckState(Qt::Unchecked);
    }
    break;
  }

}

void PaletteManager::redSliderValueChanged(int value)
{
  ui->redValue->setText(QString::number(value));
  setRGBColor();
}

void PaletteManager::greenSliderValueChanged(int value)
{
  ui->greenValue->setText(QString::number(value));
  setRGBColor();
}

void PaletteManager::blueSliderValueChanged(int value)
{
  ui->blueValue->setText(QString::number(value));
  setRGBColor();
}

void PaletteManager::setRGBColor() const noexcept
{
   const int R {ui->redSlider->value()};
   const int G {ui->greenSlider->value()};
   const int B {ui->blueSlider->value()};
   const QColor color {QColor(R, G, B)};
   const QVariant colorVariant {color};
   const QString colorCode {colorVariant.toString()};
   const QColor textColor {QColor(255 - R, 255 - G, 255 - B)};
   const QVariant textColorVariant {textColor};
   const QString textColorCode {textColorVariant.toString()};

   ui->RGBColor->setStyleSheet("QLabel { background-color :" + colorCode + " ; color : " + textColorCode + "; }");
   ui->RGBColor->setText(colorCode);
}

void PaletteManager::printQImageInfo(const QImage& qimage) noexcept
{
  auto width {qimage.width()};
  auto height {qimage.height()};
  auto bitPlaneCount {qimage.bitPlaneCount()};
  auto byteCount {qimage.sizeInBytes()};  //{qimage.byteCount()};
  auto bytesPerLine {qimage.bytesPerLine()};
  auto colorCount {qimage.colorCount()};
  auto depth {qimage.depth()};
  auto hasAlphaChannel {qimage.hasAlphaChannel()};
  auto isGrayScale {qimage.isGrayscale()};
  auto isNull {qimage.isNull()};

  qInfo() << "QImage info:";
  qInfo() << "QImage dimensions:             " << width << "x" << height;
  qInfo() << "QImage format:                 " << qimage.format();
  qInfo() << "Bit Plane Count:               " << bitPlaneCount;
  qInfo() << "Byte Count:                    " << byteCount;
  qInfo() << "Bytes per Line:                " << bytesPerLine;
  qInfo() << "Color Count:                   " << colorCount;
  qInfo() << "Color Table Size:              " << qimage.colorTable().size();
  qInfo() << "Depth (bits per pixel aka bpp):" << depth;
  qInfo() << "Has alpha channel:             " << hasAlphaChannel;
  qInfo() << "Is Gray Scale:                 " << isGrayScale;
  qInfo() << "Is Null:                       " << isNull;
}  // PaletteManager::printQImageInfo
