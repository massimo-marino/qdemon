#ifndef PALETTEMANAGER_H
#define PALETTEMANAGER_H

#include "sg/palette.h"

#include <QDialog>

class MainWindow;
class DLabel;

namespace Ui {
class PaletteManager;
}

class PaletteManager : public QDialog
{
  Q_OBJECT

public:
  explicit PaletteManager(QWidget *parent = nullptr);
  ~PaletteManager();

public slots:
  bool loadPaletteImage(bool checked = false) const noexcept;
  void colorInfo(const int x, const int y, const QColor& qc, const bool mousePress) const noexcept;

private slots:
  void clearPaletteSelection(bool checked = false) const noexcept;
  // extractPalette: extract the palette from an image file and saves it
  bool extractPalette(bool checked = false) const noexcept;
  void openPalettesDir(bool checked = false) const noexcept;
  // generatePalette: generate a standard palette and saves it; it takes the number of colors
  void generatePalette(bool checked = false) const noexcept;
  // makePaletteImgae: generate the palette image from an RGB Hex file
  void makePaletteImage(bool checked = false) const noexcept;
  void on_sortPaletteCheckBoxChanged(const int state);
  void on_shufflePaletteCheckBoxChanged(const int state);

  void redSliderValueChanged(int value);
  void greenSliderValueChanged(int value);
  void blueSliderValueChanged(int value);

private:
  Ui::PaletteManager *ui;

  MainWindow* _mainWindow;
  mutable QString _palettesDir {};
  mutable DLabel* _paletteLabelPtr {nullptr};
  mutable QImage _paletteImage {};
  mutable QPixmap _palettePixmap {};

  void writeSettings() const;
  void readSettings();
  QString selectImage(const QString& caption) const noexcept;
  QString selectRGBHexFile(const QString& caption) const noexcept;
  static void printQImageInfo(const QImage& qimage) noexcept;
  void orderPalette(sg::palette::Palette& palette) const noexcept;
  void orderPalette(sg::palette::Palette& palette, const bool sortPalette, const bool shufflePalette) const noexcept;
  // extractPalette: extract the palette from an image file and saves it
  bool extractPalette(const QString& fname,
                      int32_t colorsToExtract,
                      const bool sortPalette = false,
                      const bool shufflePalette = false) const noexcept;
  void makePaletteImage(const QString& rgbHexFileName) const noexcept;
  void setRGBColor() const noexcept;
};

#endif // PALETTEMANAGER_H
