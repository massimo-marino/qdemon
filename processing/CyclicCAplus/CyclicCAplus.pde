/*
Cyclic Cellular Automata
https://www.algoritmarte.com/cyclic-cellular-automata/ ‎

Place the file CyclicCAplus.pde in a CyclicCAplus folder and double click
it (or Open it from Processing).

A cyclic cellular automaton (CCA) is defined as an automaton where
each cell takes one of N states 0, 1,..., N-1 and a cell in state i
changes to state i+1 mod N at the next time step if it has a neighbor
that is in state i+1 mod N, otherwise it remains in state i at the
next time step. Classically CCA are applied on the 2-dimensional
integer lattice with von Neumann neighborhoods (nearest 4 neighbors).

But very interesting behaviors can be obtained if we pick
irregular/random neighborhoods.

In the code below, the neighboughood is defined by the neighborhood array,
an array of pairs; each pair represents the relative coordinates of the neighbor;
for example {-1,-1} represents the top-left nearest neighbor.

Each time you press the 'c' key a new generation is started
with a new random neighborhood (which is printed, if you want to use it later).
If you press the space bar you change the color palette, but not the neighborhood.

The colors are picked randomly (subdivided in random monochromatic segments).
*/

// ========================================
// main parameters to experiment with ...

// number of states of the cells (beware that if it's too big
// 4 neighbors are not enough to develop an evolving pattern
// (if it is greater than 24 you should also extend the number of neighbors)
final int CA_STATES = 21;

// the neighbors of the central cells are define in the following array
// change it (and try also to add more points)
final int vonNeumannR1Neighborhood[][] = {
  {-1,0}, {0,-1}, {0,1}, {1,0} // standard Von Neumann neighborhood r=1
};

final int vonNeumannR2Neighborhood[][] = {
  {-2,0}, {-1,-1}, {-1,0}, {-1,1}, {0,-2}, {0,-1}, {0,1}, {0,2}, {1,-1}, {1,0}, {1,1}, {2,0} // standard Von Neumann neighborhood r=2
};

final int plusR2Neighborhood[][] = {
    {-2,0}, {-1,0}, {0,-2}, {0,-1}, {0,1}, {0,2}, {1,0}, {2,0} // Plus neighborhood r=2
};

int neighborhood[][] = plusR2Neighborhood;

final int STATE_THRESHOLD = 3;

// the width of the cells (min. 1)
final int CA_CELL_WIDTH = 1;

// number of frames between every generation (larger values correspond to slower speed)
final int EVOLVEEVERY = 2; //4; 

// a fixed central region of state 0 that doesn't evolve
// (leads to more interesting "refractions")
// set isledimension = 0 to disable it
final int ISLE_DIMENSION = 0;

// ========================================

// other variables

boolean paused = true;
boolean verbose = false;

int FramesPerSecond = 60; // frames per second

// color array
final int PALETTE[] = {0xFFF40086,0xFF0076F4,0xFFa00000,0xFFed8200,0xFFdF0100,
                       0xFF8282F4,0xFFe00000,0xFFbc0000,0xFFF46200,0xFF8100F3,
                       0xFFF44001,0xFF0000c2,0xFFeFF501,0xFF8000F5,0xFFbe0000,
                       0xFF0000a6,0xFFF45501,0xFF7800F5,0xFF0100a6,0xFFF30100,
                       0xFF3300FF};
                       //{0xFF6963DE,0xFF6963DE,0xFF6963DE,0xFFF8D552,0xFFF8D552,
                       // 0xFF295E3C,0xFFD50000,0xFFD50000,0xFFF52A27,0xFFF52A27,
                       // 0xFFF52A27,0xFF9DA500,0xFF9DA500,0xFF9DA500,0xFF0000F7,
                       // 0xFF0000F7,0xFF0000F7,0xFF87A638,0xFF87A638,0xFF006000,
                       // 0xFF9225B0};
int caCols[] = {};
int currCols[] = {}; //new int[cacols.length]; // current colors
int caGrid[][][]; // two instances of a bidimensional grid
int caFrame = 0;
int caWidth; // width dimension of the CA grid
int caHeight; // height dimension of the CA grid
boolean newCA = false;
boolean newNeighborhood = false;
int generations = 0;
int stateChanges = 0;

// set this to true if you want to generate the frames for a video
boolean recVideo = false;
int videoDuration = 30; // duration in seconds
final String VIDEO_FRAMES_DIRECTORY = "/home/massimo/Videos/frames/cycleca-######.tga";
// ========================================

/**
 * Minimalistic setup
 */
void setup() {
  println("Dimensions WxH: " + width + "x" + height);
  size( 500, 500 );
//  noSmooth();
  frameRate(FramesPerSecond);
  background(0);
  caCols = new int[CA_STATES];
  if ( CA_STATES <= PALETTE.length) {
    for (int i = 0; i < CA_STATES; ++i) {
      caCols[i] = PALETTE[i];
    }
  } else {
    changePalette();
  }
  currCols = new int[caCols.length]; // current colors
  caWidth = width / CA_CELL_WIDTH;
  caHeight = height / CA_CELL_WIDTH;
  caGrid = new int[2][caWidth][caHeight];
  printNeighborhood();
  generateNewCA();
  loadPixels();
  setPixels();
  recordVideo();
  pauseNoPause();
  keyCommands();
}

/**
 * Change the palette
 */
static int mask[] = { 0xffff0000, 0xff00ff00, 0xff0000ff, 0xffffff00, 0xffff00ff, 0xff00ffff };

void changePalette()  {
  int n = 0;
  while ( n < CA_STATES ) {
    int z = irand(1, 3); // zone length (number of repeated colors for consecutive states)
    int c = 0xff000000 | (irand(10, 0xff) << 16 ) | ( irand(10, 0xff) << 8 ) | ( irand(10, 0xff) << 0);
    int m = irand(0, mask.length * 2 - 1);
    if ( m < mask.length) {
      c = c & mask[m]; // some sharpness :-)
    }
    for (int i = 0; (i < z) && (n < CA_STATES); ++i) {
      caCols[n++] = c;
    }
  }  
  String s = "Palette (cacols): {";
  for (int i = 0; i < CA_STATES; ++i) {
    s += (( i == 0 ) ? "" : ",") + "0x" + hex(caCols[i], 8);
  }
  s += "};";
  println(s);
}

void printNeighborhood() {
  String s = "Neighborhood: {";
  for (int i = 0; i < neighborhood.length; ++i) {
    s += (( i == 0 ) ? "" : ",") + "{" + neighborhood[i][0] + "," + neighborhood[i][1] + "}";
  }
  s += "};";
  println(s);  
}

void randomCA() {
  if ( newNeighborhood ) {
    final int R = 6;
    for (int i = 0; i < neighborhood.length; ++i) {
      //int i = irand( 0, pdirx.length-1);
      neighborhood[i][0] = irand(-R, R);
      neighborhood[i][1] = irand(-R, R);
    }
    printNeighborhood();
    newNeighborhood = false;
  }
  for (int x = 0; x < caWidth; ++x)
    for (int y = 0; y < caHeight; ++y) {
      caGrid[caFrame][x][y] = irand(0, CA_STATES - 1);
    }
  printStatus(true);
}

//void mousePressed() {
//}

void printStatus(boolean force) {
  if ( force || verbose ) {
    println("PAUSED: " + paused + " - frameCount: " + frameCount + " - FPS: " + FramesPerSecond + " - GEN: " + generations + " state changes: " + stateChanges);
  }
}

void keyCommands() {
  println( "Press '?' to show this help" );
  println( "Press SPACE to pause/resume processing" );
  println( "Press 'P' to change palette" );
  println( "Press 'c' to change nighborhood rules and restart" );
  println( "Press 'r' to restart with the same palette and neighborhood rules" );
  println( "Press '+' to increase the Frame Per Second (FPS) rate" );
  println( "Press '-' to decrease the Frame Per Second (FPS) rate" );
  println( "Press 's' to print current status" );
  println( "Press 'v' to change verbosity" );
  println( "Press 'Q' to quit" );
}

void pauseNoPause() {
  println("PAUSED: " + paused);
  if ( paused ) {
    printStatus(true);
    noLoop();
  } else {
    loop();
  }
}

void keyTyped() {
  if ( key == '?' ) {
    keyCommands();
    return;
  }
  if ( key == ' ' ) {
    paused = !paused;
    pauseNoPause();
    return;
  }
  if ( key == 'Q' ) {
    println("Generations run: " + generations);
    exit();
  }
  if ( key == 'P' ) {
    changePalette();
    return;
  }
  if ( key == 'c' ) {
    newCA = true;
    newNeighborhood = true;
    return;
  }
  if ( key == 'r' ) {
    newCA = true;
    return;
  }
  if ( key == '-' ) {
    if (FramesPerSecond > 1) {
      frameRate(--FramesPerSecond);
      println("FPS: " + FramesPerSecond);
    }
    return;
  }
  if ( key == '+' ) {
    frameRate(++FramesPerSecond);
    println("FPS: " + FramesPerSecond);
    return;
  }
  if ( key == 'v' ) {
    verbose = !verbose;
    println("verbose: " + verbose);
    return;
  }
  if ( key == 's' ) {
    printStatus(true);
    return;
  }
}

/**
 * Next generation
 */
void caEvolve() {
  stateChanges = 0;
  int a = caFrame;
  int b = (caFrame + 1) % 2;
  for (int x = 0; x < caWidth; ++x) {
    for (int y = 0; y < caHeight; ++y) {
       int v = caGrid[a][x][y];
       int v1 = (v + STATE_THRESHOLD) % CA_STATES;
        for (int j = 0; j < neighborhood.length; ++j) {
         int tx = (x + neighborhood[j][0] + caWidth) % caWidth;
         int ty = (y + neighborhood[j][1] + caHeight) % caHeight;
         if ( (tx >= 0) && (ty >= 0) && (tx < caWidth) && (ty < caHeight) ) {
           int vn = caGrid[a][tx][ty];
           if ( vn == v1 ) { 
             v = v1;
             ++stateChanges;
             break;
           }
         }         
       }
       caGrid[b][x][y] = v;
    }
  }
  caFrame = b;
  ++generations;
  if ( stateChanges == 0 ) { // if there are no evolving cells, change some of them randomly
    println("NO CHANGES IN THIS GENERATION: " + generations);
    //int p = 10 * (width * height) / 100;
    //for (int i = 0; i < p; ++i ) {
    //  cagrid[caframe][ irand(0,cawidth-1)][ irand(0,caheight-1) ] = irand( 0, CASTATES - 1);
    //}
  }
  printStatus(false);
}

void generateNewCA() {
  generations = 0;
  stateChanges = 0;
  newCA = false;
  randomCA();
}

void setPixels() {
  //loadPixels();  // called once in setup(): no need to call it every time
  for (int x = 0; x < width; ++x) {
    for (int y = 0; y < height; ++y) {
      int cx = x / CA_CELL_WIDTH;
      int cy = y / CA_CELL_WIDTH;
      int p = x + y * width;
      pixels[p] = currCols[ caGrid[caFrame][cx][cy] ];
    }
  }
  updatePixels();
  for (int i = 0; i < caCols.length; ++i) {
    currCols[i] = fadecolToTargetAbs(currCols[i], caCols[i], 128);
  }
}

void recordVideo() {
  if ( recVideo ) {
    if ( 1.0 * frameCount / frameRate < videoDuration ) {
      saveFrame(VIDEO_FRAMES_DIRECTORY);
    } else {
      println("Video recording stopped");
      recVideo = false;
    }
  }
}

void drawIsland() {
  if ( ISLE_DIMENSION > 0 ) {
  int cx = caWidth / 2;
  int cy = caHeight / 2;
    for (int x = 0; x < ISLE_DIMENSION; ++x) {
      for (int y = 0; y < ISLE_DIMENSION; ++y) {
        float d = abs(x) + abs(y);
        if ( d < ISLE_DIMENSION ) {
          caGrid[caFrame][cx + x][cy + y] = 0;
          caGrid[caFrame][cx + x][cy - y] = 0;
          caGrid[caFrame][cx - x][cy + y] = 0;
          caGrid[caFrame][cx - x][cy - y] = 0;
        }
      }
    }
  }
}

/**
 * The drawing
 */
void draw() {
  if ( newCA ) {
    generateNewCA();
  }
  if ( (frameCount % EVOLVEEVERY) == 0 ) {
      caEvolve();
      drawIsland();
  }
  setPixels();
  recordVideo();
}

/**
 * Add a fixed amount (rate) to the RGB components of the color
 */
int fadecolToTargetAbs(int c, int t,  int rate) {
  if ( c == t ) return t;
  int a = c & 0xff000000;
  int r = (c & 0xff0000) >> 16;
  int g = (c & 0xff00) >> 8;
  int b = c & 0xff;

  int ta = t & 0xff000000;
  int tr = (t & 0xff0000) >> 16;
  int tg = (t & 0xff00) >> 8;
  int tb = t & 0xff;

  if ( r > tr ) {
    r -= rate;
    if ( r < tr ) r = tr;
  } else {
    if ( r < tr ) {
      r += rate;
      if ( r > tr ) r = tr;      
    }
  }

  if ( g > tg ) {
    g -= rate;
    if ( g < tg ) g = tg;
  } else {
    if ( g < tg ) {
      g += rate;
      if ( g > tg ) g = tg;      
    }
  }

  if ( b > tb ) {
    b -= rate;
    if ( b < tb ) b = tb;
  } else {
    if ( b < tb ) {
      b += rate;
      if ( b > tb ) b = tb;      
    }
  }

  if ( r < 0) r = 0;
  if ( g < 0) g = 0;
  if ( b < 0) b = 0;
  if ( r > 255) r = 255;
  if ( g > 255) g = 255;
  if ( b > 255) b = 255;

  return ta | (r << 16) | (g << 8) | b;  
}

/**
 * Random int function
 */
int irand(int min, int max) {
  if ( max <= min ) return min;
  return min + round(random(0, 1)*(max - min));
}

int[][] vonNeumannNeighborhhod(int range) {
  final int DIM = 2 * range * (range + 1);
  int neighbors[][] = new int[DIM][2];

  println("range: " + range + " dim: " + DIM);
  int row = 0;
  for (int x = -range; x <= range; ++x) {
    for (int y = -range; y <= range; ++y) {
      int s = abs(x) + abs(y);
      if ( (x == 0) && (y == 0) ) continue;
      if ( abs(x) + abs(y) <= range ) {
        neighbors[row][0] = x;
        neighbors[row][1] = y;
        ++row;
      }
    }
  }
  String s = "VonNeumann neighbors range " + range + ": {";
  for (int i = 0; i < neighbors.length; ++i) {
    s += (( i == 0 ) ? "" : ",") + "{" + neighbors[i][0] + "," + neighbors[i][1] + "}";
  }
  s += "};";
  println(s);  
  return neighbors;
}
