//import java.io.File;

void settings ()
{
  // window size
  setWindowSize();

  // renderer possible options: FX2D (recommended), P2D, OPENGL
  size(WINCOLS, WINROWS, FX2D);
}

void setup ()
{
  // see: https://processing.org/reference/smooth_.html
  // With Processing 3.0, smooth() is different than before.
  // It was common to use smooth() and noSmooth() to turn on and off antialiasing within a sketch.
  // Now, because of how the software has changed, smooth() can only be set once within a sketch.
  // It can be used either at the top of a sketch without a setup(),
  // or after the size() function when used in a sketch with setup().
  // The noSmooth() function also follows the same rules.
  // The default is smooth which draws all geometry with smooth (anti-aliased) edges.
  // It improves image quality of resized images.
  // To improve the animation speed, use noSmooth() to disable smoothing of geometry,
  // images, and fonts, so a faster frameRate can be achieved.
  noSmooth();
  //smooth();
  surface.setResizable(false); 
  frameRate(30);

  descriptionTextBox  = new textBox(1, 1, HEADER_HEIGHT, 450);  // 270
  generationTextBox   = new textBox(descriptionTextBox.textBoxWidth_ + 2, 1, HEADER_HEIGHT, 220);  // 170
  stateChangesTextBox = new textBox(generationTextBox.xpos_ + generationTextBox.textBoxWidth_ + 1, 1, HEADER_HEIGHT, 400); // 250
  durationTextBox     = new textBox(stateChangesTextBox.xpos_ + stateChangesTextBox.textBoxWidth_ + 1, 1, HEADER_HEIGHT, 250); // 160
  statusTextBox       = new textBox(durationTextBox.xpos_ + durationTextBox.textBoxWidth_ + 1, 1, HEADER_HEIGHT, 170); // 145

  if ( PAUSE_AT_STARTUP )
  {
    pause();
  }
  else
  {
    restart();
    demonInit(true);
  }
}

void draw ()
{
  if ( status == RUNNING )
  {
    demonRun();
  }
}

void setWindowSize ()
{
  // window size
  WINCOLS = max(COLS + 2, 1505); // width w
  WINROWS = ROWS + 40;           // height h
}

void ended ()
{
  status = ENDED;
  statusTextBox.display("ENDED");
  noLoop();
}
void pause ()
{
  status = PAUSED;
  statusTextBox.display("PAUSED");
  noLoop();
}
void restart ()
{
  status = RUNNING;
  statusTextBox.display("RUNNING");
  loop();
}

void keyTyped ()
{
  if ( status == ENDED )
  {
    if ( key == 'H' )
    {
      exit();
    }
  }
  if ( (status == RUNNING) || (status == PAUSED) || (status == ENDED) )
  {
    if ( key == 'S' )
    {
      saveShot();
    }
  }
  if ( status == PAUSED )
  {
    // load a start file
    if ( key == 'l' )
    {
      selectFolder("Select a folder:", "selectFolder");
    }
    // (re)start
    else if ( key == 's' )
    {
      demonInit(true);
      restart();
    }
    // quit
    else if ( key == 'H' )
    {
      exit();
    }
  }
}

void keyPressed ()
{
  // pause/restart
  if ( key == ' ' )
  {
    if ( status == RUNNING )
    {
      pause();
    }
    else if ( (status == PAUSED) || (status == NOT_RUNNING) )
    {
      restart();
    }
  }
}

//void mousePressed ()
//{
  //color[] hsbPalette;
  //hsbPalette = new color[PALETTE_SIZE];

  //loadHSBPalette(HSB_PALETTE_FILENAME, hsbPalette);
  //drawHSBPalette(HUE,
  //               SATURATION,
  //               BRIGHTNESS,
  //               PALETTE_SIZE, 
  //               hsbPalette,
  //               COLS + 10,
  //               0);

  //for (int i = 0; i < PALETTE_SIZE; ++i)
  //{
  // println(i + ":\th=" + hue(hsbPalette[i]) + "\ts=" + saturation(hsbPalette[i]) + "\tb=" + brightness(hsbPalette[i])); 
  //}
//}

//void mouseReleased ()
//{

//}

//void mouseMoved ()
//{
  //color c = get(mouseX, mouseY);
  //println("x,y:\t" + mouseX + "," + mouseY + "\th=" + hue(c) + "\ts=" + saturation(c) + "\tb=" + brightness(c));
  //println("x,y:\t" + mouseX + "," + mouseY); 
//}

////////////////////////////////////////////////////////////////////////////////////////
void demonInit (boolean generateRandomImage)
{
  generation = 0;

  // window size
  setWindowSize();
  //surface.setSize(WINCOLS, WINROWS);

  setMaskOnRange();
  setRNGName();

  FRAMES_FOLDER = "frames-" + RNG_NAME + "-" + PALETTE_SIZE + "-" + RANGE + "-" + STATE_DELTA + "-" + COLS +"x" + ROWS + "/";
  HSB_PALETTE_FILENAME = "./" + FRAMES_FOLDER + "demon.conf.d" + "/" + "hsb-palette-" + PALETTE_SIZE + ".txt";
  RGB_PALETTE_FILENAME = "./" + FRAMES_FOLDER + "demon.conf.d" + "/" + "rgb-palette-" + PALETTE_SIZE + ".txt";

  if ( colorPalette == null )
  {
    colorPalette = new color[PALETTE_SIZE];
    makeHSBPalette(colorPalette,
                   HUE,
                   SATURATION,
                   BRIGHTNESS,
                   //PALETTE_SIZE,
                   HSB_PALETTE_FILENAME,
                   RGB_PALETTE_FILENAME);
  }
  else
  {
    loadHSBPalette(HSB_PALETTE_FILENAME, colorPalette);
  }

  if ( stateArrayOld == null )
  {
    stateArrayOld = new int[COLS][ROWS];
  }
  if ( stateArrayNew == null )
  {
    stateArrayNew = new int[COLS][ROWS];
  }

  pgNew = createGraphics(COLS, ROWS);
  if ( pgOld == null )
  {
    if ( generateRandomImage )
    {
      pgOld = generateRandomImage(stateArrayOld,
                                  HUE,
                                  SATURATION,
                                  BRIGHTNESS,
                                  PALETTE_SIZE,
                                  COLS,
                                  ROWS);
    }
    else
    {
      pgOld = generateImage(stateArrayOld,
                            HUE,
                            SATURATION,
                            BRIGHTNESS,
                            COLS,
                            ROWS);
    }
  }

  copy2DArrays(stateArrayOld, stateArrayNew);
  copyPGraphics(pgOld, pgNew);

  String G = nf(generation, 5);
  if ( SAVE_GENERATIONS )
  {
    String generationFileName = FRAMES_FOLDER + "generations.d" + "/" + G + ".txt";

    // save the first generation
    save2DArray(stateArrayNew, generationFileName);
  }

  // save the contents of the states array for the generation 0
  save2DArray(stateArrayNew,
              FRAMES_FOLDER + "demon.conf.d" + "/" + "gen-0.txt");
  // save the contents of pgNew for generation 0
  pgNew.save(FRAMES_FOLDER + "demon.conf.d" + "/" + nf(frameCounter, 5) + FRAME_EXTENSION);

  // check the saved array is correct
  // TODO: Test this function
  //checkSavedStateArray();

  drawImage(pgNew, 1, HEADER_HEIGHT + 3);
  descriptionTextBox.display(RNG_NAME + "-" + PALETTE_SIZE + "-" + RANGE + "-" + STATE_DELTA + "-" + COLS + "x" + ROWS);

  if ( SAVE_FRAMES )
  {
    saveFrame();
  }

/////////////////////////////////////////////////////////////////////////////////////////////////
  if ( generateRandomImage )
  {
    if ( fileExists(FRAMES_FOLDER + "demon.conf.d" + "/" + "mask") )
    {
      loadMask(mask, FRAMES_FOLDER + "demon.conf.d" + "/" + "mask");
    }
  }

  saveDemonConfig();
/////////////////////////////////////////////////////////////////////////////////////////////////
  //if ( fileExists(FRAMES_FOLDER + "demon.conf.d" + "/" + "demon.conf") )
  //{
  //  loadDemonConfig(FRAMES_FOLDER + "demon.conf.d");
  //}
  //else
  //{
  //  saveDemonConfig();
  //}

  // for testing/checking
  print2DArray(mask, "mask used");

}  // demonInit

void demonRun ()
{
  generateNewState_xy(stateArrayOld, stateArrayNew);

  //descriptionTextBox.display(RNG_NAME + "-" + PALETTE_SIZE + "-" + RANGE + "-" + STATEDELTA + "-" + COLS + "x" + ROWS);
  generationTextBox.display("Gen.: " + generation);
  stateChangesTextBox.display("St.changes: " + stateChanges);
  durationTextBox.display("T(ms): " + duration);

  if ( SAVE_FRAMES && (stateChanges != 0) )
  {
    saveFrame();
  }
}

boolean fileExists (String filename)
{
  InputStream input = createInput(filename);
  
  if ( input == null )
  {
    return false;
  }
  try
  {
    input.close();
  }
  catch (IOException e)
  {
    e.printStackTrace();
  }
  return true;
}

void setMaskOnRange ()
{
  switch (RANGE)
  { //<>//
    case 1:
    mask = mask1;
    break;

    case 2:
    mask = mask2;
    break;

    case 3:
    mask = mask3;
    break;

    case 4:
    mask = mask4;
    break;

    case 5:
    mask = mask5;
    break;

    case 6:
    mask = mask6;
    break;

    case 7:
    mask = mask7;
    break;

    default:
    println("setMaskOnRange: ERROR: range " + RANGE + " is not implemented");
    exit();
  } //<>//
}  // setMaskOnRange

void setRNGName ()
{
  switch (RNG)
  {
    // perlin noise 1 - pn
    case 1:
    {
      RNG_NAME = "pn";
    }
    break;

    // perlin noise 2 - pn2
    case 2:
    {
      RNG_NAME = "pn2";
    }
    break;

    // perlin noise 3 - pn3
    case 3:
    {
      RNG_NAME = "pn3";
    }
    break;

    // random noise 1 - rn
    case 20:
    {
      RNG_NAME = "rn";
    }
    break;

    // random noise 2 - rn2
    case 21:
    {
      RNG_NAME = "rn2";
    }
    break;

    // random noise 3 - rn3
    case 22:
    {
      RNG_NAME = "rn3";
    }
    break;

    // random noise 4 - rn4
    case 23:
    {
      RNG_NAME = "rn4";
    }
    break;

    // random noise T - rnT [Test only]
    case 2000:
    {
      RNG_NAME = "rnT";
    }
    break;
  }
}  // setRNGName

void saveShot ()
{
  // save the whole window
  save(FRAMES_FOLDER + "shots.d" + "/" + nf(generation, 5) + FRAME_EXTENSION);  
  save2DArray(stateArrayNew,
              FRAMES_FOLDER + "shots.d" + "/" + nf(generation, 5) + ".txt");
}

void saveFrame ()
{
  if ( SAVE_GRAPHICS_CONTENT_ONLY )
  {
    // save the contents of pgNew
    pgNew.save(FRAMES_FOLDER + nf(frameCounter, 5) + FRAME_EXTENSION);
  }
  else
  {
    // save the whole window
    save(FRAMES_FOLDER + nf(frameCounter, 5) + FRAME_EXTENSION);
  }
  ++frameCounter; 
}

void generateNewState_xy (int[][]stateArrayOld, int[][] stateArrayNew)
{
  int start = millis();
  int k;
  int l;
  int x;
  int y;
  int kx;
  int ly;
  int kxMin;
  int kxMax;
  int lyMin;
  int lyMax;
  int maskX;
  int maxX = COLS - 1;
  int maxY = ROWS - 1;
  int stateArrayOld_k_l;
  boolean brokenLoop = false;

  stateChanges = 0;
  ++generation;

  pgNew.beginDraw();
  for (x = 0; x < COLS; ++x)
  {
    kxMin = x - RANGE;
    kxMax = x + RANGE;

    for (y = 0; y < ROWS; ++y)
    {
      lyMin = y - RANGE;
      lyMax = y + RANGE;

      for (kx = kxMin; kx <= kxMax; ++kx)
      {
        maskX = modmap(kx - kxMin, MASK_SIZE);
        k = modmap(kx, maxX);

        for (ly = lyMin; ly <= lyMax; ++ly)
        {
          if (   (mask[maskX][modmap(ly - lyMin, MASK_SIZE)] == 0)
              //|| ((ly == y) && (kx == x))
              )
          {
            continue;
          }
          l = modmap(ly, maxY);
          stateArrayOld_k_l = stateArrayOld[k][l];

          if ( stateArrayOld_k_l == ((stateArrayOld[x][y] + STATE_DELTA) % PALETTE_SIZE) )
          //if ( stateArrayOld_k_l < ((stateArrayOld[x][y] + STATE_DELTA) % PALETTE_SIZE) )
          //if ( stateArrayOld_k_l > ((stateArrayOld[x][y] + STATE_DELTA) % PALETTE_SIZE) )
          {
            stateArrayNew[x][y] = stateArrayOld_k_l;
            pgNew.stroke(colorPalette[stateArrayOld_k_l]);
            pgNew.point(x,y);
            ++stateChanges;
            // break ly loop
            if ( BREAK_LOOP_ON_MATCH )
            {
              brokenLoop = true;
              break;
            }
          }
        }  // end for ly
        // break kx loop
        if ( brokenLoop )
        {
          brokenLoop = false;
          break;
        }
      }  // end for kx
    }  // end for y
  }  // end for  x
  pgNew.endDraw();
  drawImage(pgNew, 1, HEADER_HEIGHT + 3);
  copy2DArrays(stateArrayNew, stateArrayOld);
  //copyPGraphics(pgNew, pgOld);  // no need to copy these data
  duration = millis() - start;

  if ( stateChanges == 0 )
  {
    ended();
  }
  else if ( stateChanges == previousStateChanges )
  {
    pause();
  }

  if ( SAVE_GENERATIONS )
  {
    String G = nf(generation, 5);
    String generationFileName = FRAMES_FOLDER + "generations.d" + "/" + G + ".txt";

    save2DArray(stateArrayNew, generationFileName);
  }
  previousStateChanges = stateChanges;
}  // generateNewState

void copy2DArrays (int[][]src, int[][] dst)
{
  int cols = src.length;
  int x;

  for (x = 0; x < cols; ++x)
  {
    arrayCopy(src[x], dst[x]);
  }
}

void drawAndSaveImage (PGraphics pg, int x, int y, String fileName)
{
  pg.save(fileName);

  image(pg, x, y, COLS, ROWS);
}

void drawImage (PGraphics pg, int x, int y)
{
  image(pg, x, y, COLS, ROWS);
}

void saveImage (PGraphics pg, String fileName)
{
  pg.save(fileName);
}

PGraphics generateRandomImage (int[][] stateArray,
                               float hue,
                               float saturation,
                               float brightness,
                               int paletteSize,
                               int cols,
                               int rows)
{
  PGraphics pg = createGraphics(cols, rows);

  // draw random image
  pg.beginDraw();
  pg.colorMode(HSB, hue, saturation, brightness);

  // white color in HSB
  color hsb_white = color(0, 0, 100);
  pg.background(hue(hsb_white), saturation(hsb_white), brightness(hsb_white));

  int x = 0;
  int y = 0;
  int paletteMaxIndex = paletteSize - 1;

  switch (RNG)
  {
    // perlin noise 3 - pn3
    case 3:
    {
      float pn;
      int r;
      float k = PN3_K;
      float t = random(k);

      for (x = 0; x < cols; ++x)
      {
        for (y = 0; y < rows; ++y)
        {
          pn = noise(t);
          t = t + k;
          r = int(map(pn, 0.0, 1.0, 0, paletteMaxIndex));
          stateArray[x][y] = r;

          pg.stroke(colorPalette[r]);
          pg.point(x,y);
        }
        k = k / PN3_KD;
        //t = random(k / t);
        t = random(k / t) / t;
      }
    }
    break;

    // perlin noise 2 - pn2
    case 2:
    {
      float pn;
      int r;
      float k = PN2_K;
      float t = random(k);

      for (x = 0; x < cols; ++x)
      {
        for (y = 0; y < rows; ++y)
        {
          pn = noise(t);
          t = t + k;
          r = int(map(pn, 0.0, 1.0, 0, paletteMaxIndex));
          stateArray[x][y] = r;

          pg.stroke(colorPalette[r]);
          pg.point(x,y);
        }
        t = random(t);
        k = k + PN2_KD;
      }
    }
    break;

    // perlin noise 1 - pn
    case 1:
    {
      float pn;
      int r;
      float k = PN1_K;
      float d = PN1_D;
      float t = random(k);

      for (x = 0; x < cols; ++x)
      {
        for (y = 0; y < rows; ++y)
        {
          pn = noise(t);
          t = t + k;
          k = k + d;
          r = int(map(pn, 0.0, 1.0, 0, paletteMaxIndex));
          stateArray[x][y] = r;

          pg.stroke(colorPalette[r]);
          pg.point(x,y);
        }
        //noiseSeed((long)(random(x*10)) * (long)(random(x*1000)) * (long)(random(x*100000)));
        t = random(t);
        k = k * PN1_KM;
        d = d * PN1_DM;
      }
    }
    break;

    // random noise 1 - rn
    case 20:
    {
      int r;

      for (x = 0; x < cols; ++x)
      {
        for (y = 0; y < rows; ++y)
        {
          // random noise
          r = int(random(paletteSize));
          stateArray[x][y] = r;
          // end of random noise

          pg.stroke(colorPalette[r]);
          pg.point(x,y);
        }
      }
    }
    break;

    // random noise 2 - rn2
    case 21:
    {
      float rn;
      int r;
      float M = 2.0;
      float v;

      for (x = 0; x < cols; ++x)
      {
        for (y = 0; y < rows; ++y)
        {
          // random noise
          v = M * (x + 1) * (y + 1);  // <<--- THIS IS REALLY INTERESTING
          rn = random(v);
          r = int(map(rn, 0.0, v, 0, paletteMaxIndex));

          stateArray[x][y] = r;
          // end of random noise

          pg.stroke(colorPalette[r]);
          pg.point(x,y);
        }
      }
    }
    break;

    // random noise 3 - rn3
    case 22:
    {
      float rn;
      int r;
      float M = 2.0;
      float v;
      int Kx;
      int Ky;

      for (x = 0; x < cols; ++x)
      {
        Kx = x + 1;

        for (y = 0; y < rows; ++y)
        {
          // random noise
          Ky = y + 2;
          v = M * (Kx / Ky);  // <<--- THIS IS FUNNY [red\chaos]
          rn = random(v);
          r = int(map(rn, 0.0, v, 0, paletteMaxIndex));

          stateArray[x][y] = r;
          // end of random noise

          pg.stroke(colorPalette[r]);
          pg.point(x,y);
        }
      }
    }
    break;

    // random noise 4 - rn4
    case 23:
    {
      float rn;
      int r;
      float M = 2.0;
      float v;
      int Kx;
      int Ky;

      for (x = 0; x < cols; ++x)
      {
        Kx = x + 2;

        for (y = 0; y < rows; ++y)
        {
          // random noise
          Ky = y + 1;
          v = M * (Ky / Kx);  // <<--- THIS IS FUNNY TOO [chaos\red]
          rn = random(v);
          r = int(map(rn, 0.0, v, 0, paletteMaxIndex));

          stateArray[x][y] = r;
          // end of random noise

          pg.stroke(colorPalette[r]);
          pg.point(x,y);
        }
      }
    }
    break;

    // random noise T - rnT [Test only]
    case 2000:
    {
      float rn;
      int r;
      float M = 2.0;
      float v = 0;
      int Kx;
      int Ky;
      int vf = 1;

      for (x = 0; x < cols; ++x)
      {
        Kx = x + 1;

        for (y = 0; y < rows; ++y)
        {
          // random noise
          Ky = y + 1;
          //if ( y < (rows / 64) )
          //{
          //  if ( x < (cols / 64) )
          //  {
          //    v = M * (Kx / Ky); vf = 1;
          //  }
          //  else
          //  {
          //    v = M * (Ky / Kx); vf = 2;
          //  }
          //}
          //else
          //if ( y < (rows / 32) )
          //{
          //  if ( x < (cols / 32) )
          //  {
          //    v = M * (Kx / Ky); vf = 1;
          //  }
          //  else
          //  {
          //    v = M * (Ky / Kx); vf = 2;
          //  }
          //}
          //else
          if ( y < (rows / 16) )
          {
            if ( x < (cols / 16) )
            {
              v = M * (Kx / Ky); vf = 1;
            }
            else
            {
              v = M * (Ky / Kx); vf = 2;
            }
          }
          else
          if ( y < (rows / 8) )
          {
            if ( x < (cols / 8) )
            {
//              v = M * (Kx / Ky); vf = 1;
              v = M * (Ky / Kx); vf = 2;
            }
            else
            {
              v = M * (Kx / Ky); vf = 1;
//              v = M * (Ky / Kx); vf = 2;
            }
          }
          else
          if ( y < (rows / 4) )
          {
            if ( x < (cols / 4) )
            {
              v = M * (Kx / Ky); vf = 1;
            }
            else
            {
              v = M * (Ky / Kx); vf = 2;
            }
          }
          else
          if ( y < (rows / 2) )
          {
            if ( x < (cols / 2) )
            {
//              v = M * (Kx / Ky); vf = 1;
              v = M * (Ky / Kx); vf = 2;
            }
            else
            {
              v = M * (Kx / Ky); vf = 1;
//              v = M * (Ky / Kx); vf = 2;
            }
          }
          else
          if ( y < (3 * rows / 4) )
          {
            if ( x < (3 * cols / 4) )
            {
              v = M * (Kx / Ky); vf = 1;
//              v = M * (Ky / Kx); vf = 2;
            }
            else
            {
//              v = M * (Kx / Ky); vf = 1;
              v = M * (Ky / Kx); vf = 2;
            }
          }
          else
          if ( y < (7 * rows / 8) )
          {
            if ( x < (7 * cols / 8) )
            {
//              v = M * (Kx / Ky); vf = 1;
              v = M * (Ky / Kx); vf = 2;
            }
            else
            {
              v = M * (Kx / Ky); vf = 1;
//              v = M * (Ky / Kx); vf = 2;
            }
          }
          else
          if ( y < rows )
          {
            if ( x < cols )
            {
              v = M * (Kx / Ky); vf = 1;
//              v = M * (Ky / Kx); vf = 2;
            }
            else
            {
//              v = M * (Kx / Ky); vf = 1;
              v = M * (Ky / Kx); vf = 2;
            }
          }
          else
          {
            v = M * (Kx / Ky);// vf = 1;
            //if ( vf == 1 )
            //{
            //  v = M * (Kx / Ky);// vf = 1;
            //}
            //else
            //{
            //  v = M * (Ky / Kx);// vf = 2;
            //}
          }
          rn = random(v);
          r = int(map(rn, 0.0, v, 0, paletteMaxIndex));

          stateArray[x][y] = r;
          // end of random noise

          pg.stroke(colorPalette[r]);
          pg.point(x,y);
        }
        //randomSeed((long)(random(x+1) * random(x+2) * random(x+3)));
      }
    }
    break;
  }  // end switch (RNG)

  pg.endDraw();

  return pg;
}  // generateRandomImage

PGraphics generateImage (int[][] stateArray,
                         float hue,
                         float saturation,
                         float brightness,
                         int cols,
                         int rows)
{
  PGraphics pg = createGraphics(cols, rows);

  // draw image
  pg.beginDraw();
  pg.colorMode(HSB, hue, saturation, brightness);

  // white color in HSB
  color hsb_white = color(0, 0, 100);
  pg.background(hue(hsb_white), saturation(hsb_white), brightness(hsb_white));

  int x;
  int y;

  for (x = 0; x < cols; ++x)
  {
    for (y = 0; y < rows; ++y)
    {
      pg.stroke(colorPalette[stateArray[x][y]]);
      pg.point(x,y);
    }
  }

  pg.endDraw();

  return pg;
}  // generateImage

void print2DArray (int[][] array, String prompt)
{
  int cols = array.length;
  int rows = array[0].length;
  int x;
  int y;

  println("----- array begins [" + cols + "x" + rows + " - " + prompt + "] -----");
  for (x = 0; x < cols; ++x)
  {
    for (y = 0; y < rows; ++y)
    {
      print(array[x][y] + " ");
    }
    println();
  }
  println("----- array ends [" + prompt + "] -----");
  println();
}  // print2DArray

void save2DArray (int[][] array, String file)
{
  PrintWriter out = createWriter(file);
  int cols = array.length;
  int rows = array[0].length;
  int x;
  int y;

   out.print(rows);
   out.print(" ");
   out.println(cols);
  for (y = 0; y < rows; ++y)
  {
    for (x = 0; x < cols; ++x)
    {
      out.print(array[x][y]);
      if ( x < (cols - 1) )
      {
        out.print(" ");
      }
    }
    out.println();
  }

  out.flush();
  out.close();  
}  // save2DArray

// check the new saved array is correct
void checkSavedStateArray ()
{
  int[][] sa = new int[COLS][ROWS];
  String G = nf(generation, 5);
  String generationFileName = FRAMES_FOLDER + "generations.d" + "/" + G + ".txt";

  load2DArray(sa, generationFileName);
  for (int x = 0; x < COLS; ++x)
  {
    for (int y = 0 ; y < ROWS; ++y)
    {
      if ( stateArrayNew[x][y] != sa[x][y] )
      {
        println("checkSavedStateArray: ERROR: saved state array does not match the array in mmeory");
        exit();
      }
    }
  }
}

void load2DArray (int[][] array, String file)
{
  BufferedReader in = createReader(file);
  int cols = array.length;
  int rows = array[0].length;
  int x;
  int y;
  String line;

  try
  {
    line = in.readLine();
  }
  catch (IOException e)
  {
    try
    {
      in.close();
    }
    catch  (IOException ee)
    {
      ee.printStackTrace();
    }
    e.printStackTrace();
    return;
  }

  for (y = 0; y < rows; ++y)
  {
    try
    {
      line = in.readLine();
      int[] states = int(split(line, ' '));
      x = 0;
      for (int s : states)
      {
        array[x++][y] = s;
      }
    }
    catch (IOException e)
    {
      try
      {
        in.close();
      }
      catch  (IOException ee)
      {
        ee.printStackTrace();
      }
      e.printStackTrace();
      return;
    }
  }

  try
  {
    in.close();
  }
  catch (IOException e)
  {
    e.printStackTrace();
    return;
  }
}  // load2DArray

void setPImageXY (PImage i, int x, int y, color c)
{
  i.pixels[y * i.width + x] = c;
}

void copyPImage (PImage src, PImage dst)
{
  src.loadPixels();
  dst.loadPixels();

  arrayCopy(src.pixels, dst.pixels); 

  dst.updatePixels();
  src.updatePixels();
}

PGraphics copyPImage2PGraphics (PImage img)
{
  PGraphics pgimg = createGraphics(COLS, ROWS);

  pgimg.beginDraw();
  pgimg.copy(img,0,0,COLS,ROWS,0,0,COLS,ROWS);
  pgimg.endDraw();
  
  return pgimg;
}
PGraphics copyPImage2PGraphics (PImage img, PGraphics pgimg)
{
  if ( pgimg == null )
  {
    pgimg = createGraphics(COLS, ROWS);
  }
  pgimg.beginDraw();
  pgimg.copy(img,0,0,COLS,ROWS,0,0,COLS,ROWS);
  pgimg.endDraw();
  
  return pgimg;
}

void setPGraphicsXY (PGraphics i, int x, int y, color c)
{
  i.pixels[y * i.width + x] = c;
}

void copyPGraphics (PGraphics pgSrc, PGraphics pgDst)
{
  //pgSrc.beginDraw();
  //pgSrc.loadPixels();
  pgDst.beginDraw();
  pgDst.loadPixels();

  arrayCopy(pgSrc.pixels, pgDst.pixels); 

  pgDst.updatePixels();
  pgDst.endDraw();
  //pgSrc.updatePixels();
  //pgSrc.endDraw();
}

int rgbRed (color c)
{
  return ((c >> 16) & 0xFF);
}

int rgbGreen (color c)
{
  return ((c >> 8) & 0xFF);
}

int rgbBlue (color c)
{
  return (c & 0xFF);
}

// if v is in [0, max] then return v
// if v < 0 or v > max then return a map of v in [0,max]
int modmap (int v, int max)
{
  if ( (v >= 0) && (v <= max) )
  {
    return v;
  }
  // range = max - min + 1; here min is 0
  int range = max + 1;

  if ( v > max )
  {
    return (v % range); 
  }

  //if ( v < 0 )
  return ((range - (abs(v) % range)) % range);
}  // modmap

void selectFolder (File selection)
{
  if (selection == null)
  {
    println("Warning: Window was closed or the user hit cancel.");
  }
  else
  {
    // demon.conf.d
    String demonConfigDir = selection.getAbsolutePath();

    println("Demon Conf Dir selected: " + demonConfigDir + " - selection.getParentFile(): " + selection.getParentFile());
    // loadDemonConfig() sets: RNG, PALETTE_SIZE, RANGE, STATEDELTA, COLS, ROWS
    loadDemonConfig(demonConfigDir);
    stateArrayOld = new int[COLS][ROWS];
    colorPalette = new color[PALETTE_SIZE];

    boolean generateInitialRandomImage = true;
    String gen0FileName = demonConfigDir + "/" + "00000.txt";

    if ( fileExists(gen0FileName) )
    {
      load2DArray(stateArrayOld, gen0FileName);
      generateInitialRandomImage = false;
    }

    // demonInit() does NOT generate an initial random image
    // if it was loaded from gen 0 file (otherwise it is generated),
    // and loads the mask used to save the random image
    demonInit(generateInitialRandomImage);
    restart();
  }
}  // selectFolder

void saveMask (int[][] maskArray, String file)
{
  save2DArray(maskArray, file);
}

void loadMask (int[][] maskArray, String file)
{
  load2DArray(maskArray, file);
}

void saveDemonConfig ()
{
  int DECIMALS = 16;
  String demonConfigFile = FRAMES_FOLDER + "demon.conf.d" + "/" + "demon.conf";
  String maskFile = FRAMES_FOLDER + "demon.conf.d" + "/" + "mask";

  PrintWriter outf = createWriter(demonConfigFile);

  outf.print(RNG + "-" + PALETTE_SIZE + "-" +
             RANGE + "-" + STATE_DELTA + "-" + COLS + "x" + ROWS + " " +
             nf(PN1_K,1,DECIMALS) + " " + nf(PN1_KM,1,DECIMALS) + " " +
             nf(PN1_D,1,DECIMALS) + " " + nf(PN1_DM,1,DECIMALS) + " " +
             nf(PN2_K,1,DECIMALS) + " " + nf(PN2_KD,1,DECIMALS) + " " +
             nf(PN3_K,1,DECIMALS) + " " + nf(PN3_KD,1,DECIMALS));
  outf.flush();
  outf.close();

  saveMask(mask, maskFile); //<>//
}  // saveDemonConfig

void loadDemonConfig (String demonConfigDir)
{
  String demonConfigFile = "";

  if ( demonConfigDir == "" )
  {
    demonConfigDir = FRAMES_FOLDER + "demon.conf.d";
  }
  demonConfigFile = demonConfigDir + "/" + "demon.conf";

  BufferedReader reader = createReader(demonConfigFile);
  String line = null;

  try
  {
    line = reader.readLine();
    String[] tokens = splitTokens(line, "-xX ");

    RNG = int(tokens[0]);
    PALETTE_SIZE = int(tokens[1]);
    RANGE = int(tokens[2]);
    STATE_DELTA = int(tokens[3]);
    COLS = int(tokens[4]);
    ROWS = int(tokens[5]);
    if ( tokens.length > 6 )
    {
      PN1_K  = float(tokens[6]);
      PN1_KM = float(tokens[7]);
      PN1_D  = float(tokens[8]);
      PN1_DM = float(tokens[9]);
      if ( tokens.length == 14 )
      {
        PN2_K  = float(tokens[10]);
        PN2_KD = float(tokens[11]);
        PN3_K  = float(tokens[12]);
        PN3_KD = float(tokens[13]);
      }
    }
    else
    {
      // values used when they were not saved in demon.conf
      // don't change these values
      PN1_K  = 0.0002000000000000;
      PN1_KM = 0.9911800000000000;
      PN1_D  = 0.0009870000000000;
      PN1_DM = 0.1111990000000000;
      PN2_K  = 0.9999990000000000;
      PN2_KD = 0.0000010000000000;
      PN3_K  = 0.0009990000000000;
      PN3_KD = 0.9790000000000000;
    }
    println("loadDemonConfig: " + RNG + "-" + PALETTE_SIZE + "-" +
            RANGE + "-" + STATE_DELTA + "-" + COLS + "x" + ROWS + " " +
            PN1_K + " " + PN1_KM + " " + PN1_D + " " + PN1_DM + " " +
            PN2_K + " " + PN2_KD + " " +
            PN3_K + " " + PN3_KD);
    reader.close();
  }
  catch (IOException e)
  {
    e.printStackTrace();
  }
  setMaskOnRange();
  // TODO: check if the mask's file does not exist: create a new one in that case
  loadMask(mask, demonConfigDir + "/" + "mask");
}  // loadDemonConfig

// EOF demon.pde
