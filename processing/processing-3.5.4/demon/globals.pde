//// Configurable parameters
boolean PAUSE_AT_STARTUP = false;
boolean SAVE_FRAMES = false;
boolean BREAK_LOOP_ON_MATCH = true;
// picture size: COLS x ROWS, or, w x h
//  500x500  px -> 4.2x4.2 cm
// 5000x2500 px -> 42x20 cm
// 5906x3500 px -> 50x30 cm
// 5906x5906 px -> 50x50 cm
// 7000x3500 px -> 60x30 cm
// 9000x4500 px -> 76x38 cm
// 9500x4750 px -> 80x40 cm
// A4 is 21.0x29.7 cm or 210x297 mm
int COLS = 500;  //500;  //1800;  //1500; //600;  // width w
int ROWS = 500;  //6000; //1000;  //1500; //2000; // height h
// number of states/colors
// 42-4-12
int STATES = 21; //256; //96; //55;  //36; //42;  //36; //64;
int RANGE = 2; //5; // 1 -> mask1, 2 -> mask2, 3 -> mask3, ..., n -> maskn
//int STATE_DELTA = STATES-5;
//int STATE_DELTA = STATES-3;
//int STATE_DELTA = STATES-1;
int STATE_DELTA = 6; //3; //220;

int MASK_SIZE = RANGE * 2 + 1;

// mask used for range 1 - 3x3
int[][] mask1 = {{1,1,1},
                 {1,0,1},
                 {1,1,1}
                };
// mask used for range 2 - 5x5
int[][] mask2_Moore = {{1,1,1,1,1},
                 {1,1,1,1,1},
                 {1,1,0,1,1},
                 {1,1,1,1,1},
                 {1,1,1,1,1}
                };
int[][] mask2 = {{0,0,1,0,0},
                 {0,0,1,0,0},
                 {1,1,0,1,1},
                 {0,0,1,0,0},
                 {0,0,1,0,0}
                };
// mask used for range 3 - 7x7
int[][] mask3 = {{1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1},
                 {1,1,1,0,1,1,1},
                 {1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1}
                };
// mask used for range 4 - 9x9
int[][] mask4 = {{1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,0,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1}
                };
// mask used for range 5 - 11x11
int[][] mask5 = {{1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,0,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1}
                };
// mask used for range 6 - 13x13
int[][] mask6 = {{1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,0,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1}
                };
int[][] mask6_1={{1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,0,0,0,0,0,0,0,0,0,0,0,1},
                 {1,0,1,1,1,1,1,1,1,1,1,0,1},
                 {1,0,1,0,0,0,0,0,0,0,1,0,1},
                 {1,0,1,0,1,1,1,1,1,0,1,0,1},
                 {1,0,1,0,1,0,0,0,1,0,1,0,1},
                 {1,0,1,0,1,0,0,0,1,0,1,0,1},
                 {1,0,1,0,1,0,0,0,1,0,1,0,1},
                 {1,0,1,0,1,1,1,1,1,0,1,0,1},
                 {1,0,1,0,0,0,0,0,0,0,1,0,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,0,0,0,0,0,0,0,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1}
                };
// mask used for range 7 - 15x15
int[][] mask7 = {{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,0,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                 {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
                };


int[][] mask;

float HUE = 360.0;        // ° [0.0,360.0]
float SATURATION = 100.0; // % [0.0,100.0]
float BRIGHTNESS = 100.0; // % [0.0,100.0]

//////////////////////////////////////////////////////////////////////////

int PERLIN_NOISE_1 = 1;

//float PN1_K  = 0.0002000000000000;  // *
float PN1_K    = 0.9009000000000000;
//float PN1_KM = 0.9911800000000000;  // *0 - the bigger the more right shifted and viceversa
//float PN1_KM = 0.9911100000000000;  // 1
//float PN1_KM = 0.9910000000000000;  // 2
//float PN1_KM = 0.9909000000000000;  // 3
//float PN1_KM = 0.9000000000000000;  // 4
//float PN1_KM = 0.9979990000000000;  // 5
//float PN1_KM = 0.9984000000000000;  // 5.1 3/4 for 9500x4750
//float PN1_KM = 0.9974000000000000;  // 5.2 1/2 for 9500x4750
float PN1_KM   = 0.9900000000000000;  // 6

//float PN1_D  = 0.0009870000000000;  // *
float PN1_D    = 1.0000000000000000;
//float PN1_DM = 0.1111990000000000;  // *0
//float PN1_DM = 0.9099990000000000;  // 1
//float PN1_DM = 0.9800000000000000;  // 2
//float PN1_DM = 0.0009990000000000;  // 3
//float PN1_DM = 0.9999990000000000;  // 4
//float PN1_DM = 0.9990000000000000;  // 5
float PN1_DM   = 2.0000000000000000;  // 6

//////////////////////////////////////////////////////////////////////////

int PERLIN_NOISE_2 = 2;

float PN2_K  = 0.9999990000000000;  // *
//float PN2_KD = 0.0000010000000000;  // *
//float PN2_KD = 0.0000100000000000;  //
//float PN2_KD = 0.00000880000000000;  // ok
float PN2_KD = 0.0100010000000000;  //

//////////////////////////////////////////////////////////////////////////
// waterfall/curved effect
int PERLIN_NOISE_3 = 3;

//float PN3_K  = 0.0000000000000001;  //   ok - can be greater, BUT NOT ZERO
//float PN3_K  = 0.0000000000000010;  //   ok - can be greater
//float PN3_K  = 0.0000000000000100;  //   ok - can be greater
//float PN3_K  = 0.0000000000001000;  //   ok - can be greater
//float PN3_K  = 0.0000000000010000;  //   ok - can be greater
//float PN3_K  = 0.0000000000100000;  //   ok - can be greater
//float PN3_K  = 0.0000000001000000;  //   ok - can be greater
//float PN3_K  = 0.0000000010000000;  //   ok - can be greater
//float PN3_K  = 0.0000000100000000;  //   ok - can be greater
//float PN3_K  = 0.0000001000000000;  //   ok - can be greater
//float PN3_K  = 0.0000010000000000;  //   ok - can be greater
//float PN3_K  = 0.0000090000000000;  //   ok - can be greater
//float PN3_K  = 0.0000990000000000;  //   ok - can be greater
//float PN3_K  = 0.0009990000000000;  //   ok - can be greater
//float PN3_K  = 0.0099990000000000;  //   ok - can be greater
//float PN3_K  = 0.0999990000000000;  //   ok - can be greater
//float PN3_K  = 0.9999990000000000;  //   ok - can be greater
//float PN3_K  = 1.0000000000000000;  // * ok - can be greater
//float PN3_K  = 1.0000090000000000;  //   ok - can be greater
//float PN3_K  = 1.0000990000000000;  //   ok - can be greater
//float PN3_K  = 1.0009990000000000;  //   ok - can be greater
//float PN3_K  = 1.0099990000000000;  //   ok - can be greater
//float PN3_K  = 1.0999990000000000;  //   ok - can be greater
//float PN3_K  = 1.9999990000000000;  //   ok - can be greater
//float PN3_K  = 2.0000000000000000;  //   ok - can be greater
//float PN3_K  = 2.0000090000000000;  //   ok - can be greater ... and so on
float PN3_K    = 0.0000000090000000;  // choose your value
//float PN3_KD = 0.0000999999999999;  //   ok - can be greater, smaller not interesting, NOT 0
//float PN3_KD = 0.0001000000000000;  //   ok - can be greater
//float PN3_KD = 0.0010000000000000;  //   ok - can be greater
//float PN3_KD = 0.0100000000000000;  //   ok - can be greater
//float PN3_KD = 0.1000000000000000;  //   ok - can be greater
//float PN3_KD = 0.9790000000000000;  // *
float PN3_KD   = 0.9790000000000000;  // choose your value

//////////////////////////////////////////////////////////////////////////

int RANDOM_NUMBER_GENERATOR_1 = 20;
int RANDOM_NUMBER_GENERATOR_2 = 21;
int RANDOM_NUMBER_GENERATOR_3 = 22;
int RANDOM_NUMBER_GENERATOR_4 = 23;
int RANDOM_NUMBER_GENERATOR_T = 2000;

//////////////////////////////////////////////////////////////////////////
// RNG := RANDOM_NUMBER_GENERATOR_N [N := 1,2,3,4,T] | PERLIN_NOISE_N [N := 1,2,3]
int RNG = RANDOM_NUMBER_GENERATOR_1;
//int RNG = PERLIN_NOISE_2;

//////////////////////////////////////////////////////////////////////////
// make movie
boolean SAVE_GRAPHICS_CONTENT_ONLY = true;
String TIF = ".tif";
String TGA = ".tga";
String JPG = ".jpg";
String PNG = ".png";
String FRAME_EXTENSION = PNG;  //TGA;

// save generation states (state numbers)
boolean SAVE_GENERATIONS = false;

String FRAMES_FOLDER = "";
String HSB_PALETTE_FILENAME = "";
String RGB_PALETTE_FILENAME = "";

//// End of configurable parameters
///////////////////////////////////////////////////////////////////////////////////////
// window size
int WINCOLS;  // window width w
int WINROWS;  // window height h

int HEADER_HEIGHT = 35;

int PALETTE_SIZE = STATES;
// the set of colors used is an array of colors
color[] colorPalette = null;
// stateArrayOld[i][j], or stateArrayNew[i][j] is the color index
// used to get the color from the palette colorPalette: colorPalette[stateArrayXxx[i][j]]
int[][] stateArrayOld = null;
int[][] stateArrayNew = null;

PGraphics pgOld = null;
PGraphics pgNew = null;

int stateChanges = 0;
int previousStateChanges = -1;
int generation = 0;
int duration = 0;

int frameCounter = 0;

textBox generationTextBox;
textBox stateChangesTextBox;
textBox durationTextBox;
textBox descriptionTextBox;
textBox statusTextBox;

int NOT_RUNNING = 0;
int RUNNING     = 1;
int PAUSED      = 2;
int ENDED       = 3;
int status = NOT_RUNNING;

String RNG_NAME = "";

// EOF globals.pde
