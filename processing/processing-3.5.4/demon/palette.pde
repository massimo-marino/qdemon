
void loadHSBPalette (String file, color[] hsbPalette)
{
  BufferedReader reader;
  String line;

  reader = createReader(file);
  try
  {
    line = reader.readLine();
  } catch (IOException e)
  {
    e.printStackTrace();
    line = null;
    noLoop();
    try
    {
      reader.close();
    } catch (IOException ee)
    {
      ee.printStackTrace();
    }
    return;
  }

  float[] hsbValues = float(split(line, ' '));
  
  float hue = hsbValues[0];
  float saturation = hsbValues[1];
  float brightness = hsbValues[2];
  
  colorMode(HSB, hue, saturation, brightness);

  //println("FROM FILE: h=" + hue + "\ts=" + saturation + "\tb=" + brightness);
  
  boolean loop = true;

  while (loop)
  {
    try
    {
      line = reader.readLine();
    } catch (IOException e)
    {
      e.printStackTrace();
      line = null;
      noLoop();
      try
      {
        reader.close();
      } catch (IOException ee)
      {
        ee.printStackTrace();
      }
      loop = false;
      continue;
    }
    if (line != null)
    {
      float[] tokens = float(split(line, ' '));
      
      int colIdx = int(tokens[0]);
      float colHue = tokens[1];
      float colSaturation = tokens[2];
      float colBrightness = tokens[3];
      
      //println("FROM FILE: colIdx: " + colIdx + "\th=" + colHue + "\ts=" + colSaturation + "\tb=" + colBrightness);

      color c = color(colHue, colSaturation, colBrightness);
      hsbPalette[colIdx] = c;
    }
    else
    {
      loop = false;
    }
  }
  
  try
  {
    reader.close();
  } catch (IOException e)
  {
    e.printStackTrace();
    noLoop();
    return;
  }
}

void saveHSBPalette (color[] colorPalette,
                     String file,
                     float hue,
                     float saturation,
                     float brightness)
{
  PrintWriter outpalette = createWriter(file);
  
  outpalette.println(hue + " " + saturation + " " + brightness);
  for (int p = 0; p < colorPalette.length; ++p)
  {
    outpalette.println(p + " " + hue(colorPalette[p]) + " " + saturation(colorPalette[p]) + " " + brightness(colorPalette[p]));
  }
  outpalette.flush();
  outpalette.close();  
}

void saveRGBPalette (color[] colorPalette, String file)
{
  PrintWriter outpalette = createWriter(file);

  for (int p = 0; p < colorPalette.length; ++p)
  {
    outpalette.println(p + " " + rgbRed(colorPalette[p]) + " " + rgbGreen(colorPalette[p]) + " " + rgbBlue(colorPalette[p]));
  }
  outpalette.flush();
  outpalette.close();  
}

void drawHSBPalette (float hue,
                     float saturation,
                     float brightness,
                     int paletteSize,
                     color[] hsbPalette,
                     int palettePosX,
                     int palettePosY)
{
  int d = 15;
  int x = 0;
  int y = 0;
  int w = d;
  int h = 100;
  color c;  
  color hsb_white = color(0,0,100);
  
  PGraphics pg;

  pg = createGraphics(paletteSize * d, h);

  // Draw hsb palette
  pg.beginDraw();
  pg.colorMode(HSB, hue, saturation, brightness);
  pg.background(hue(hsb_white), saturation(hsb_white), brightness(hsb_white));
  pg.noStroke();
  
  for (int i = 0; i < paletteSize; ++i)
  {
    c = hsbPalette[i];
    //println(hue(c) + " " + saturation(c) + " " + brightness(c));
    pg.fill(hue(c), saturation(c), brightness(c));
    pg.rect(x, y, w, h);
    x = x + d;
  }

  pg.endDraw();
  image(pg, palettePosX, palettePosY, d * paletteSize, h);
}

void makeHSBPalette (color[] colorPalette,
                     float hue,
                     float saturation,
                     float brightness,
                     //int paletteSize,
                     String hsbPaletteFilename,
                     String rgbPaletteFilename)
{
  if ( (colorPalette == null) || (colorPalette.length == 0) )
  {
    // error
    println("makeHSBPalette: ERROR: colorPalette is empty.");
    exit();
  }

  colorMode(HSB, hue, saturation, brightness);

  int d = 3;
  int x = 0;
  float i = 0.0;
  int counter = 0;
  color c;
  float incr = (360.0 / colorPalette.length);

  noStroke();
  for (i = 0.0; (i < 360.0) && (counter < colorPalette.length); i = i + incr)
  {
    c = color(i, saturation, brightness);
    colorPalette[counter] = c;
    x = x + d;
    ++counter;
  }
  //println("x: " + x);
  //println("counter: " + counter);

  if ( hsbPaletteFilename != "" )
  {
    saveHSBPalette(colorPalette, hsbPaletteFilename, hue, saturation, brightness);
  }
  if ( rgbPaletteFilename != "" )
  {
    saveRGBPalette(colorPalette, rgbPaletteFilename);
  }
}

// EOF palette.pde
