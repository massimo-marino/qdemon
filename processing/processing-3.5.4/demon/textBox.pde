class textBox
{
  color strokeColor_;
  color textBoxFillColor_;
  color textColor_;
  int xpos_;
  int ypos_;
  int textBoxHeight_;
  int textBoxWidth_;
  int textPosX_;
  int textPosY_;
  int fontSize_;
  PFont f_;
  String textDisplayed_;
  boolean placing_;
   
  textBox (int xpos, int ypos, int textBoxHeight, int textBoxWidth)
  {
    placing_ = false;
    xpos_ = xpos;
    ypos_ = ypos;
    textPosX_ = xpos_ + 10;
    textPosY_ = ypos_ + 25;
    textBoxHeight_ = textBoxHeight;
    textBoxWidth_ = textBoxWidth;
    strokeColor_ = color(0,0,0);
    textBoxFillColor_ = color(255, 255, 0);  // yellow
    textColor_ = color(255, 0, 0);
    fontSize_ = 32;
    f_ = createFont("Courier", fontSize_, true);
    textFont(f_, fontSize_);
  }

  int getX()
  {
    return xpos_;
  }

  int getY()
  {
    return ypos_;
  }

  boolean mouseIsIn ()
  {
    return ( ((mouseX >= xpos_) && (mouseX <= xpos_ + textBoxWidth_)) &&
             ((mouseY >= ypos_) && (mouseY <= ypos_ + textBoxHeight_)) );
  }

  void clear ()
  {
    stroke(204);
    fill(204);
    rect(xpos_, ypos_, textBoxWidth_, textBoxHeight_);
  }

  void display ()
  {
    stroke(strokeColor_);
    fill(textBoxFillColor_);
    rect(xpos_, ypos_, textBoxWidth_, textBoxHeight_);
    fill(textColor_);
    text(textDisplayed_, textPosX_, textPosY_);
  }

  void display (String text)
  {
    textDisplayed_ = text;
    display();
  }

  void setPlacing ()
  {
    if (mouseIsIn())
    {
      placing_ = true;
    }
  }

  void resetPlacing ()
  {
    placing_ = false; 
  }

  void replace ()
  {
    if ( placing_ )
    {
      clear();
      place(); 
   }
  }

  void place ()
  {
    xpos_ = mouseX;
    ypos_ = mouseY;
    textPosX_ = xpos_ + 10;
    textPosY_ = ypos_ + 25;
    display();
  }
  
  void test (int i)
  {
    display("Hi folks: " + i);
  }
}  // class textBox

// EOF textBox.pde
