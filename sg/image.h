//
// image.h
//
// Created by massimo on 10/13/18.
//
#pragma once

#include "rgb.h"

#include <vector>
#include <fstream>
#include <sstream>
#include <exception>
#include <memory>
#include <zlib.h>
#include <png.h>
////////////////////////////////////////////////////////////////////////////////

namespace sg {

using pixels_t = rgb::pixels_t;

#pragma pack (push, 1)

class image {
protected:
  // image width in pixels
  mutable uint32_t _width {0};
  // image height in pixels
  mutable uint32_t _height {0};
  // total number of pixels
  mutable uint32_t _size {0};
  // the pixel image
  mutable pixels_t _data {};

  bool
  checkGraphicFile(const std::string &path, const std::string& magic, const ulong offset = 0) const noexcept(false)
  {
    if (std::ifstream ifs {path, std::ios_base::binary})
    {
      const auto numChars {magic.length()};
      auto magicFromFile {std::make_unique<char[]>(numChars + 1)};
      size_t i {0};

      ifs.seekg(offset);
      for (i = 0; i < numChars; ++i)
      {
        ifs >> magicFromFile[i];
      }
      magicFromFile[i] = '\0';
      return (static_cast<std::string>(magicFromFile.get()) == magic);
    }
    return false;
  }  // checkGraphicFile

public:
  // create an empty default 100x100-sized image object
  image() :
  _width(100),
  _height(100),
  _size(100 * 100),
  _data(static_cast<size_t>(100 * 100))
  {
    _data.reserve(_size);
  }

  // create an "empty" image with a given _width and _height; _data is filled with zeros
  image(const uint32_t _width, const uint32_t _height) :
  _width(_width),
  _height(_height),
  _size(_width * _height),
  _data(static_cast<size_t>(_width * _height))
  {
    _data.reserve(_size);
  }

  // default dtor
  virtual ~image() = default;

  // default copy ctor
  // usage:
  // image obj{};
  // image copiedObj{obj};
  image(const image &rhs) = default;

  // default copy assignment
  // usage:
  // image obj{};
  // image copiedObj;
  // copiedObj = obj;
  image&
  operator=(const image &rhs) = default;

  // default move ctor
  // The move constructor is called whenever selected by overload resolution,
  // which typically occurs when an object is initialized (by direct-initialization
  // or copy-initialization) from rvalue (xvalue or prvalue) (until C++17)
  // xvalue (since C++17) of the same type, including:
  //
  // initialization: T a = std::move(b); or T a(std::move(b));, where b is of type T;
  // function argument passing: f(std::move(a));, where a is of type T and f is void f(T t);
  // function return: return a; inside a function such as T f(), where a is of type T which has a move constructor.
  image(image &&rhs) = default;

  // default move assignment operator
  image&
  operator=(image &&rhs) = default;

  uint32_t
  width() const noexcept
  {
    return _width;
  }

  uint32_t
  height() const noexcept
  {
    return _height;
  }

  image&
  setRGB(const size_t x, const size_t y, const rgb::RGB& color) noexcept
  {
    _data[x + y * _width].setRGB(color);
    return *this;
  }

  image&
  setRGB(const size_t index, const rgb::RGB& color) noexcept
  {
    _data[index].setRGB(color);
    return *this;
  }

  image&
  setR(const size_t index, const u_char r) noexcept {
    _data[index].setR(r);
    return *this;
  }

  image&
  setR(const size_t x, size_t y, const u_char r) noexcept {
    _data[x + y * _width].setR(r);
    return *this;
  }

  image&
  setG(const size_t index, const u_char g) noexcept {
    _data[index].setG(g);
    return *this;
  }
  image&
  setG(const size_t x, size_t y, const u_char g) noexcept {
    _data[x + y * _width].setG(g);
    return *this;
  }

  image&
  setB(const size_t index, const u_char b) noexcept {
    _data[index].setB(b);
    return *this;
  }

  image&
  setB(const size_t x, size_t y, const u_char b) noexcept {
    _data[x + y * _width].setB(b);
    return *this;
  }

  image&
  setRGB(const size_t index, const u_char r, const u_char g, const u_char b) noexcept {
    _data[index].setRGB(r, g, b);
    return *this;
  }

  image&
  setRGB(const size_t x, const size_t y, const u_char r, const u_char g, const u_char b) noexcept {
    _data[x + y * _width].setRGB(r, g, b);
    return *this;
  }

  rgb::RGB&
  getRGB(const size_t x, const size_t y) const noexcept
  {
    return _data[y * _width + x];
  }

  rgb::RGB&
  getRGB(const size_t index) const noexcept
  {
    return _data[index];
  }

};  // class image

class png final : public image {
private:
  unsigned long _bpp {3};

public:
  // create a PNG default-sized object
  png() :
  image()
  {}

  // create an "empty" PNG with a given _width and _height; _data is filled with zeros
  png(const uint32_t _width, const uint32_t _height, const unsigned long _bpp = 3) :
  image(_width, _height),
  _bpp(_bpp)
  {}

  ~png() = default;

  // png signature: 0x?? 0x50 0x4E 0x47
  bool
  isPNG(const std::string& fname) const noexcept(false)
  {
    return checkGraphicFile(fname, "PNG", 1);
  }

  bool
  isPNGlib(const std::string& fname) const noexcept(false)
  {
    std::ifstream inf(fname, std::ios::in | std::ofstream::binary);
    if ( !inf.is_open() )
    {
      return false;
    }

    const png_size_t numToCheck {64};
    char header[numToCheck] {};

    inf.read(header, numToCheck);

    int is_png {!png_sig_cmp(reinterpret_cast<png_bytep>(header), static_cast<png_size_t>(0), numToCheck)};
    if ( !is_png )
    {
      return false;
    }
    return true;
  }  // isPNG

  std::vector<unsigned char>
  write() const noexcept(false)
  {
    struct pngimage_t
    {
      std::vector <unsigned char> _pngData {};
    };

    pngimage_t pngImage;
    png_infop info_ptr;
    png_structp png_ptr;

    u_char r {};
    u_char g {};
    u_char b {};
    u_char a {};

    pngImage._pngData.reserve(_data.size() * _bpp);

    for (const auto& c : _data)
    {
      r = c.Red();
      g = c.Green();
      b = c.Blue();
      pngImage._pngData.emplace_back(r);
      pngImage._pngData.emplace_back(g);
      pngImage._pngData.emplace_back(b);
      if ( 4 == _bpp )
      {
        a = c.Alpha();
        pngImage._pngData.emplace_back(a);
      }
    }

    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);

    if ( !png_ptr ) {throw std::runtime_error( "png_create_write_struct" ); }

    info_ptr = png_create_info_struct(png_ptr);

    if ( !info_ptr ) {throw std::runtime_error( "png_create_info_struct" ); }

    if ( 3 == _bpp ) png_set_IHDR(png_ptr, info_ptr, _width, _height,
                                  8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
                                  PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
    if ( 4 == _bpp ) png_set_IHDR(png_ptr, info_ptr, _width, _height,
                                  8, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE,
                                  PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

    // set the zlib compression level
    png_set_compression_level(png_ptr, Z_BEST_SPEED);

    std::vector<unsigned char> file_content;

    png_set_write_fn(png_ptr, &file_content,
                     [] (const png_structp _png_ptr, const png_bytep _data, const png_size_t _length)
                     {
                       std::vector<unsigned char> &fileContent = *(reinterpret_cast<std::vector<unsigned char>*>(png_get_io_ptr(_png_ptr)));
                       fileContent.reserve(_length);
                       for (uint32_t i {0}; i < _length; ++i)
                       {
                         fileContent.emplace_back(_data[i]);
                       }
                     }, nullptr);

    png_write_info(png_ptr, info_ptr);

    std::vector<png_bytep> row_pointers(_height);

    for (uint32_t y {0}; y < _height; ++y)
    {
      row_pointers[y] = &pngImage._pngData[y * png_get_rowbytes(png_ptr, info_ptr)];
    }
    png_write_image(png_ptr, &row_pointers[0]);
    png_write_end(png_ptr, nullptr);
    png_destroy_write_struct(&png_ptr, &info_ptr);
    return file_content;
  }  // write

  bool
  write(const std::string &fname)
  {
    std::vector<unsigned char> data_to_write {write()};

    std::ofstream outf(fname, std::ios::out | std::ofstream::binary);
    std::copy(data_to_write.begin(), data_to_write.end(), std::ostreambuf_iterator<char>(outf));

    return true;
  }  // write_png_file
};  // class png

class ppm final : public image {
private:
  mutable uint32_t _max_col_val {255};

public:
  // create a PPM default-sized object
  ppm() :
  image(),
  _max_col_val(255)
  {}

  // create an "empty" PPM with a given _width and _height; _data is filled with zeros
  ppm(const uint32_t _width, const uint32_t _height, uint32_t _max_col_val = 255) :
  image(_width, _height),
  _max_col_val(_max_col_val)
  {}

  ~ppm() = default;

  // ppm signature: 0x50 0x36
  bool
  isPPM(const std::string& fname) const noexcept(false)
  {
    return checkGraphicFile(fname, "P6");
  }

  // read a PPM image from fname
  bool
  read(const std::string& fname) const noexcept(false) {
    std::ifstream ifs(fname.c_str(), std::ios::in | std::ios::binary);

    if (ifs.is_open()) {
      std::string line;
      std::getline(ifs, line);
      if (line != "P6") {
        std::cerr << "Error: Unrecognized file format:"
                  << fname
                  << " is not a PPM file.\n";
        return false;
      }

      std::getline(ifs, line);
      while (line[0] == '#') {
        std::getline(ifs, line);
      }
      std::stringstream dimensions(line);

      try {
        dimensions >> _width;
        dimensions >> _height;
      }
      catch (std::exception &e) {
        std::cerr << "Header file format error. "
                  << e.what()
                  << "\n";
        return false;
      }

      std::getline(ifs, line);
      std::stringstream max_val(line);
      try {
        max_val >> _max_col_val;
      }
      catch (std::exception &e) {
        std::cerr << "Header file format error. "
                  << e.what()
                  << "\n";
        return false;
      }

      _size = _width * _height;
      _data.clear();
      _data.resize(_size);
      _data.reserve(_size);

      char r {};
      char g {};
      char b {};
      for (size_t i{0}; i < _size; ++i) {
        ifs.get(r);
        ifs.get(g);
        ifs.get(b);
        _data[i].setRGB(r, g, b);
      }
    } else {
      std::cerr << "Error: Unable to open file "
                << fname
                << "\n";
      return false;
    }
    return true;
  }  // read

  // write a PPM image in fname
  bool
  write(const std::string& fname) const noexcept(false) {
    std::ofstream ofs(fname.c_str(), std::ios::out | std::ios::binary);

    if (ofs.is_open()) {
      ofs << "P6\n" << _width << " " << _height << "\n" << _max_col_val << "\n";

      for (size_t i{0}; i < _size; ++i) {
        ofs << _data[i].Red() << _data[i].Green() << _data[i].Blue();
      }
    } else {
      std::cerr << "Error: Unable to open file "
                << fname
                << "\n";
      return false;
    }
    return true;
  }  // write

};  // class ppm

class bmp final : public image {
private:
  struct BMPHeader
  {
    BMPHeader(const std::uint32_t _bfSize,
              const std::uint32_t _biWidth,
              const std::uint32_t _biHeight,
              const std::uint32_t _biSizeImage) :
            bfSize(_bfSize),
            biWidth(_biWidth),
            biHeight(_biHeight),
            biSizeImage(_biSizeImage)
    {}

    std::uint16_t bfType {0x4d42};  // "BM"
    std::uint32_t bfSize {};
    std::uint16_t bfReserved1 {0};
    std::uint16_t bfReserved2 {0};
    std::uint32_t bfOffBits {sizeof(BMPHeader)};
    std::uint32_t biSize {40};
    std::uint32_t biWidth {};
    std::uint32_t biHeight {};
    std::uint16_t biPlanes {1};
    std::uint16_t biBitCount {24};
    std::uint32_t biCompression {0};
    std::uint32_t biSizeImage {};
    std::int32_t  biXPelsPerMeter {0};
    std::int32_t  biYPelsPerMeter {0};
    std::uint32_t biClrUsed {0};
    std::uint32_t biClrImportant {0};
  };  // struct BMPHeader
  static_assert(sizeof(BMPHeader) == 54, "BMPHeader must be 54 bytes");

//  constexpr
//  bool
//  inBounds(const std::int32_t y, const std::int32_t x) const noexcept
//  {
//    return (0 <= y) && (y < height_) && (0 <= x) && (x < width_);
//  }

public:
  // create a BMP default-sized object
  bmp() :
  image()
  {}

  // create an "empty" BMP with a given _width and _height; _data is filled with zeros
  bmp(const uint32_t _width, const uint32_t _height) :
  image(_width, _height)
  {}

  ~bmp() = default;

  // bmp signature: 0x42 0x4D
  bool
  isBMP(const std::string& fname) const noexcept(false)
  {
    return checkGraphicFile(fname, "BM");
  }

  bool
  write(const std::string& fname) const noexcept(false)
  {
    const std::uint32_t rowSize {_width * 3 + _width % 4};
    const std::uint32_t bmpsize {static_cast<uint32_t>(rowSize * _height)};
    const BMPHeader header(static_cast<std::uint32_t>(bmpsize + sizeof(BMPHeader)),
                           _width,
                           _height,
                           bmpsize);

    if (std::ofstream ofs {fname, std::ios_base::binary})
    {
      ofs.write(reinterpret_cast<const char*>(&header), sizeof(header));

      std::vector<std::uint8_t> line(static_cast<size_t>(rowSize));
      rgb::RGB color(0,0,0);

      for (std::int32_t y {static_cast<std::int32_t>(_height) - 1}; (-1) < y; --y)
      {
        size_t pos {0};

        for (std::uint32_t x {0}; x < _width; ++x)
        {
          color = getRGB(x, static_cast<size_t>(y));

          line[pos++] = color.Blue();
          line[pos++] = color.Green();
          line[pos++] = color.Red();
        }

        ofs.write(reinterpret_cast<const char*>(line.data()), static_cast<long>(line.size()));
      }
      return true;
    }
    // error trying to open the file
    return false;
  }
};  // class bmp

#pragma pack (pop)

}  // namespace sg
