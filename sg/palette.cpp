//
// palette.cpp
//
// Created by massimo on 10/8/18.
//
#include "palette.h"
////////////////////////////////////////////////////////////////////////////////
namespace sg::palette {

const std::string Palette::_rgbHexPaletteFileNameExtension {".rgbhex"};

std::string Palette::getOrder() const
{
  return _order;
}

void Palette::setOrder(const std::string& order)
{
  _order = order;
}

}

std::ostream& operator<<(std::ostream &os, const sg::palette::Palette& p)
{
  for (auto&& c : p())
  {
    os << c << "\n";
  }
  return os;
}
