// util.cpp
//
#include "util.h"

#include <QCoreApplication>
#include <QWidget>
#include <QSettings>
//#include <QDesktopWidget>
#include <QDir>
#include <QDesktopServices>
#include <QUrl>
#include <QDebug>

#include <iostream>

namespace demon {

void util::writeGeometryInSettings(const QWidget& widget, const QString& groupName) {
//  QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
  QSettings settings;

  settings.beginGroup(groupName);
  settings.setValue("version", QCoreApplication::applicationVersion());
  settings.setValue("size", widget.size());
  settings.setValue("pos", widget.pos());
//  settings.setValue("geometry", widget.saveGeometry());
  settings.endGroup();
  settings.sync();
}

void util::readGeometryFromSettings(QWidget& widget, const QString& groupName) {
//  QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
  QSettings settings;

  settings.beginGroup(groupName);
  auto size {settings.value("size", QSize(400, 400)).toSize()};
  auto pos {settings.value("pos", QPoint(0, 0)).toPoint()};
//  const QByteArray geometry = settings.value("geometry", QByteArray()).toByteArray();
  settings.endGroup();

  widget.resize(size);
  widget.move(pos);

//    if (geometry.isEmpty()) {
//      const QRect availableGeometry = QApplication::desktop()->availableGeometry(&widget);
//      widget.resize(availableGeometry.width() / 3, availableGeometry.height() / 2);
//      widget.move((availableGeometry.width() - widget.width()) / 2, (availableGeometry.height() - widget.height()) / 2);
//    } else {
//      widget.restoreGeometry(geometry);
//    }
}

void util::logSettings(const bool setFallBacksEnabled) {
  QSettings settings;

  settings.setFallbacksEnabled(setFallBacksEnabled);
  const QStringList allKeys {settings.allKeys()};
  for (const auto& key : allKeys) {
    qInfo() << "util::logSettings: key" << key
            << "->" << settings.value(key); //.toString();
  }
}

QString util::getObjectName(QObject* object) {
  return QString("%1:%2").arg(object->metaObject()->className(), object->objectName());
}

void
util::openFolder(const QString& absolutePath) noexcept {
  QDesktopServices::openUrl(QUrl("file:" + absolutePath, QUrl::TolerantMode));
}  // util::openFolder

}  // namespace demon
