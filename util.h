// util.h
//
#pragma once

#include <QObject>

namespace demon {
#pragma pack (push, 1)

class util final : public QObject
{
  Q_OBJECT

public:
  util() = delete;

  static void writeGeometryInSettings(const QWidget& widget, const QString& groupName);
  static void readGeometryFromSettings(QWidget& widget, const QString& groupName);
  static void logSettings(const bool setFallBacksEnabled = true);
  static QString getObjectName(QObject *object);
  static void openFolder(const QString& absolutePath) noexcept;

};  // class util

#pragma pack (pop)

}  // namespace demon
